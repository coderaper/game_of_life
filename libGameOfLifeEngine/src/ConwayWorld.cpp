/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <stdexcept>
#include <system_error>
#include <cstdlib>
#include <fstream>
#include <filesystem>
#include <charconv>
#include <algorithm>
#include <string>

#include <type_traits>
#include <cassert>

#include <boost/gil/extension/io/cells.hpp>
#include <boost/gil/extension/io/png.hpp>

#include <stringutils/stringutils.h>
#include <GameOfLifeEngine/ConwayWorld.h>

namespace {

std::seed_seq generate_mt19937_seed_sequence( std::random_device& rd )
{
    std::array<std::random_device::result_type, std::mt19937::state_size> seed_data;
    std::generate_n( seed_data.data(), seed_data.size(), std::ref( rd ) );

    return std::seed_seq( std::begin( seed_data ), std::end( seed_data ) );
}


std::seed_seq generate_mt19937_seed_sequence( std::uint_fast32_t sseq_seed )
{
    std::minstd_rand lc_rng( sseq_seed );

    std::array<std::minstd_rand::result_type, std::mt19937::state_size> seed_data;
    std::generate_n( seed_data.data(), seed_data.size(), std::ref( lc_rng ) );

    return std::seed_seq( std::begin( seed_data ), std::end( seed_data ) );
}


}  // nameless namespace


namespace game_of_life {

constexpr ConwayWorld::cell_value_t ConwayWorld::CELL_DEAD_VALUE;
constexpr ConwayWorld::cell_value_t ConwayWorld::CELL_ALIVE_VALUE;

// x_min,  y_min, width, height
ConwayWorld::ConwayWorld( size_t rows,
                          size_t cols )
: cells_image_( cols + 2, rows + 2, cell_t{ CELL_DEAD_VALUE }, std::size_t{ 0 } ),  // zero alignment
  cells_field_( subimage_view( view( cells_image_ ), 1, 1,
                                     cells_image_.width() - 2,
                                     cells_image_.height() - 2  ) ),

  outside_top_boundary_(  subimage_view( view( cells_image_ ),
                                         1, 0,
                                         cells_image_.width() - 2, 1 ) ),
  outside_bottom_boundary_( subimage_view( view( cells_image_ ),
                                           1, cells_image_.height() - 1,
                                           cells_image_.width() - 2 , 1 ) ),
  outside_left_boundary_( subimage_view( view( cells_image_ ),
                                         0, 1,
                                         1, cells_image_.height() - 2 ) ),
  outside_right_boundary_( subimage_view( view( cells_image_ ),
                                          cells_image_.width() - 1, 1,
                                          1, cells_image_.height() - 2 ) ),

  field_top_boundary_( subimage_view( cells_field_,
                                      0, 0,
                                      cells_field_.width(), 1 ) ),
  field_bottom_boundary_( subimage_view( cells_field_,
                                         0, cells_field_.height() - 1,
                                         cells_field_.width(), 1 ) ),
  field_left_boundary_( subimage_view( cells_field_,
                                       0, 0,
                                       1, cells_field_.height() ) ),
  field_right_boundary_( subimage_view( cells_field_,
                                        cells_field_.width() - 1, 0,
                                        1, cells_field_.height() ) ),
  world_topology_maker_( get_topology_maker( topology_ ) ),
  cells_field_cl_topleft_( cells_field_.xy_at( 0, 0 ).cache_location( -1, -1 ) ),
  cells_field_cl_top_( cells_field_.xy_at( 0, 0 ).cache_location(  0, -1 ) ),
  cells_field_cl_topright_( cells_field_.xy_at( 0, 0 ).cache_location(  1, -1 ) ),
  cells_field_cl_left_( cells_field_.xy_at( 0, 0 ).cache_location( -1,  0 ) ),
  cells_field_cl_right_( cells_field_.xy_at( 0, 0 ).cache_location(  1,  0 ) ),
  cells_field_cl_bottomleft_( cells_field_.xy_at( 0, 0 ).cache_location( -1,  1 ) ),
  cells_field_cl_bottom_( cells_field_.xy_at( 0, 0 ).cache_location(  0,  1 ) ),
  cells_field_cl_bottomright_( cells_field_.xy_at( 0, 0 ).cache_location(  1,  1 ) )
{
    static_assert( std::is_same<std::remove_cv_t<decltype(CELL_DEAD_VALUE)>, unsigned char>::value,
                   "CELL_DEAD_VALUE must be unsigned char" );
    static_assert( std::is_same<std::remove_cv_t<decltype(CELL_ALIVE_VALUE)>, unsigned char>::value,
                   "CELL_ALIVE_VALUE must be unsigned char" );

    // Seed the RNG with a real random value, if available
    std::random_device randdev;
    rng_seed( randdev );
}


ConwayWorld::ConwayWorld( size_t rows,
                          size_t cols,
                          double alive_ratio )
: ConwayWorld( rows, cols )
{
    populate_randomly( alive_ratio );
}


ConwayWorld::ConwayWorld( std::string_view filename )
{
    namespace fs = std::filesystem;
    fs::path in_file = fs::u8path( filename );
    if ( ! fs::exists( in_file ) ) {
        std::string msg{ "Cannot open input file \"" + in_file.string() +
                         "\" -- file does not exist!"
        };
        throw std::runtime_error( msg );
    }

    if ( ! fs::is_regular_file( in_file ) ) {
        std::string msg{ "Cannot open input file \"" + in_file.string() +
                         "\" -- file is not a regular file!"
        };
        throw std::runtime_error( msg );
    }

    using boost::gil::subimage_view;
    {
        cells_image_t image;
        using boost::gil::read_image;

        if ( stringutils::iequals( in_file.extension().string(), ".cells" ) )
        {
            using boost::gil::cells_tag;
            read_image( in_file.u8string(), image, cells_tag() );
        }
        else if ( stringutils::iequals( in_file.extension().string(), ".png" ) )
        {
            using boost::gil::png_tag;
            read_image( in_file.u8string(), image, png_tag() );
        }

        cells_image_.recreate( image.width() + 2,
                               image.height() + 2,
                               cell_t{ CELL_DEAD_VALUE },
                               std::size_t{0} );

        cells_field_ = subimage_view( view( cells_image_ ),
                                      1, 1,
                                      cells_image_.width() - 2,
                                      cells_image_.height() - 2 );

        using boost::gil::view;
        using boost::gil::copy_pixels;
        copy_pixels( view( image ), cells_field_ );
    }

    // x_min,  y_min, width, height
    outside_top_boundary_ = subimage_view( view( cells_image_ ),
                                           1, 0,
                                           cells_image_.width() - 2, 1 );
    outside_bottom_boundary_ = subimage_view( view( cells_image_ ),
                                              1, cells_image_.height() - 1,
                                              cells_image_.width() - 2, 1 );
    outside_left_boundary_ = subimage_view( view( cells_image_ ),
                                            0, 1,
                                            1, cells_image_.height() - 2 );
    outside_right_boundary_ = subimage_view( view( cells_image_ ),
                                             cells_image_.width() - 1, 1,
                                             1, cells_image_.height() - 2 );

    field_top_boundary_ = subimage_view( cells_field_,
                                         0, 0,
                                         cells_field_.width(), 1 );
    field_bottom_boundary_ = subimage_view( cells_field_,
                                            0, cells_field_.height() - 1,
                                            cells_field_.width(), 1 );
    field_left_boundary_ = subimage_view( cells_field_,
                                          0, 0,
                                          1, cells_field_.height() );
    field_right_boundary_ = subimage_view( cells_field_,
                                           cells_field_.width() - 1, 0,
                                           1, cells_field_.height() );

    world_topology_maker_ = get_topology_maker( topology_ );

    cells_field_cl_topleft_ = cells_field_.xy_at( 0, 0 ).cache_location( -1, -1 );
    cells_field_cl_top_ = cells_field_.xy_at( 0, 0 ).cache_location(  0, -1 );
    cells_field_cl_topright_ = cells_field_.xy_at( 0, 0 ).cache_location(  1, -1 );
    cells_field_cl_left_ = cells_field_.xy_at( 0, 0 ).cache_location( -1,  0 );
    cells_field_cl_right_ = cells_field_.xy_at( 0, 0 ).cache_location(  1,  0 );
    cells_field_cl_bottomleft_ = cells_field_.xy_at( 0, 0 ).cache_location( -1,  1 );
    cells_field_cl_bottom_ = cells_field_.xy_at( 0, 0 ).cache_location(  0,  1 );
    cells_field_cl_bottomright_ = cells_field_.xy_at( 0, 0 ).cache_location(  1,  1 );

    // Seed the RNG with a real random value, if available
    std::random_device randdev;
    rng_seed( randdev );
}


ConwayWorld::ConwayWorld( ConwayWorld const& other )
: cells_image_( other.cells_image_ ),
  cells_field_( subimage_view( view( cells_image_ ),
                               1, 1,
                               cells_image_.width() - 2,
                               cells_image_.height() - 2 ) ),
  outside_top_boundary_(  subimage_view( view( cells_image_ ),
                                         1, 0,
                                         cells_image_.width() - 2, 1 ) ),
  outside_bottom_boundary_( subimage_view( view( cells_image_ ),
                                           1, cells_image_.height() - 1,
                                           cells_image_.width() - 2, 1 ) ),
  outside_left_boundary_( subimage_view( view( cells_image_ ),
                                         0, 1,
                                         1, cells_image_.height() - 2 ) ),
  outside_right_boundary_( subimage_view( view( cells_image_ ),
                                          cells_image_.width() - 1, 1,
                                          1, cells_image_.height() - 2 ) ),
  field_top_boundary_( subimage_view( cells_field_,
                                      0, 0,
                                      cells_field_.width(), 1 ) ),
  field_bottom_boundary_( subimage_view( cells_field_,
                                         0, cells_field_.height() - 1,
                                         cells_field_.width(), 1 ) ),
  field_left_boundary_( subimage_view( cells_field_,
                                       0, 0,
                                       1, cells_field_.height() ) ),
  field_right_boundary_( subimage_view( cells_field_,
                                        cells_field_.width() - 1, 0,
                                        1, cells_field_.height() ) ),
  topology_( other.topology_ ),
  world_topology_maker_( get_topology_maker( topology_ ) ),
  cells_field_cl_topleft_( cells_field_.xy_at( 0, 0 ).cache_location( -1, -1 ) ),
  cells_field_cl_top_( cells_field_.xy_at( 0, 0 ).cache_location(  0, -1 ) ),
  cells_field_cl_topright_( cells_field_.xy_at( 0, 0 ).cache_location(  1, -1 ) ),
  cells_field_cl_left_( cells_field_.xy_at( 0, 0 ).cache_location( -1,  0 ) ),
  cells_field_cl_right_( cells_field_.xy_at( 0, 0 ).cache_location(  1,  0 ) ),
  cells_field_cl_bottomleft_( cells_field_.xy_at( 0, 0 ).cache_location( -1,  1 ) ),
  cells_field_cl_bottom_( cells_field_.xy_at( 0, 0 ).cache_location(  0,  1 ) ),
  cells_field_cl_bottomright_( cells_field_.xy_at( 0, 0 ).cache_location(  1,  1 ) ),
  rng_( other.rng_ )
{
}


ConwayWorld::~ConwayWorld() = default;


std::filesystem::path
ConwayWorld::check_out_file_and_add_extension( const char* filename,
                                               bool overwrite ) const
{
    namespace fs = std::filesystem;
    fs::path out_file{ fs::u8path( filename ) };
    if ( fs::exists( out_file ) ) {
        if ( ! overwrite ) {
            std::string msg{ "'" + out_file.u8string() + "' already exist!" };
            throw std::runtime_error( msg );
        }

        if ( ! fs::is_regular_file( out_file ) ) {
            std::string msg{ "Output file '" + out_file.u8string() +
                             "' is not a regular file!"
            };
            throw std::runtime_error( msg );
        }
    }

    if ( out_file.has_extension() ) {
        fs::path extension = out_file.extension();
        bool equal = stringutils::iequals( extension.string(), ".cells" );
        equal = stringutils::iequals( extension.string(), ".png" );
        if ( ! is_valid_extension( extension.string() ) ) {
            std::string msg{ "Ouput file '" + out_file.u8string() +
                             "' extension is not valid!"
            };
            throw std::runtime_error( msg );
        }
    }
    else {
        if ( rows() < 128 && cols() < 128 ) {
            out_file.append( ".cells" );
        }
        else {
            out_file.append( ".png" );
        }
    }

    return out_file;
}

void ConwayWorld::store( const char* filename, bool overwrite ) const
{
    namespace fs = std::filesystem;
    fs::path out_file = check_out_file_and_add_extension( filename, overwrite );

    namespace gil = boost::gil;
    if ( stringutils::iequals( out_file.extension().string(), ".cells" ) ) {
        gil::image_write_info< gil::cells_tag > out_image_info;
        //out_image_info._comments = in_image_info._info._comments;
        gil::write_view( out_file.u8string(), cells_field_, out_image_info );
    }
    else {
         gil::image_write_info< gil::png_tag > out_image_info;
         //out_image_info._comments = in_image_info._info._comments;
         gil::write_view( out_file.u8string(), cells_field_, out_image_info );
    }
}


void ConwayWorld::store( const char* filename,
                         size_t row, size_t col,
                         size_t width, size_t height,
                         bool overwrite ) const
{
    namespace fs = std::filesystem;
    fs::path out_file = check_out_file_and_add_extension( filename, overwrite );

    namespace gil = boost::gil;
    auto selection_view = gil::subimage_view( cells_field_,
                                              col,
                                              row,
                                              width,
                                              height );

    if ( stringutils::iequals( out_file.extension().string(), ".cells" ) ) {
        gil::image_write_info< gil::cells_tag > out_image_info;
        //out_image_info._comments = in_image_info._info._comments;
        gil::write_view( out_file.u8string(), selection_view, out_image_info );
    }
    else {
         gil::image_write_info< gil::png_tag > out_image_info;
         //out_image_info._comments = in_image_info._info._comments;
         gil::write_view( out_file.u8string(), selection_view, out_image_info );
    }
}


bool ConwayWorld::is_valid_extension( const std::string_view extension )
{
    static constexpr std::string_view valid_extensions[] = {
        ".cells",
        ".png",
    };

    auto it = std::find_if( std::cbegin( valid_extensions ),
                            std::cend( valid_extensions ),
                            [extension]( const std::string_view sv ) {
                                return stringutils::iequals( extension, sv );
                            } );

    return it != std::cend( valid_extensions );
}


size_t ConwayWorld::rows() const { return cells_field_.height(); }


size_t ConwayWorld::cols() const { return cells_field_.width(); }


size_t ConwayWorld::size() const { return rows() * cols(); }


void ConwayWorld::kill()
{
    using boost::gil::for_each_pixel;
    for_each_pixel( cells_field_,
                    []( cell_t& p ) {
                        boost::gil::at_c<0>( p ) = CELL_DEAD_VALUE;
                    } );
}


void ConwayWorld::kill( size_t row, size_t col,
                        size_t width, size_t height )
{
    auto selection_view = subimage_view( cells_field_,
                                         col,
                                         row,
                                         width,
                                         height );

    using boost::gil::for_each_pixel;
    for_each_pixel( selection_view,
                    []( cell_t& p ) {
                        boost::gil::at_c<0>( p ) = CELL_DEAD_VALUE;
                    } );
}


void ConwayWorld::resurrect()
{
    using boost::gil::for_each_pixel;
    for_each_pixel( cells_field_,
                    []( cell_t& p ) {
                        boost::gil::at_c<0>( p ) = CELL_ALIVE_VALUE;
                    } );
}

void ConwayWorld::resurrect( size_t row, size_t col,
                size_t width, size_t height )
{
    using boost::gil::subimage_view;
    auto selection_view = subimage_view( cells_field_,
                                         col,
                                         row,
                                         width,
                                         height );

    using boost::gil::for_each_pixel;
    for_each_pixel( selection_view,
                    []( cell_t& p ) {
                        boost::gil::at_c<0>( p ) = CELL_ALIVE_VALUE;
                    } );
}


void ConwayWorld::populate_randomly( double alive_ratio )
{
    auto alive_treshold = static_cast<std::mt19937::result_type>(
        ( rng_.max() - rng_.min() ) * alive_ratio
    );

    using boost::gil::for_each_pixel;
    for_each_pixel( cells_field_,
                    [this, alive_treshold]( cell_t& p ) {
                        boost::gil::at_c<0>( p ) = rng_() < alive_treshold ?
                        CELL_ALIVE_VALUE : CELL_DEAD_VALUE;
                    } );
}


void ConwayWorld::populate_randomly( size_t row, size_t col,
                                     size_t width, size_t height,
                                     double alive_ratio )
{
    auto alive_treshold = static_cast<std::mt19937::result_type>(
        ( rng_.max() - rng_.min() ) * alive_ratio
    );

    using boost::gil::subimage_view;
    auto selection_view = subimage_view( cells_field_,
                                         col,
                                         row,
                                         width,
                                         height );

    using boost::gil::for_each_pixel;
    for_each_pixel( selection_view,
                    [this, alive_treshold]( cell_t& p ) {
                        boost::gil::at_c<0>( p ) = rng_() < alive_treshold ?
                        CELL_ALIVE_VALUE : CELL_DEAD_VALUE;
                    } );
}


void ConwayWorld::copy_cells( const ConwayWorld& src,
                              size_t src_row, size_t src_col,
                              size_t src_width, size_t src_height,
                              size_t dst_row, size_t dst_col )
{

    using boost::gil::subimage_view;
    using boost::gil::copy_pixels;
    copy_pixels( subimage_view( src.cells_field_,
                                src_col,
                                src_row,
                                src_width,
                                src_height ),
                 subimage_view( cells_field_,
                                dst_col,
                                dst_row,
                                src_width,
                                src_height ) );
}


size_t ConwayWorld::population_count() const
{
    size_t num_cells_alive = 0;

    using boost::gil::for_each_pixel;
    for_each_pixel( std::as_const( cells_field_ ),
                    [&num_cells_alive]( const cell_t& p ) {
                        num_cells_alive += boost::gil::at_c<0>( p ) == CELL_ALIVE_VALUE ?
                        1 : 0;
                    } );

    return num_cells_alive;
}


bool ConwayWorld::is_cell_alive( size_t row,
                                 size_t col ) const
{
    return std::as_const( cells_field_ )( col, row ) == CELL_ALIVE_VALUE;
}


void ConwayWorld::set_cell_alive( size_t row,
                                  size_t col,
                                  bool alive )
{
    using boost::gil::view;
    cells_field_( col, row ) = alive ? CELL_ALIVE_VALUE : CELL_DEAD_VALUE;
}


uint_fast16_t ConwayWorld::sum_living_neighbors( const cells_loc_t& loc ) const
{
    uint_fast16_t sum{ 0 };

    sum += loc[cells_field_cl_topleft_];
    sum += loc[cells_field_cl_top_];
    sum += loc[cells_field_cl_topright_];
    sum += loc[cells_field_cl_left_];
    sum += loc[cells_field_cl_right_];
    sum += loc[cells_field_cl_bottomleft_];
    sum += loc[cells_field_cl_bottom_];
    sum += loc[cells_field_cl_bottomright_];

    return sum;
}


size_t ConwayWorld::count_living_neighbors( size_t row, size_t col ) const
{
    cells_loc_t loc = std::as_const( cells_field_ ).xy_at( col, row );
    return sum_living_neighbors( loc ) / CELL_ALIVE_VALUE;
}


WorldTopology ConwayWorld::get_topology() const
{
    return topology_;
}


void ConwayWorld::set_topology( WorldTopology topology )
{
    if ( topology == topology_ )
        return;

    world_topology_maker_ = get_topology_maker( topology );
    topology_ = topology;
}


void ConwayWorld::form_bounded_plain_world_topology()
{
    using boost::gil::view;
    cells_view_t cells_image_view = view( cells_image_ );

    cells_image_view( 0, 0 ) = CELL_DEAD_VALUE;
    using boost::gil::fill_pixels;
    fill_pixels( outside_top_boundary_, CELL_DEAD_VALUE );
    cells_image_view( cells_image_view.width() - 1, 0 ) = CELL_DEAD_VALUE;
    cells_image_view( 0, cells_image_view.height() - 1 ) = CELL_DEAD_VALUE;
    fill_pixels( outside_bottom_boundary_, CELL_DEAD_VALUE );
    cells_image_view( cells_image_view.width() - 1,
                      cells_image_view.height() - 1 ) = CELL_DEAD_VALUE;
    fill_pixels( outside_left_boundary_, CELL_DEAD_VALUE );
    fill_pixels( outside_right_boundary_, CELL_DEAD_VALUE );
}


void ConwayWorld::form_torus_world_topology()
{
    using boost::gil::view;
    cells_view_t cells_image_view = view( cells_image_ );

    cells_image_view( 0, 0 ) =
        cells_field_( cells_field_.width() - 1, cells_field_.height() - 1 );
    using boost::gil::copy_pixels;
    copy_pixels( field_bottom_boundary_, outside_top_boundary_ );
    cells_image_view( cells_image_view.width() - 1, 0 ) =
        cells_field_( 0, cells_field_.height() - 1 );
    cells_image_view( 0, cells_image_view.height() - 1 ) =
        cells_field_( cells_field_.width() - 1, 0 );
    copy_pixels( field_top_boundary_, outside_bottom_boundary_ );
    cells_image_view( cells_image_view.width() - 1,
                      cells_image_view.height() - 1 ) =
        cells_field_( 0, 0 );
    copy_pixels( field_right_boundary_, outside_left_boundary_ );
    copy_pixels( field_left_boundary_, outside_right_boundary_ );
}


void ConwayWorld::form_klein_bottle_horiz_twist_world_topology()
{
    using boost::gil::view;
    cells_view_t cells_image_view = view( cells_image_ );

    cells_image_view( 0, 0 ) =
        cells_field_( 0, cells_field_.height() - 1 );
    using boost::gil::copy_pixels;
    using boost::gil::flipped_left_right_view;
    copy_pixels( flipped_left_right_view( field_bottom_boundary_ ),
                 outside_top_boundary_ );
    cells_image_view( cells_image_view.width() - 1, 0 ) =
        cells_field_( cells_field_.width() - 1, cells_field_.height() - 1 );
    cells_image_view( 0, cells_image_view.height() - 1 ) =
        cells_field_( 0, 0 );
    copy_pixels( flipped_left_right_view( field_top_boundary_ ),
                 outside_bottom_boundary_ );
    cells_image_view( cells_image_view.width() - 1,
                      cells_image_view.height() - 1 ) =
        cells_field_( cells_field_.width() - 1, 0 );
    copy_pixels( field_right_boundary_, outside_left_boundary_ );
    copy_pixels( field_left_boundary_, outside_right_boundary_ );
}


void ConwayWorld::form_klein_bottle_vert_twist_world_topology()
{
    using boost::gil::view;
    cells_view_t cells_image_view = view( cells_image_ );

    cells_image_view( 0, 0 ) = cells_field_( cells_field_.width() - 1, 0 );
    using boost::gil::copy_pixels;
    copy_pixels( field_bottom_boundary_, outside_top_boundary_ );
    cells_image_view( cells_image_view.width() - 1, 0 ) = cells_field_( 0, 0 );

    cells_image_view( 0, cells_image_view.height() - 1 ) =
        cells_field_( cells_field_.width() - 1, cells_field_.height() - 1 );

    copy_pixels( field_top_boundary_, outside_bottom_boundary_ );
    cells_image_view( cells_image_view.width() - 1,
                      cells_image_view.height() - 1 ) =
        cells_field_( 0, cells_field_.height() - 1 );
    using boost::gil::flipped_up_down_view;
    copy_pixels( flipped_up_down_view( field_right_boundary_ ),
                 outside_left_boundary_ );
    copy_pixels( flipped_up_down_view( field_left_boundary_ ),
                 outside_right_boundary_ );
}


void ConwayWorld::form_cross_surface_world_topology()
{
    using boost::gil::view;
    cells_view_t cells_image_view = view( cells_image_ );

    cells_image_view( 0, 0 ) = cells_field_( 0, 0 );
    using boost::gil::copy_pixels;
    using boost::gil::flipped_left_right_view;
    copy_pixels( flipped_left_right_view( field_bottom_boundary_ ),
                 outside_top_boundary_ );
    cells_image_view( cells_image_view.width() - 1, 0 ) =
        cells_field_( cells_field_.width() - 1, 0 );
    cells_image_view( 0, cells_image_view.height() - 1 ) =
        cells_field_( 0, cells_field_.height() - 1 );
    copy_pixels( flipped_left_right_view( field_top_boundary_ ),
                 outside_bottom_boundary_ );
    cells_image_view( cells_image_view.width() - 1,
                      cells_image_view.height() - 1 ) =
        cells_field_( cells_field_.width() - 1, cells_field_.height() - 1 );
    using boost::gil::flipped_up_down_view;
    copy_pixels( flipped_up_down_view( field_right_boundary_ ),
                 outside_left_boundary_ );
    copy_pixels( flipped_up_down_view( field_left_boundary_ ),
                 outside_right_boundary_ );
}


//void ConwayWorld::form_sphere_world_topology()
//{
//    throw std::logic_error( "Sphere World Topology is still not implemented" );
//}


std::function<void()> ConwayWorld::get_topology_maker( WorldTopology topology )
{
    switch ( topology ) {
        case WorldTopology::BOUNDED_PLANE:
            return [this]() { form_bounded_plain_world_topology(); };

        case WorldTopology::TORUS:
            return [this]() { form_torus_world_topology(); };

        case WorldTopology::KLEIN_BOTTLE_HORIZ_TWIST:
            return [this]() { form_klein_bottle_horiz_twist_world_topology(); };

        case WorldTopology::KLEIN_BOTTLE_VERT_TWIST:
            return [this]() { form_klein_bottle_vert_twist_world_topology(); };

        case WorldTopology::CROSS_SURFACE:
            return [this]() { form_cross_surface_world_topology(); };

//        case WorldTopology::SPHERE:
//            return [this]() { form_sphere_world_topology(); };
        default:
            throw std::logic_error( "cannot create topology maker -- "
                            "insupported topology type" );
    }

    return nullptr;
}


void ConwayWorld::evolve( ConwayWorld& next ) const
{
    // enforce selected world topology
    world_topology_maker_();

    // Every cell in the Conway's world interacts with its eight neighboring
    // cells in accordance with these four rules:
    //   - Any live cell with fewer than two living neighbours dies (isolation).
    //   - Any cell with two or three living neighbours survives (survival).
    //   - Any cell with more than three living neighbours dies (overpopulation).
    //   - Any dead cell with three living neighbours comes to life (reproduction).
    auto lambda = [this]( const cells_loc_t& loc ) -> cell_value_t {
        static constexpr uint_fast16_t count_preserve = 2 * CELL_ALIVE_VALUE;
        static constexpr uint_fast16_t count_born = 3 * CELL_ALIVE_VALUE;
        uint_fast16_t sum = sum_living_neighbors( loc );
        return sum == count_preserve ? *loc :
                                       ( sum == count_born ?
                                         CELL_ALIVE_VALUE :
                                         CELL_DEAD_VALUE );
    };

    using boost::gil::transform_pixel_positions;
    transform_pixel_positions( std::as_const( cells_field_ ),
                               next.cells_field_,
                               lambda );
}


void ConwayWorld::rng_seed( std::random_device& rd )
{
    std::seed_seq sseq = generate_mt19937_seed_sequence( rd );
    rng_.seed( sseq );
}


void ConwayWorld::rng_seed( std::uint_fast32_t sseq_seed )
{
    std::seed_seq sseq = generate_mt19937_seed_sequence( sseq_seed );
    rng_.seed( sseq );
}


}  // namespace game_of_life
