/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <stdexcept>
#include <system_error>
#include <cstdlib>
#include <fstream>
//#include <filesystem>
//#include <charconv>
#include <algorithm>
#include <string>

#include <type_traits>
#include <cassert>

#include <GameOfLifeEngine/ConwayWorld.h>
#include <GameOfLifeEngine/GameOfLifeEngine.h>

namespace {

template< typename T >
class CallbackRAII
{
public:
    using value_type = T;
    using reference_type = T&;

    CallbackRAII( value_type callback ) : callback_( callback ) {}
    CallbackRAII( const CallbackRAII& ) = delete;
    CallbackRAII( CallbackRAII&& ) = delete;
    CallbackRAII& operator=( const CallbackRAII& ) = delete;
    CallbackRAII& operator=( CallbackRAII&& ) = delete;
    ~CallbackRAII() { if ( callback_ != nullptr ) callback_(); }

    constexpr operator reference_type() { return callback_; }
    constexpr operator reference_type() const { return callback_; }

private:
    value_type callback_;
};


}  // nameless namespace


namespace game_of_life {


class GameOfLifeEngine::Impl {
public:
    Impl( size_t rows,
          size_t cols );

    void resize_and_exterminate( size_t rows, size_t columns );
    void resize_and_populate_randomly( size_t rows, size_t cols );
    void expand( size_t width, size_t height,
                 size_t offset_cols, size_t offset_rows );
    void crop( size_t row, size_t col,
               size_t width, size_t height );

    void save_world( const char* filename, bool overwrite = false ) const;

    void save_world( const char* filename,
                     size_t row, size_t col,
                     size_t width, size_t height,
                     bool overwrite = false ) const;

    void load_world( std::string_view filename );

    static bool is_valid_extension( const std::string_view extension );

    size_t rows() const;

    size_t cols() const;

    size_t size() const;

    WorldTopology get_topology() const;

    void set_topology( WorldTopology topology );

    int frame_period() const;
    void set_frame_period( int ms );

    void kill();

    void kill( size_t row, size_t col,
               size_t width, size_t height );

    void resurrect();

    void resurrect( size_t row, size_t col,
                    size_t width, size_t height );

    void populate_randomly();

    void populate_randomly( size_t row, size_t col,
                            size_t width, size_t height );

    bool is_cell_alive( size_t row,
                        size_t col ) const;

    void set_cell_alive( size_t row,
                         size_t col,
                         bool alive );

    size_t count_living_neighbors( size_t row,
                                   size_t col ) const;

    size_t population_count() const;

    // Returns the current population density of the world
    double population_density() const;

    double populate_alive_ratio() const;

    void set_populate_alive_ratio( double alive_ratio );

    bool is_resettable_to_seed_generation() const;

    void reset_to_seed_generation();

    bool is_running() const noexcept { return is_running_; }

    enum class RunMode {
        SINGLE,
        CONTINUOUS
    };

    RunMode run_mode() const noexcept { return run_mode_; }

    size_t generation() const noexcept { return generation_; }

    void start_single_generation( std::function<void()> callback );

    void start_continuous_generation( std::function<void()> callback );

    bool has_generation_ready() const;

    void stop_continuous_generation();

    void flip_generations();

private:
    void compute_next_generation();

    void launch_compute_next_generation();

    void throw_if_running( std::string_view errmsg ) const;

    void on_world_externally_changed();

    void compute_statistics() const;

    void update_statistics_new_cell() const;

    void update_statistics_dead_cell() const;

private:
    size_t rows_ = 24;
    size_t columns_ = 48;
    WorldTopology topology_ = WorldTopology::DEFAULT;

    double alive_ratio_ = 1.0 / 3.0;

    // Mersenne Twister 19937 random number generator
    std::mt19937 rng_;

    std::atomic<size_t> generation_ = 0;

    std::atomic<bool> is_running_ = false;

    std::atomic<RunMode> run_mode_ = RunMode::SINGLE;

    std::chrono::milliseconds frame_period_ms_;

    using ConwayWorld = game_of_life::ConwayWorld;
    std::unique_ptr<ConwayWorld> current_world_;
    std::unique_ptr<ConwayWorld> next_world_;
    std::unique_ptr<ConwayWorld> seed_world_;

    std::future<void> future_;
    std::atomic<bool> has_generation_ready_ = false;
    std::function<void()> callbackNextGenerationComputed_;

    mutable bool is_statistics_up_to_date_ = false;
    mutable size_t population_count_ = 0;
    mutable double population_density_ = 0.0;
};


GameOfLifeEngine::Impl::Impl( size_t rows,
                              size_t cols )
: rows_( rows ),
  columns_( cols ),
  frame_period_ms_{ 400 },
  current_world_( std::make_unique<ConwayWorld>( static_cast<size_t>( rows_ ),
                                                 static_cast<size_t>( columns_ ) ) ),
  next_world_( std::make_unique<ConwayWorld>( static_cast<size_t>( rows_ ),
                                              static_cast<size_t>( columns_ ) ) )
{
    current_world_->set_topology( topology_ );
    next_world_->set_topology( topology_ );
}


void GameOfLifeEngine::Impl::resize_and_exterminate( size_t rows, size_t cols )
{
    throw_if_running( "cannot resize & exterminate the world while running" );

    if ( rows_ == rows && columns_ == cols ) {
        kill();
        return;
    }

    rows_ = rows;
    columns_ = cols;

    current_world_ = std::make_unique<ConwayWorld>( rows_, columns_ );
    next_world_ = std::make_unique<ConwayWorld>( rows_, columns_ );

    current_world_->set_topology( topology_ );
    next_world_->set_topology( topology_ );

    on_world_externally_changed();
//    has_generation_ready_ = false;
}


void GameOfLifeEngine::Impl::resize_and_populate_randomly( size_t rows,
                                                           size_t cols )
{
    throw_if_running( "cannot resize & populate the world while running" );

    if ( rows_ == rows && columns_ == cols ) {
        populate_randomly();
        return;
    }

    rows_ = rows;
    columns_ = cols;

    current_world_ = std::make_unique<ConwayWorld>( rows_, columns_,
                                                    alive_ratio_ );
    next_world_ = std::make_unique<ConwayWorld>( rows_, columns_ );

    current_world_->set_topology( topology_ );
    next_world_->set_topology( topology_ );

    on_world_externally_changed();
//    has_generation_ready_ = false;
}


void GameOfLifeEngine::Impl::expand( size_t width, size_t height,
                                     size_t offset_cols, size_t offset_rows )
{
    throw_if_running( "cannot expand the world while running" );

    if ( height < rows_ || width < columns_ ) {
        std::logic_error( std::string( "cannot expand the world to smaller "
                                       "dimentions" ) );
    }

    if ( offset_rows + height > rows_ || offset_cols + width > columns_ ) {
        std::logic_error( std::string( "old world is shifted outside the new "
                                       "world dimentions" ) );
    }

    if ( rows_ == height && columns_ == width ) {
        return;                            // no change - nothing to do
    }

    std::unique_ptr<ConwayWorld> expanded_world =
        std::make_unique<ConwayWorld>( height, width );

    expanded_world->copy_cells( *current_world_,
                                0, 0,
                                current_world_->cols(),
                                current_world_->rows(),
                                offset_rows, offset_cols );
    current_world_ = std::move( expanded_world );

    rows_ = current_world_->rows();
    columns_ = current_world_->cols();
    next_world_ = std::make_unique<ConwayWorld>( rows_, columns_ );

    current_world_->set_topology( topology_ );
    next_world_->set_topology( topology_ );

    on_world_externally_changed();
//    has_generation_ready_ = false;
}


void GameOfLifeEngine::Impl::crop( size_t row, size_t col,
                                   size_t width, size_t height )
{
    throw_if_running( "cannot crop the world while running" );

    if ( row + height > rows_ || col + width > columns_ ) {
        std::logic_error( std::string( "crop area exceeds the world "
                                       "dimentions" ) );
    }

    if ( rows_ == height && columns_ == width && row == 0 && col == 0  ) {
        return;                            // no change - nothing to do
    }

    std::unique_ptr<ConwayWorld> cropped_world =
        std::make_unique<ConwayWorld>( height, width );

    cropped_world->copy_cells( *current_world_,
                               row, col,
                               cropped_world->cols(),
                               cropped_world->rows(),
                               0, 0 );
    current_world_ = std::move( cropped_world );

    rows_ = current_world_->rows();
    columns_ = current_world_->cols();
    next_world_ = std::make_unique<ConwayWorld>( rows_, columns_ );

    current_world_->set_topology( topology_ );
    next_world_->set_topology( topology_ );

    on_world_externally_changed();
//    has_generation_ready_ = false;
}


void GameOfLifeEngine::Impl::save_world( const char* filename, bool overwrite ) const
{
    throw_if_running( "cannot save the world while running" );

    current_world_->store( filename, overwrite );
}

void GameOfLifeEngine::Impl::save_world( const char* filename,
                                         size_t row, size_t col,
                                         size_t width, size_t height,
                                         bool overwrite ) const
{
    throw_if_running( "cannot save the world while running" );

    current_world_->store( filename,
                           row, col,
                           width, height,
                           overwrite );
}

void GameOfLifeEngine::Impl::load_world( std::string_view filename )
{
    throw_if_running( "cannot load new world while running" );

    std::unique_ptr<ConwayWorld> world =
        std::make_unique<ConwayWorld>( filename );

    if ( ! world ) {
        throw std::runtime_error( "Cannot load world from file" );
    }

    current_world_ = std::move( world );

    rows_ = current_world_->rows();
    columns_ = current_world_->cols();
    next_world_ = std::make_unique<ConwayWorld>( rows_, columns_ );

    current_world_->set_topology( topology_ );
    next_world_->set_topology( topology_ );

    on_world_externally_changed();
//    has_generation_ready_ = false;
}


bool GameOfLifeEngine::Impl::is_valid_extension( const std::string_view extension )
{
    return ConwayWorld::is_valid_extension( extension );
}


size_t GameOfLifeEngine::Impl::rows() const { return rows_; }


size_t GameOfLifeEngine::Impl::cols() const { return columns_; }


size_t GameOfLifeEngine::Impl::size() const { return rows() * cols(); }


WorldTopology GameOfLifeEngine::Impl::get_topology() const
{
    return topology_;
}

void GameOfLifeEngine::Impl::set_topology( WorldTopology topology )
{
    if ( topology_ != topology ) {
        topology_ = topology;
        current_world_->set_topology( topology_ );
        next_world_->set_topology( topology_ );
    }
}

int GameOfLifeEngine::Impl::frame_period() const
{
    return frame_period_ms_.count();
}


void GameOfLifeEngine::Impl::set_frame_period( int ms )
{
    frame_period_ms_ = std::chrono::milliseconds( ms );
}


void GameOfLifeEngine::Impl::kill()
{
    throw_if_running( "cannot exterminate the world while running" );

    current_world_->kill();
    on_world_externally_changed();
}


void GameOfLifeEngine::Impl::kill( size_t row, size_t col,
                                   size_t width, size_t height )
{
    throw_if_running( "cannot exterminate the world selection while running" );

    current_world_->kill( row, col, width, height );
    on_world_externally_changed();
}


void GameOfLifeEngine::Impl::resurrect()
{
    throw_if_running( "cannot resurrect the world while running" );

    current_world_->resurrect();
    on_world_externally_changed();
}


void GameOfLifeEngine::Impl::resurrect( size_t row, size_t col,
                                        size_t width, size_t height )
{
    throw_if_running( "cannot resurrect the world selection while running" );

    current_world_->resurrect( row, col, width, height );
    on_world_externally_changed();;
}


void GameOfLifeEngine::Impl::populate_randomly()
{
    throw_if_running( "cannot populate the world randomly while running" );

    current_world_->populate_randomly( alive_ratio_ );
    on_world_externally_changed();
}


void GameOfLifeEngine::Impl::populate_randomly( size_t row, size_t col,
                                                size_t width, size_t height )
{
    throw_if_running( "cannot populate the world selection randomly while running" );

    current_world_->populate_randomly( row, col, width, height, alive_ratio_ );
    on_world_externally_changed();
}


bool GameOfLifeEngine::Impl::is_cell_alive( size_t row, size_t col ) const
{
    return current_world_->is_cell_alive( row, col );
}


void GameOfLifeEngine::Impl::set_cell_alive( size_t row,
                                             size_t col,
                                             bool alive )
{
    throw_if_running( "cannot edit cell while running" );

    if ( current_world_->is_cell_alive( row, col ) != alive ) {
        current_world_->set_cell_alive( row, col, alive );
        on_world_externally_changed();
        if ( alive )
            update_statistics_new_cell();
        else
            update_statistics_dead_cell();
    }
}


size_t GameOfLifeEngine::Impl::count_living_neighbors( size_t row,
                                                       size_t col ) const
{
    return current_world_->count_living_neighbors( row, col );
}


size_t GameOfLifeEngine::Impl::population_count() const
{
    if ( ! is_statistics_up_to_date_ )
        compute_statistics();

    return population_count_;
}


double GameOfLifeEngine::Impl::population_density() const
{
    if ( ! is_statistics_up_to_date_ )
        compute_statistics();

    return population_density_;
}


double GameOfLifeEngine::Impl::populate_alive_ratio() const
{
    return alive_ratio_;
}


void GameOfLifeEngine::Impl::set_populate_alive_ratio( double alive_ratio )
{
    alive_ratio_ = alive_ratio;
}


bool GameOfLifeEngine::Impl::is_resettable_to_seed_generation() const
{
    return ! is_running() && seed_world_ != nullptr;
}


void GameOfLifeEngine::Impl::reset_to_seed_generation()
{
    throw_if_running( "cannot reset to seed generation while running" );

    current_world_ = std::move( seed_world_ );
    is_statistics_up_to_date_ = false;
    generation_ = 0;
}


void GameOfLifeEngine::Impl::start_single_generation( std::function<void()> callback )
{
    throw_if_running( "cannot start single generation while running" );

    assert( ! has_generation_ready() );

    if ( seed_world_ == nullptr )
        seed_world_ = std::make_unique<ConwayWorld>( *current_world_ );

    is_running_ = true;

    run_mode_ = RunMode::SINGLE;

    callbackNextGenerationComputed_ = std::move( callback );
    launch_compute_next_generation();
}


void GameOfLifeEngine::Impl::start_continuous_generation( std::function<void()> callback )

{
    throw_if_running( "cannot start continuous generation while running" );

    assert( ! has_generation_ready() );

    if ( seed_world_ == nullptr )
        seed_world_ = std::make_unique<ConwayWorld>( *current_world_ );

    is_running_ = true;

    run_mode_ = RunMode::CONTINUOUS;

    callbackNextGenerationComputed_ = std::move( callback );
    launch_compute_next_generation();
}


bool GameOfLifeEngine::Impl::has_generation_ready() const
{
    return has_generation_ready_;
}


void GameOfLifeEngine::Impl::stop_continuous_generation()
{
    run_mode_ = RunMode::SINGLE;
}


void GameOfLifeEngine::Impl::flip_generations()
{
    if ( ! future_.valid() || ! is_running() ) {
        std::logic_error( "invalid state when generations flipping "
                          "is requested" );
    }

    future_.get();

    assert( has_generation_ready_ );

    std::swap( current_world_, next_world_ );
    has_generation_ready_ = false;
    is_statistics_up_to_date_ = false;
    ++generation_;

    if ( run_mode_ == RunMode::CONTINUOUS ) {
        launch_compute_next_generation();
    }
    else {
        is_running_ = false;
    }
}


void GameOfLifeEngine::Impl::compute_next_generation()
{
    auto start_time = std::chrono::steady_clock::now();

    current_world_->evolve( *next_world_ );

    if ( run_mode_ == RunMode::CONTINUOUS ) {
        if ( frame_period_ms_.count() > 0 ) {
            auto end_time = start_time + frame_period_ms_;
            std::this_thread::sleep_until( end_time );
        }
    }

    has_generation_ready_ = true;
}


void GameOfLifeEngine::Impl::launch_compute_next_generation()
{
    future_ = std::async( std::launch::async,
                          [this]() {
                              CallbackRAII notifier( callbackNextGenerationComputed_ );
                              compute_next_generation();
                          } );
}


void GameOfLifeEngine::Impl::throw_if_running( std::string_view errmsg ) const
{
    if ( future_.valid() || is_running() ) {
        std::logic_error( std::string( errmsg ) );
    }
}


void GameOfLifeEngine::Impl::on_world_externally_changed()
{
    seed_world_.reset( nullptr );
    is_statistics_up_to_date_ = false;
    generation_ = 0;
}


void GameOfLifeEngine::Impl::compute_statistics() const
{
    population_count_ = current_world_->population_count();

    population_density_ = ConwayWorld::population_density( current_world_->size(),
                                                           population_count_ );

    is_statistics_up_to_date_ = true;
}


void GameOfLifeEngine::Impl::update_statistics_new_cell() const
{
    if ( ! is_statistics_up_to_date_ )
        return;

    ++population_count_;

    population_density_ = ConwayWorld::population_density( current_world_->size(),
                                                           population_count_ );
}


void GameOfLifeEngine::Impl::update_statistics_dead_cell() const
{
    if ( ! is_statistics_up_to_date_ )
        return;

    assert( population_count_ > 0 );
    --population_count_;

    population_density_ = ConwayWorld::population_density( current_world_->size(),
                                                           population_count_ );
}


GameOfLifeEngine::GameOfLifeEngine( size_t rows,
                                    size_t cols )
 : pimpl_{ std::make_unique<GameOfLifeEngine::Impl>( rows, cols ) }
{}

//GameOfLifeEngine::GameOfLifeEngine( GameOfLifeEngine const& other ) noexcept
// : pimpl_{ std::make_unique<GameOfLifeEngine::Impl>( *other.pimpl_ ) }
//{}

GameOfLifeEngine::GameOfLifeEngine( GameOfLifeEngine&& other ) noexcept = default;

GameOfLifeEngine::~GameOfLifeEngine() = default;

//GameOfLifeEngine& GameOfLifeEngine::operator=( GameOfLifeEngine const& other ) noexcept {
//  *pimpl_ = *other.pimpl_;
//  return *this;
//}

GameOfLifeEngine& GameOfLifeEngine::operator=( GameOfLifeEngine&& other ) noexcept = default;


void GameOfLifeEngine::resize_and_exterminate( size_t rows, size_t cols )
{
    pimpl_->resize_and_exterminate( rows, cols );
}


void GameOfLifeEngine::resize_and_populate_randomly( size_t rows,
                                                     size_t cols )
{
    pimpl_->resize_and_populate_randomly( rows, cols );
}


void GameOfLifeEngine::expand( size_t width, size_t height,
                               size_t offset_cols, size_t offset_rows )
{
    pimpl_->expand( width, height, offset_cols, offset_rows );
}


void GameOfLifeEngine::crop( size_t row, size_t col,
                             size_t width, size_t height )
{
    pimpl_->crop( row, col, width, height );
}


void GameOfLifeEngine::save_world( const char* filename, bool overwrite ) const
{
    pimpl_->save_world( filename, overwrite );
}


void GameOfLifeEngine::save_world( const char* filename,
                                   size_t row, size_t col,
                                   size_t width, size_t height,
                                   bool overwrite ) const
{
    return pimpl_->save_world( filename, row, col, width, height, overwrite );
}


void GameOfLifeEngine::load_world( std::string_view filename )
{
    pimpl_->load_world( filename );
}


size_t GameOfLifeEngine::rows() const
{
    return pimpl_->rows();
}


size_t GameOfLifeEngine::cols() const
{
    return pimpl_->cols();
}


size_t GameOfLifeEngine::size() const
{
    return pimpl_->size();
}


WorldTopology GameOfLifeEngine::get_topology() const
{
    return pimpl_->get_topology();
}

void GameOfLifeEngine::set_topology( WorldTopology topology )
{
    return pimpl_->set_topology( topology );
}


int GameOfLifeEngine::frame_period() const
{
    return pimpl_->frame_period();
}


void GameOfLifeEngine::set_frame_period( int ms )
{
    pimpl_->set_frame_period( ms );
}


void GameOfLifeEngine::kill()
{
    pimpl_->kill();
}


void GameOfLifeEngine::kill( size_t row, size_t col,
                             size_t width, size_t height )
{
    pimpl_->kill( row, col, width, height );
}


void GameOfLifeEngine::resurrect()
{
    pimpl_->resurrect();
}


void GameOfLifeEngine::resurrect( size_t row, size_t col,
                                  size_t width, size_t height )
{
    pimpl_->resurrect( row, col, width, height );
}


void GameOfLifeEngine::populate_randomly()
{
    pimpl_->populate_randomly();
}


void GameOfLifeEngine::populate_randomly( size_t row, size_t col,
                                          size_t width, size_t height )

{
    pimpl_->populate_randomly( row, col, width, height );
}


size_t GameOfLifeEngine::population_count() const
{
    return pimpl_->population_count();
}


double GameOfLifeEngine::population_density() const
{
    return pimpl_->population_density();
}


double GameOfLifeEngine::populate_alive_ratio() const
{
    return pimpl_->populate_alive_ratio();
}


void GameOfLifeEngine::set_populate_alive_ratio( double alive_ratio )
{
    pimpl_->set_populate_alive_ratio( alive_ratio );
}


bool GameOfLifeEngine::is_cell_alive( size_t row,
                                      size_t col ) const
{
    return pimpl_->is_cell_alive( row, col );
}


void GameOfLifeEngine::set_cell_alive( size_t row,
                                       size_t col,
                                       bool alive )
{
    pimpl_->set_cell_alive( row, col, alive );
}


size_t GameOfLifeEngine::count_living_neighbors( size_t row,
                                                 size_t col ) const

{
    return pimpl_->count_living_neighbors( row, col );
}


bool GameOfLifeEngine::is_resettable_to_seed_generation() const
{
    return pimpl_->is_resettable_to_seed_generation();
}


void GameOfLifeEngine::reset_to_seed_generation()
{
    return pimpl_->reset_to_seed_generation();
}


bool GameOfLifeEngine::is_running() const
{
    return pimpl_->is_running();
}


size_t GameOfLifeEngine::generation() const
{
    return pimpl_->generation();
}


void GameOfLifeEngine::start_single_generation( std::function<void()> callback )
{
    pimpl_->start_single_generation( callback );
}


void GameOfLifeEngine::start_continuous_generation( std::function<void()> callback )
{
    pimpl_->start_continuous_generation( callback );
}


bool GameOfLifeEngine::has_generation_ready() const
{
    return pimpl_->has_generation_ready();
}


void GameOfLifeEngine::stop_continuous_generation()
{
    pimpl_->stop_continuous_generation();
}


void GameOfLifeEngine::flip_generations()
{
    pimpl_->flip_generations();
}

}  // namespace game_of_life
