/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAME_OF_LIFE_CONWAYWORLD_H_INCLUDED
#define GAME_OF_LIFE_CONWAYWORLD_H_INCLUDED

#include <cstddef>
#include <memory>
#include <filesystem>
#include <functional>
#include <future>
#include <string_view>
#include <cstdint>
#include <random>
#include <vector>

#include <boost/gil.hpp>

#include <GameOfLifeEngine/WorldTopology.h>

namespace game_of_life {

class ConwayWorld {
public:
    // Creates ConwayWorld where all cells are dead
    ConwayWorld( size_t rows,
                 size_t cols );

    // Creates ConwayWorld and fills it randomly with a living cells.
    ConwayWorld( size_t rows,
                 size_t cols,
                 double alive_ratio );

    // Loads existing ConwayWorld from file with name filename
    ConwayWorld( std::string_view filename );

    ConwayWorld( ConwayWorld const& other );
    ConwayWorld& operator=( ConwayWorld const& other ) = delete;

    ConwayWorld( ConwayWorld&& other ) = delete;
    ConwayWorld& operator=( ConwayWorld&& other ) = delete;

    ~ConwayWorld();

    // Stores ConwayWorld to file with name filename
    void store( const char* filename, bool overwrite = false ) const;

    // Stores ConwayWorld selection to file with name filename
    void store( const char* filename,
                size_t row, size_t col,
                size_t width, size_t height,
                bool overwrite = false ) const;

    static bool is_valid_extension( const std::string_view extension );

    // Returns the number of ConwayWorld rows
    size_t rows() const;

    // Returns the number of ConwayWorld columns
    size_t cols() const;

    // Returns the ConwayWorld size
    size_t size() const;

    // Kills all cells in the life world
    void kill();

    // Kills all cells in the specified area
    void kill( size_t row, size_t col,
               size_t width, size_t height );

    // Resurrects all cells in the life world
    void resurrect();

    // Resurrects all cells in the specified area
    void resurrect( size_t row, size_t col,
                    size_t width, size_t height );

    // Populates the whole life world with random life trying to
    // satisfy requested alive ratio
    void populate_randomly( double alive_ratio );

    // Populates the specified part of the life world with random life trying to
    // satisfy requested alive ratio
    void populate_randomly( size_t row, size_t col,
                            size_t width, size_t height,
                            double alive_ratio );

    void copy_cells( const ConwayWorld& src,
                     size_t src_row, size_t src_col,
                     size_t src_width, size_t src_height,
                     size_t dst_row, size_t dst_col );

    /// Returns the number of alive cells. The result is not cached, therefore
    /// this operation is expensive.
    size_t population_count() const;

    // Convenience function - calculates the cells population density
    // from population count and world size
    static double population_density( size_t world_size,
                                      size_t population_count );

    // Returns true if the cell at position (row, col) is alive,
    // otherwise returns false.
    bool is_cell_alive( size_t row,
                        size_t col ) const;

    // Set the state of the cell at position (row, col)
    void set_cell_alive( size_t row,
                         size_t col,
                         bool alive );

    // Counts the living neighbors of the cell at position (row, col)
    size_t count_living_neighbors( size_t row,
                                   size_t col ) const;

    // Returns the world's topology
    WorldTopology get_topology() const;

    // Sets the world topology
    void set_topology( WorldTopology topology );

    // Evolves the current ConwayWorld into the ConwayWorld next.
    // The ConwayWorld next must have the same dimensions as the current.
    void evolve( ConwayWorld& next ) const;

    void rng_seed( std::random_device& rd );
    void rng_seed( std::uint_fast32_t seed );

private:
    using cells_image_t = boost::gil::gray8_image_t;
    using cell_t = boost::gil::gray8_pixel_t;
    using cells_view_t = cells_image_t::view_t;
    using cells_const_view_t = cells_image_t::const_view_t;
    using cells_loc_t = boost::gil::gray8_loc_t;

    // The cell image channel type
    using cell_value_t = boost::gil::channel_type<cells_view_t::value_type>::type;

private:
    void form_bounded_plain_world_topology();

    void form_torus_world_topology();

    void form_klein_bottle_horiz_twist_world_topology();

    void form_klein_bottle_vert_twist_world_topology();

    void form_cross_surface_world_topology();

    void form_sphere_world_topology();

    std::function<void()> get_topology_maker( WorldTopology topology );

    std::filesystem::path check_out_file_and_add_extension( const char* filename,
                                                            bool overwrite ) const;

    uint_fast16_t sum_living_neighbors( const cells_loc_t& loc ) const;

private:
    // We keep our cells in this image
    cells_image_t cells_image_;

    // A shrunk down by one cell in each direction view of the cells_image_
    cells_view_t cells_field_;

    cells_view_t outside_top_boundary_;
    cells_view_t outside_bottom_boundary_;
    cells_view_t outside_left_boundary_;
    cells_view_t outside_right_boundary_;

    cells_view_t field_top_boundary_;
    cells_view_t field_bottom_boundary_;
    cells_view_t field_left_boundary_;
    cells_view_t field_right_boundary_;

    WorldTopology topology_ = WorldTopology::DEFAULT;
    std::function<void()> world_topology_maker_;

    // Neighbours cached locations within cells_field_
    cells_loc_t::cached_location_t cells_field_cl_topleft_;
    cells_loc_t::cached_location_t cells_field_cl_top_;
    cells_loc_t::cached_location_t cells_field_cl_topright_;
    cells_loc_t::cached_location_t cells_field_cl_left_;
    cells_loc_t::cached_location_t cells_field_cl_right_;
    cells_loc_t::cached_location_t cells_field_cl_bottomleft_;
    cells_loc_t::cached_location_t cells_field_cl_bottom_;
    cells_loc_t::cached_location_t cells_field_cl_bottomright_;

    // Mersenne Twister 19937 random number generator
    std::mt19937 rng_;

    // Cell dead value. Note that current implementation assumes that this
    // value is zero.
    static constexpr cell_value_t CELL_DEAD_VALUE = 0;
    // Cell alive value. Note that current implementation assumes that this
    // is a non-zero value.
    static constexpr cell_value_t CELL_ALIVE_VALUE = 255;
};


inline double ConwayWorld::population_density( size_t world_size,
                                               size_t population_count )
{
    auto density = world_size > 0 ?
                   static_cast<double>( population_count ) /
                   static_cast<double>( world_size ) : 0.0;

    return density;
}

}  // namespace game_of_life

#endif // GAME_OF_LIFE_CONWAYWORLD_H_INCLUDED
