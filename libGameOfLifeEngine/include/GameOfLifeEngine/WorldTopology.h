/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAME_OF_LIFE_WORLDTOPOLOGY_H_INCLUDED
#define GAME_OF_LIFE_WORLDTOPOLOGY_H_INCLUDED

namespace game_of_life {

// Supported World Topologies
enum class WorldTopology {
    BOUNDED_PLANE,
    TORUS,
    KLEIN_BOTTLE_HORIZ_TWIST,
    KLEIN_BOTTLE_VERT_TWIST,
    CROSS_SURFACE,
//        SPHERE,       // It is possible only for worlds with square dimentions
    DEFAULT = TORUS
};

}  // namespace game_of_life

#endif // GAME_OF_LIFE_WORLDTOPOLOGY_H_INCLUDED
