/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAME_OF_LIFE_GAMEOFLIFEENGINE_H_INCLUDED
#define GAME_OF_LIFE_GAMEOFLIFEENGINE_H_INCLUDED

#include <cstddef>
#include <memory>
#include <functional>
#include <future>
#include <string_view>
#include <cstdint>
#include <random>
#include <vector>

#include <GameOfLifeEngine/WorldTopology.h>
#include <GameOfLifeEngine/GameOfLifeEngine_export.h>

namespace game_of_life {

class GAMEOFLIFEENGINE_EXPORT GameOfLifeEngine {
public:
    /// Constructs new GameOfLifeEngine with life world with
    /// dimensions cols x rows where all cells are dead.
    GameOfLifeEngine( size_t rows = 24,
                      size_t cols = 48 );

    GameOfLifeEngine( GameOfLifeEngine const& other ) noexcept = delete;

    GameOfLifeEngine( GameOfLifeEngine&& other ) noexcept;

    ~GameOfLifeEngine();

    GameOfLifeEngine& operator=( GameOfLifeEngine const& other ) noexcept = delete;

    GameOfLifeEngine& operator=( GameOfLifeEngine&& other ) noexcept;

    /// Resizes the engine's world and exterminates all cells.
    void resize_and_exterminate( size_t rows, size_t cols );

    /// Resizes the engine's world and populates it randomly with living cells.
    void resize_and_populate_randomly( size_t rows, size_t cols );

    /// Expands the engine's world to new dimensions while preserving the
    /// living cells.
    void expand( size_t width, size_t height,
                 size_t offset_cols, size_t offset_rows );

    /// Crops the engine's world to new dimensions while preserving the
    /// living cells within the selection.
    void crop( size_t row, size_t col,
               size_t width, size_t height );

    /// Stores the whole world to file with name filename.
    void save_world( const char* filename, bool overwrite = false ) const;

    /// Stores world selection to file with name filename.
    void save_world( const char* filename,
                     size_t row, size_t col,
                     size_t width, size_t height,
                     bool overwrite = false ) const;

    /// Loads world from file with name filename
    void load_world( std::string_view filename );

    /// Returns the number of world's rows
    size_t rows() const;

    /// Returns the number of world's columns
    size_t cols() const;

    /// Returns the world's size
    size_t size() const;

    /// Returns the world's topology
    WorldTopology get_topology() const;

    /// Sets the world topology
    void set_topology( WorldTopology topology );

    /// Returns the update period in milliseconds.
    int frame_period() const;

    /// Sets the update period in milliseconds.
    void set_frame_period( int ms );

    /// Kills all cells in the world
    void kill();

    /// Kills all cells in the specified rectangular area
    void kill( size_t row, size_t col,
               size_t width, size_t height );

    /// Resurrects all cells in the world
    void resurrect();

    /// Resurrects all cells in the specified rectangular area
    void resurrect( size_t row, size_t col,
                    size_t width, size_t height );

    /// Populates the whole life world with random life trying to
    /// achieve the alive ratio set by set_populate_alive_ratio()
    void populate_randomly();

    /// Populates the specified part of the life world with random life trying
    /// achieve the alive ratio set by set_populate_alive_ratio()
    void populate_randomly( size_t row, size_t col,
                            size_t width, size_t height );

    /// Returns the number of alive cells.
    size_t population_count() const;

    /// Returns the current population density of the world
    double population_density() const;

    /// Returns the currently set alive ratio which is used by the
    /// populate_randomly() set of functions.
    double populate_alive_ratio() const;

    /// Sets a new set alive ratio which is used by the populate_randomly()
    /// set of functions.
    void set_populate_alive_ratio( double alive_ratio );

    /// Returns true if the cell at position (row, col) is alive,
    /// otherwise returns false.
    bool is_cell_alive( size_t row,
                        size_t col ) const;

    /// Set the state of the cell at position (row, col)
    void set_cell_alive( size_t row,
                         size_t col,
                         bool alive );

    /// Counts the living neighbors of the cell at position (row, col)
    size_t count_living_neighbors( size_t row,
                                   size_t col ) const;

    bool is_resettable_to_seed_generation() const;

    void reset_to_seed_generation();

    bool is_running() const;

    /// The generation number of the current living cells population
    size_t generation() const;

    /// Initiate the computation of next living cells generation
    void start_single_generation( std::function<void()> callback );

    /// Initiate continuous computation of new living cells generations
    void start_continuous_generation( std::function<void()> callback );

    /// Returns true if the computation of next living cells generation is
    /// accomplished, otherwise returns false.
    bool has_generation_ready() const;

    void stop_continuous_generation();

    void flip_generations();

private:
    class Impl;
    std::unique_ptr<Impl> pimpl_;

};


}  // namespace game_of_life

#endif // GAME_OF_LIFE_GAMEOFLIFEENGINE_H_INCLUDED
