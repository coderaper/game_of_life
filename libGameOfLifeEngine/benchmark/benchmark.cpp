/*******************************************************************************
*  Conway's Game of Life
*  Benchmark of ConwayWorld evolve()
*  Author: Daniel Balkanski
*  File: benchmark.cpp
*******************************************************************************/
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <chrono>
#include <algorithm>

#include <GameOfLifeEngine/ConwayWorld.h>

double calc_average( const std::vector<double>& values )
{
    double average = 0.0;

    if ( values.empty() )
      return average;

    average = std::accumulate( std::cbegin( values ),
                               std::cend( values ),
                               0.0 ) / values.size();

    return average;
}


double calc_median( std::vector<double> values )
{
    assert( ! values.empty() );

    if ( values.size() == 1 )
      return values.front();

    auto div_result = std::div( values.size(), 2 );

    if ( div_result.rem == 0 ) {
        auto hi_middle_it = std::next( std::begin( values ), div_result.quot );

        std::nth_element( std::begin( values ), hi_middle_it,
                          std::end( values ) );

        const auto lo_middle_it = std::max_element( std::begin( values ),
                                                    hi_middle_it );

        return ( *lo_middle_it + *hi_middle_it ) / 2.0;
    } else {
        auto middle_it = std::next( std::begin( values ), div_result.quot );

        std::nth_element( std::begin( values ), middle_it,
                          std::end( values ) );

        return static_cast<double>( *middle_it );
    }
}


int main()
{
    static constexpr size_t rows = 2000;
    static constexpr size_t cols = 2000;

    using ConwayWorld = game_of_life::ConwayWorld;
    ConwayWorld current_world( rows, cols );
    ConwayWorld next_world( rows, cols );

    current_world.rng_seed( 1979 );
    current_world.populate_randomly( 1.0 / 3.0 );

    static constexpr size_t num_repetitions = 10;
    std::vector<double> exec_times;
    exec_times.reserve( num_repetitions );

    using namespace std::chrono;
    auto start_time = steady_clock::now();

    for ( size_t i = 0; i < num_repetitions; ++i ) {
        current_world.evolve( next_world );

        auto exec_time = steady_clock::now() - start_time;
        exec_times.emplace_back( duration_cast< duration<double> >( exec_time ).count() );
    }

    std::cout << "Execution times in seconds of "  << rows << "x" << cols
              << " ConwayWorld's evolve():" << std::endl;

    for ( const auto& exec_time : exec_times ) {
        std::cout << std::fixed << std::setprecision( 8 )
                  << exec_time << '\n';
    }
    std::cout << "Average execution time in seconds: "
               << std::fixed << calc_average( exec_times ) << std::endl;
    std::cout << "Median execution time in seconds:  "
               << std::fixed << calc_median( exec_times ) << std::endl;

    return EXIT_SUCCESS;
}
