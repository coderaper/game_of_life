//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_TEST_EXTENSION_IO_PATHS_HPP
#define BOOST_GIL_TEST_EXTENSION_IO_PATHS_HPP

#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#include <string>

// `base` holds the path to ../.., i.e. the directory containing `images`
static const std::string base =
    (boost::filesystem::absolute(boost::filesystem::path(__FILE__)).parent_path().string()) + "/";

static const std::string cells_image_in_dir  = base + "images/cells/";
static const std::string cells_image_out_dir = base + "output/cells/";

static const std::string cells_in_filename( cells_image_in_dir + "pentadecathlon.cells" );
static const std::string cells_in_filename_sparse( cells_image_in_dir + "pentadecathlon_sparse.cells" );

static const std::string cells_out_filename( cells_image_out_dir + "pentadecathlon.cells" );
static const std::string cells_out_filename_from_sparse( cells_image_out_dir + "pentadecathlon_from_sparse.cells" );

#endif // BOOST_GIL_TEST_EXTENSION_IO_PATHS_HPP
