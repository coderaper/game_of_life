//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#include <string>

#include <boost/gil/extension/io/png.hpp>
#include <boost/gil/extension/io/cells.hpp>

#include <boost/core/lightweight_test.hpp>

#include "paths.hpp"

namespace gil = boost::gil;
namespace fs = boost::filesystem;

// Test will include tested format's headers and load and write some images.
// This test is more of a compilation test.

void test_cells( const std::string& cells_in_filename,
                 const std::string& cells_out_filename )
{
    auto in_image_info = gil::read_image_info( cells_in_filename,
                                               gil::cells_tag() );

    gil::gray8_image_t img;
    gil::read_image( cells_in_filename, img, gil::cells_tag() );

    fs::create_directories( fs::path( cells_image_out_dir ) );
//    gil::write_view( cells_out_file_png,
//                     gil::view( img ), gil::png_tag() );


    gil::image_write_info< gil::cells_tag > out_image_info;
    out_image_info._comments = in_image_info._info._comments;
    gil::write_view( cells_out_filename, gil::view( img ), out_image_info );
}


void test_cells_sparse( const std::string& cells_in_filename_sparse,
                        const std::string& cells_out_filename_from_sparse )
{
    auto in_image_info = gil::read_image_info( cells_in_filename_sparse,
                                               gil::cells_tag() );

    gil::gray8_image_t img;
    gil::read_image( cells_in_filename_sparse, img, gil::cells_tag() );

    fs::create_directories( fs::path( cells_image_out_dir ) );

    gil::image_write_info< gil::cells_tag > out_image_info;
    out_image_info._comments = in_image_info._info._comments;
    gil::write_view( cells_out_filename_from_sparse, gil::view( img ), out_image_info );
}


int main( int argc, char* argv[] )
{
    static const std::string cells_in_filename( cells_image_in_dir + "pentadecathlon.cells" );
    static const std::string cells_in_filename_sparse( cells_image_in_dir + "pentadecathlon_sparse.cells" );

    static const std::string cells_out_filename( cells_image_out_dir + "pentadecathlon.cells" );
    static const std::string cells_out_filename_from_sparse( cells_image_out_dir + "pentadecathlon_from_sparse.cells" );

    try
    {
        test_cells( cells_in_filename,
                    cells_out_filename );

        test_cells_sparse( cells_in_filename_sparse,
                           cells_out_filename_from_sparse );
    }
    catch ( std::exception const& ex )
    {
        BOOST_ERROR( ex.what() );
    }
    return boost::report_errors();
}
