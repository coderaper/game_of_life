//
// Copyright 2013 Christian Henning
// Copyright 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#include <boost/gil.hpp>
#include <boost/gil/extension/io/cells.hpp>

#include <boost/mp11.hpp>
#include <boost/core/lightweight_test.hpp>

#include <fstream>
#include <sstream>
#include <string>

#include "paths.hpp"
#include "subimage_test.hpp"

namespace gil = boost::gil;
namespace mp11 = boost::mp11;

#ifdef BOOST_GIL_IO_TEST_ALLOW_READING_IMAGES
void test_read_image_info_using_string()
{
    {
        using backend_t   = gil::get_reader_backend<std::string const, gil::cells_tag>::type;
        backend_t backend = gil::read_image_info(cells_in_filename, gil::cells_tag());

        BOOST_TEST_EQ(backend._info._width, 61u);
        BOOST_TEST_EQ(backend._info._height, 24u);
    }
    {
        std::ifstream in(cells_in_filename.c_str(), std::ios::binary);

        using backend_t   = gil::get_reader_backend<std::ifstream, gil::cells_tag>::type;
        backend_t backend = gil::read_image_info(in, gil::cells_tag());

        BOOST_TEST_EQ(backend._info._width, 61u);
        BOOST_TEST_EQ(backend._info._height, 24u);
    }
    {
        FILE* file = fopen(cells_in_filename.c_str(), "rb");

        using backend_t   = gil::get_reader_backend<FILE*, gil::cells_tag>::type;
        backend_t backend = gil::read_image_info(file, gil::cells_tag());

        BOOST_TEST_EQ(backend._info._width, 61u);
        BOOST_TEST_EQ(backend._info._height, 24u);
    }
}

void test_read_image()
{
    {
        gil::gray8_image_t img;
        gil::read_image(cells_in_filename, img, gil::cells_tag());

        BOOST_TEST_EQ(img.width(), 61u);
        BOOST_TEST_EQ(img.height(), 24u);
    }
    {
        std::ifstream in(cells_in_filename.c_str(), std::ios::binary);

        gil::gray8_image_t img;
        gil::read_image(in, img, gil::cells_tag());

        BOOST_TEST_EQ(img.width(), 61u);
        BOOST_TEST_EQ(img.height(), 24u);
    }
    {
        FILE* file = fopen(cells_in_filename.c_str(), "rb");

        gil::gray8_image_t img;
        gil::read_image(file, img, gil::cells_tag());

        BOOST_TEST_EQ(img.width(), 61u);
        BOOST_TEST_EQ(img.height(), 24u);
    }
}

void test_read_and_convert_image()
{
    {
        gil::gray8_image_t img;
        gil::read_and_convert_image(cells_in_filename, img, gil::cells_tag());

        BOOST_TEST_EQ(img.width(), 61u);
        BOOST_TEST_EQ(img.height(), 24u);
    }
    {
        std::ifstream in(cells_in_filename.c_str(), std::ios::binary);

        gil::gray8_image_t img;
        gil::read_and_convert_image(in, img, gil::cells_tag());

        BOOST_TEST_EQ(img.width(), 61u);
        BOOST_TEST_EQ(img.height(), 24u);
    }
    {
        FILE* file = fopen(cells_in_filename.c_str(), "rb");

        gil::gray8_image_t img;
        gil::read_and_convert_image(file, img, gil::cells_tag());

        BOOST_TEST_EQ(img.width(), 61u);
        BOOST_TEST_EQ(img.height(), 24u);
    }
}

void test_read_view()
{
    {
        gil::gray8_image_t img(61, 24);
        gil::read_view(cells_in_filename, gil::view(img), gil::cells_tag());
    }
    {
        std::ifstream in(cells_in_filename.c_str(), std::ios::binary);

        gil::gray8_image_t img(61, 24);
        gil::read_view(in, gil::view(img), gil::cells_tag());
    }
    {
        FILE* file = fopen(cells_in_filename.c_str(), "rb");

        gil::gray8_image_t img(61, 24);
        gil::read_view(file, gil::view(img), gil::cells_tag());
    }
}

void test_read_and_convert_view()
{
    {
        gil::gray8_image_t img(61, 24);
        gil::read_and_convert_view(cells_in_filename, gil::view(img), gil::cells_tag());
    }
    {
        std::ifstream in(cells_in_filename.c_str(), std::ios::binary);

        gil::gray8_image_t img(61, 24);
        gil::read_and_convert_view(in, gil::view(img), gil::cells_tag());
    }
    {
        FILE* file = fopen(cells_in_filename.c_str(), "rb");

        gil::gray8_image_t img(61, 24);

        gil::read_and_convert_view(file, gil::view(img), gil::cells_tag());
    }
}

void test_stream()
{
    // 1. Read an image.
    //std::ifstream in(cells_in_filename.c_str(), std::ios::binary);
    std::ifstream in(cells_in_filename.c_str());

    gil::gray8_image_t img;
    gil::read_image(in, img, gil::cells_tag());

    // 2. Write image to in-memory buffer.
//    std::stringstream out_buffer(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    std::stringstream out_buffer(std::ios_base::in | std::ios_base::out );
    gil::write_view(out_buffer, gil::view(img), gil::cells_tag());
    {
        std::string filename(cells_image_out_dir + "stream_test1.cells");
        std::ofstream out(filename.c_str(), std::ios_base::binary);
        out << out_buffer.str();
    }

    // 3. Copy in-memory buffer to another.
    std::stringstream in_buffer(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    in_buffer << out_buffer.rdbuf();

    // 4. Read in-memory buffer to gil image
    gil::gray8_image_t dst;
    gil::read_image(in_buffer, dst, gil::cells_tag());

    // 5. Write out image.
    std::string filename(cells_image_out_dir + "stream_test.cells");
    std::ofstream out(filename.c_str(), std::ios_base::binary);
#ifdef BOOST_GIL_IO_TEST_ALLOW_WRITING_IMAGES
    gil::write_view(out, gil::view(dst), gil::cells_tag());
#endif  // BOOST_GIL_IO_TEST_ALLOW_WRITING_IMAGES
}

void test_stream_2()
{
    std::filebuf in_buf;
    if (!in_buf.open(cells_in_filename.c_str(), std::ios::in | std::ios::binary))
    {
        BOOST_TEST(false);
    }

    std::istream in(&in_buf);

    gil::gray8_image_t img;
    gil::read_image(in, img, gil::cells_tag());
}

void test_subimage()
{
    run_subimage_test<gil::gray8_image_t, gil::cells_tag>(
        cells_in_filename, gil::point_t(1, 1), gil::point_t(34, 9));

    run_subimage_test<gil::gray8_image_t, gil::cells_tag>(
        cells_in_filename, gil::point_t(27, 14), gil::point_t(34, 9));
}

//void test_dynamic_image_test()
//{
//    gil::any_image
//    <
//        gil::gray8_image_t
////        gil::gray16_image_t,
////        gil::gray1_image_t
//    > image;

//    gil::read_image(cells_in_filename.c_str(), image, gil::cells_tag());

//#ifdef BOOST_GIL_IO_TEST_ALLOW_WRITING_IMAGES
//    gil::write_view(cells_image_out_dir + "dynamic_image_test.cells", gil::view(image), gil::cells_tag());
//#endif  // BOOST_GIL_IO_TEST_ALLOW_WRITING_IMAGES
//}

int main()
{
    test_read_image_info_using_string();
    test_read_image();
    test_read_and_convert_image();
    test_read_view();
    test_read_and_convert_view();
    test_stream();
    test_stream_2();
    test_subimage();
////    test_dynamic_image_test();

    return boost::report_errors();
}
#else
int main() {}
#endif // BOOST_GIL_IO_TEST_ALLOW_READING_IMAGES
