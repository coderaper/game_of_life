//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_TAGS_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_TAGS_HPP

#define BOOST_GIL_EXTENSION_IO_CELLS_READ_ENABLED // TODO: Document, explain, review

#include <cstdint>
#include <string>
#include <vector>
#include <type_traits>

#include <boost/gil/io/base.hpp>

namespace boost { namespace gil {

/// Defines cells tag.
struct cells_tag : format_tag {};

/// Defines type for bit depth method property.
struct cells_bitdepth : property_base< int > {};

/// Defines type for bit depth method property.
struct cells_color_type : property_base< int > {};

/// Defines type for image width property.
struct cells_image_width : property_base< uint32_t > {};

/// Defines type for image height property.
struct cells_image_height : property_base< uint32_t > {};

/// Defines type for comments property.
struct cells_comments : property_base< std::vector< std::string > > {};

/// Defines the cells format comments symbol
struct cells_comment_symbol : property_base< byte_t >
{
    static constexpr type value = '!';
};

/// Defines the cells format dead cell symbol
struct cells_dead_cell_symbol : property_base< byte_t >
{
    static constexpr type value = '.';
};

/// Defines the cells format living cell symbol
struct cells_living_cell_symbol : property_base< byte_t >
{
    static constexpr type value = 'O';
};


/// Cells info base class. Containing all information both for reading and writing.
///
/// This base structure was created to avoid code duplication.
struct cells_info_base
{
    /// The image comments
    cells_comments::type        _comments;
};

/// Read information for cells images.
///
/// The structure is returned when using read_image_info.
template<>
struct image_read_info< cells_tag > : public cells_info_base
{
    /// The image width.
    cells_image_width::type     _width;
    /// The image height.
    cells_image_height::type    _height;
};

/// Read settings for cells images.
///
/// The structure can be used for all read_xxx functions, except read_image_info.
template<>
struct image_read_settings< cells_tag > : public image_read_settings_base
{
    /// Default constructor
    image_read_settings< cells_tag >()
    : image_read_settings_base()
    {}

    /// Constructor
    /// \param top_left   Top left coordinate for reading partial image.
    /// \param dim        Dimensions for reading partial image.
    image_read_settings( const point_t& top_left
                       , const point_t& dim
                       )
    : image_read_settings_base( top_left
                              , dim
                              )
    {}
};

/// Write information for cells images.
///
/// The structure can be used for write_view() function.
template<>
struct image_write_info< cells_tag >  : public cells_info_base
{
};

} // namespace gil
} // namespace boost

#endif
