//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_WRITE_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_WRITE_HPP

#include <boost/gil/extension/io/cells/tags.hpp>
#include <boost/gil/extension/io/cells/detail/supported_types.hpp>
#include <boost/gil/extension/io/cells/detail/write.hpp>

#include <boost/gil/io/make_dynamic_image_writer.hpp>
#include <boost/gil/io/make_writer.hpp>
#include <boost/gil/io/write_view.hpp>

#endif
