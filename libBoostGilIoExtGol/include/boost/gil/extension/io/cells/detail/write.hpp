//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_WRITE_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_WRITE_HPP

#include <boost/gil/extension/io/cells/tags.hpp>
#include <boost/gil/extension/io/cells/detail/writer_backend.hpp>

#include <boost/gil/io/base.hpp>
#include <boost/gil/io/bit_operations.hpp>
#include <boost/gil/extension/io/cells/detail/device.hpp>
#include <boost/gil/io/dynamic_io_new.hpp>
#include <boost/gil/detail/mp11.hpp>

#include <cassert>
#include <cstdlib>
#include <string>
#include <type_traits>
#include <iterator>

namespace boost { namespace gil {

#if BOOST_WORKAROUND(BOOST_MSVC, >= 1400)
#pragma warning(push)
#pragma warning(disable:4512) //assignment operator could not be generated
#endif

namespace detail {

struct cells_write_is_supported
{
    template< typename View >
    struct apply
        : public is_write_supported< typename get_pixel_type< View >::type
                                   , cells_tag
                                   >
    {};
};

} // namespace detail

///
/// CELLS Writer
///
template< typename Device >
class writer< Device
            , cells_tag
            >
    : public writer_backend< Device
                           , cells_tag
                           >
{
private:
    using backend_t = writer_backend<Device, cells_tag>;

public:

    writer( const Device&                      io_dev
          , const image_write_info< cells_tag >& info
          )
    : backend_t( io_dev
                , info
                )
    {}

    template< typename View >
    void apply( const View& view )
    {
        using pixel_t = typename get_pixel_type<View>::type;

        std::size_t width  = view.width();
        std::size_t height = view.height();

        //static constexpr std::size_t chn = num_channels< View >::value;
        std::size_t chn = num_channels< View >::value;
        //static_assert( chn == 1, "Only views with one channel are supported" );
        std::size_t pitch = chn * width;

        // write comments
        static const std::string comment_symbol( { cells_comment_symbol::value } );
        for ( const auto& comment : this->_info._comments ) {
            this->_io_dev.print_line( comment_symbol + comment + '\n' );
        }

        // write data
        write_data( view
                  , pitch
                  , typename is_bit_aligned< pixel_t >::type()
                  );
    }

private:

//    template< int Channels >
//    unsigned int get_type( std::true_type  /* is_bit_aligned */ )
//    {
//        return mp11::mp_if_c
//            <
//                Channels == 1,
//                cells_image_type::mono_bin_t,
//                cells_image_type::color_bin_t
//            >::value;
//    }

//    template< int Channels >
//    unsigned int get_type( std::false_type /* is_bit_aligned */ )
//    {
//        return mp11::mp_if_c
//            <
//                Channels == 1,
//                cells_image_type::gray_bin_t,
//                cells_image_type::color_bin_t
//            >::value;
//    }

    template< typename View >
    void write_data( const View&   src
                   , std::size_t   pitch
                   , const std::true_type&    // bit_aligned
                   )
    {
        static_assert(std::is_same<View, typename gray1_image_t::view_t>::value, "");

        byte_vector_t row( pitch / 8 );

        using x_it_t = typename View::x_iterator;
        x_it_t row_it = x_it_t( &( *row.begin() ));

        detail::negate_bits<byte_vector_t, std::true_type> negate;
        detail::mirror_bits<byte_vector_t, std::true_type> mirror;
        for (typename View::y_coord_t y = 0; y < src.height(); ++y)
        {
            std::copy(src.row_begin(y), src.row_end(y), row_it);

            mirror(row);
            negate(row);

            this->_io_dev.write(&row.front(), pitch / 8);
        }
    }

    template< typename View >
    void write_data( const View&   src
                   , std::size_t   pitch
                   , const std::false_type&    // bit_aligned
                   )
    {
        using out_pixel_t = pixel< typename channel_type< View >::type,
                                   layout<typename color_space_type< View >::type > >;

        //std::vector<out_pixel_t> buf( src.width() );
        std::vector<out_pixel_t> buff;
        buff.reserve( src.width() + 1 );

        // using pixel_t = typename View::value_type;
        // using view_t = typename view_type_from_pixel< pixel_t >::type;

        //view_t row = interleaved_view( src.width()
        //                             , 1
        //                             , reinterpret_cast< pixel_t* >( &buf.front() )
        //                             , pitch
        //                             );

        for( typename View::y_coord_t y = 0; y < src.height(); ++y )
        {
            //copy_pixels( subimage_view( src
            //                          , 0
            //                          , (int) y
            //                          , (int) src.width()
            //                          , 1
            //                          )
            //           , row
            //           );

//            std::copy( src.row_begin( y )
//                     , src.row_end( y )
//                     , buf.begin()
//                     );
            auto pixel_transform = []( const out_pixel_t& p ) -> out_pixel_t {
                return boost::gil::at_c<0>( p ) == 0 ?
                       out_pixel_t{ cells_dead_cell_symbol::value } :
                       out_pixel_t{ cells_living_cell_symbol::value };
            };
            std::transform( src.row_begin( y ), src.row_end( y ),
                            std::back_insert_iterator< std::vector<out_pixel_t> >( buff ),
                            pixel_transform );
            buff.emplace_back( '\n' );

            byte_t* buff_data = reinterpret_cast< byte_t* >( buff.data() );
            this->_io_dev.write( buff_data, buff.size() );
            buff.clear();
        }
    }
};

///
/// CELLS Writer
///
template< typename Device >
class dynamic_image_writer< Device
                          , cells_tag
                          >
    : public writer< Device
                   , cells_tag
                   >
{
    using parent_t = writer<Device, cells_tag>;

public:

    dynamic_image_writer( const Device&                      io_dev
                        , const image_write_info< cells_tag >& info
                        )
    : parent_t( io_dev
              , info
              )
    {}

    template< typename Views >
    void apply( const any_image_view< Views >& views )
    {
        detail::dynamic_io_fnobj< detail::cells_write_is_supported
                                , parent_t
                                > op( this );

        apply_operation( views, op );
    }
};

#if BOOST_WORKAROUND(BOOST_MSVC, >= 1400)
#pragma warning(pop)
#endif

} // gil
} // boost

#endif
