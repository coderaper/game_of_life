//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_READER_BACKEND_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_READER_BACKEND_HPP

#include <boost/gil/extension/io/cells/tags.hpp>

namespace boost { namespace gil {

#if BOOST_WORKAROUND(BOOST_MSVC, >= 1400)
#pragma warning(push)
#pragma warning(disable:4512) //assignment operator could not be generated
#endif

///
/// CELLS Backend
///
template< typename Device >
struct reader_backend< Device
                     , cells_tag
                     >
{
public:

    using format_tag_t = cells_tag;

public:

    reader_backend( const Device&                           io_dev
                  , const image_read_settings< cells_tag >& settings
                  )
    : _io_dev  ( io_dev   )
    , _settings( settings )
    , _info()

    , _scanline_length( 0 )
    {
        extract_image_info();

        if( _settings._dim.x == 0 )
        {
            _settings._dim.x = _info._width;
        }

        if( _settings._dim.y == 0 )
        {
            _settings._dim.y = _info._height;
        }
    }

    void extract_image_info()
    {
        bool is_line_comment = false;
        std::string comment;
        uint32_t line_length = 0;
        int ch;
        while ( ch = _io_dev.getc_unchecked(), ch != EOF ) {
            if ( ch == '\r' || ch == '\n' ) {
                if ( line_length > _info._width ) {
                    _info._width = line_length;
                }
                if ( is_line_comment ) {
                    _info._comments.emplace_back( std::move( comment ) );
                    // after move the source object is left in valid, but unspecified
                    // state, therefore we do not assume that it is empty.
                    comment.clear();
                }
                else {
                    ++_info._height;
                }
                is_line_comment = false;
                line_length = 0;
                continue;
            }

            if ( is_line_comment ) {
                comment.push_back( static_cast<char>( ch ) );
            }
            else {
                if ( ch == cells_comment_symbol::value ) {
                    if ( line_length != 0 ) {
                        std::string msg{
                            "Line " + std::to_string( _info._height ) +
                            ": comment sign ! is not at the beginning of the line"
                        };
                        io_error( msg.c_str() );
                    }
                    is_line_comment = true;
                }
                else {
                    ++line_length;
                    if ( ch != cells_dead_cell_symbol::value &&
                         ch != cells_living_cell_symbol::value ) {
                        std::string msg{
                            "Line " + std::to_string( _info._height ) +
                            ": invalid character '" +
                            std::to_string( { static_cast<char>( ch ) } ) +
                            "' at position " + std::to_string( line_length )
                        };
                        io_error( msg.c_str() );
                    }
                }
            }
        }

        seek_back_to_first_scanline();
    }

    /// Check if image is large enough.
    void check_image_size( const point_t& img_dim )
    {
        if( _settings._dim.x > 0 )
        {
            if( img_dim.x < _settings._dim.x ) { io_error( "Supplied image is too small" ); }
        }
        else
        {
            if( img_dim.x < _info._width ) { io_error( "Supplied image is too small" ); }
        }


        if( _settings._dim.y > 0 )
        {
            if( img_dim.y < _settings._dim.y ) { io_error( "Supplied image is too small" ); }
        }
        else
        {
            if( img_dim.y < _info._height ) { io_error( "Supplied image is too small" ); }
        }
    }

private:

    // We do not make any integrity checks here becase they where already
    // made by extract_image_info()
    void seek_back_to_first_scanline()
    {
        _io_dev.seek( 0, SEEK_SET );
        int ch;
        while ( ch = _io_dev.getc_unchecked(), ch != EOF ) {
            if ( ch == cells_comment_symbol::value )
            {
                // skip comment to EOL
                do
                {
                    ch = _io_dev.getc_unchecked();
                    if ( ch == EOF ) {
                        return;
                    }
                }
                while ( ch != '\r' && ch != '\n' );
            }
            else {
                break;
            }
        }
        _io_dev.seek( -1, SEEK_CUR );
    }


public:

    Device _io_dev;

    image_read_settings< cells_tag > _settings;
    image_read_info< cells_tag >     _info;

    std::size_t _scanline_length;
};

#if BOOST_WORKAROUND(BOOST_MSVC, >= 1400)
#pragma warning(pop)
#endif

} // namespace gil
} // namespace boost

#endif
