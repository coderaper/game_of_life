//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_READ_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_READ_HPP

#include <boost/gil/extension/io/cells/tags.hpp>
#include <boost/gil/extension/io/cells/detail/reader_backend.hpp>
#include <boost/gil/extension/io/cells/detail/is_allowed.hpp>

#include <boost/gil.hpp> // FIXME: Include what you use!
#include <boost/gil/io/base.hpp>
#include <boost/gil/io/bit_operations.hpp>
#include <boost/gil/io/conversion_policies.hpp>
#include <boost/gil/extension/io/cells/detail/device.hpp>
#include <boost/gil/io/dynamic_io_new.hpp>
#include <boost/gil/io/reader_base.hpp>
#include <boost/gil/io/row_buffer_helper.hpp>
#include <boost/gil/io/typedefs.hpp>

#include <type_traits>
#include <vector>

namespace boost { namespace gil {

#if BOOST_WORKAROUND(BOOST_MSVC, >= 1400)
#pragma warning(push)
#pragma warning(disable:4512) //assignment operator could not be generated
#endif

///
/// CELLS Reader
///
template< typename Device
        , typename ConversionPolicy
        >
class reader< Device
            , cells_tag
            , ConversionPolicy
            >
    : public reader_base< cells_tag
                        , ConversionPolicy
                        >
    , public reader_backend< Device
                           , cells_tag
                           >
{

private:

    using this_t = reader<Device, cells_tag, ConversionPolicy>;
    using cc_t = typename ConversionPolicy::color_converter_type;

public:

    using backend_t = reader_backend<Device, cells_tag>;

    reader( const Device&                         io_dev
          , const image_read_settings< cells_tag >& settings
          )
    : reader_base< cells_tag
                 , ConversionPolicy
                 >()
    , backend_t( io_dev
               , settings
               )
    {}

    reader( const Device&                         io_dev
          , const cc_t&                           cc
          , const image_read_settings< cells_tag >& settings
          )
    : reader_base< cells_tag
                 , ConversionPolicy
                 >( cc )
    , backend_t( io_dev
               , settings
               )
    {}

    template<typename View>
    void apply( const View& view )
    {
        using is_read_and_convert_t = typename std::is_same
            <
                ConversionPolicy,
                detail::read_and_no_convert
            >::type;

        io_error_if( !detail::is_allowed< View >( this->_info
                                                , is_read_and_convert_t()
                                                )
                   , "Image types aren't compatible."
                   );

        this->_scanline_length = this->_info._width;
        read_text_data< gray8_view_t >( view );
    }

private:

    template< typename View_Src
            , typename View_Dst
            >
    void read_text_data( const View_Dst& dst )
    {
        using y_t = typename View_Dst::y_coord_t;

        byte_vector_t row( this->_scanline_length );

        //Skip scanlines if necessary.
        for( int y = 0; y <  this->_settings._top_left.y; ++y )
        {
            read_text_row< View_Src >( dst, row, y, false );
        }

        for( y_t y = 0; y < dst.height(); ++y )
        {
            read_text_row< View_Src >( dst, row, y, true );
        }
    }

    template< typename View_Src
            , typename View_Dst
            >
    void read_text_row( const View_Dst&              dst
                      , byte_vector_t&               row
                      , typename View_Dst::y_coord_t y
                      , bool                         process
                      )
    {
        View_Src src = interleaved_view( this->_info._width
                                       , 1
                                       , (typename View_Src::value_type*) row.data()
                                       , this->_scanline_length
                                       );

        using channel_t = typename channel_type<typename get_pixel_type<View_Dst>::type>::type;

        int ch;
        uint32_t x = 0;
        for( ; x < this->_scanline_length; ++x )
        {
            ch = this->_io_dev.getc_unchecked();
            if( ch == EOF ||  ch == '\r' || ch == '\n' )  {
                break;
            }

            if( process )
            {
                if ( ch != cells_dead_cell_symbol::value &&
                     ch != cells_living_cell_symbol::value )
                {
                    std::string msg{
                        "found invalid scanline character '" +
                        std::to_string( { static_cast<char>( ch ) } ) + "'"
                    };
                    io_error( msg.c_str() );
                }
                row[x] = ch == cells_dead_cell_symbol::value ?
                         typename channel_traits< channel_t >::value_type( 0 ) :
                         channel_traits< channel_t >::max_value();
            }
        }

        if ( x == this->_scanline_length ) {
            // full sized line, drop the EOL symbol
            ch = this->_io_dev.getc_unchecked();
        }

        if( process )
        {
            // fill the rest of the row with dead cells
            for( ; x < this->_scanline_length; ++x ) {
                row[x] = typename channel_traits< channel_t >::value_type( 0 );
            }

            // We are reading a gray1_image like a gray8_image but the two pixel_t
            // aren't compatible. Though, read_and_no_convert::read(...) wont work.
            copy_data< View_Dst
                     , View_Src >( dst
                                 , src
                                 , y
                                 , typename std::is_same< View_Dst
                                                   , gray1_image_t::view_t
                                                   >::type()
                                 );
        }
    }

    template< typename View_Dst
            , typename View_Src
            >
    void copy_data( const View_Dst&              dst
                  , const View_Src&              src
                  , typename View_Dst::y_coord_t y
                  , std::true_type // is gray1_view
                  )
    {
        if(  this->_info._max_value == 1 )
        {
            typename View_Dst::x_iterator it = dst.row_begin( y );

            for( typename View_Dst::x_coord_t x = 0
               ; x < dst.width()
               ; ++x
               )
            {
                it[x] = src[x];
            }
        }
        else
        {
            copy_data(dst, src, y, std::false_type{});
        }
    }

    template< typename View_Dst
            , typename View_Src
            >
    void copy_data( const View_Dst&              view
                  , const View_Src&              src
                  , typename View_Dst::y_coord_t y
                  , std::false_type // is gray1_view
                  )
    {
        typename View_Src::x_iterator beg = src.row_begin( 0 ) + this->_settings._top_left.x;
        typename View_Src::x_iterator end = beg + this->_settings._dim.x;

        this->_cc_policy.read( beg
                             , end
                             , view.row_begin( y )
                             );
    }


private:

    char buf[16];

};


namespace detail {

struct cells_type_format_checker
{
    cells_type_format_checker( cells_bitdepth::type   bit_depth
                             , cells_color_type::type color_type
                             )
    : _bit_depth ( bit_depth  )
    , _color_type( color_type )
    {}

    template< typename Image >
    bool apply()
    {
        using is_supported_t = is_read_supported
            <
                typename get_pixel_type<typename Image::view_t>::type,
                cells_tag
            >;

        return is_supported_t::_bit_depth  == _bit_depth
            && is_supported_t::_color_type == _color_type;
    }

private:

    cells_bitdepth::type   _bit_depth;
    cells_color_type::type _color_type;
};

struct cells_read_is_supported
{
    template< typename View >
    struct apply : public is_read_supported< typename get_pixel_type< View >::type
                                           , cells_tag
                                           >
    {};
};

} // namespace detail

///
/// CELLS Dynamic Image Reader
///
template< typename Device
        >
class dynamic_image_reader< Device
                          , cells_tag
                          >
    : public reader< Device
                   , cells_tag
                   , detail::read_and_no_convert
                   >
{
    using parent_t = reader
        <
            Device,
            cells_tag,
            detail::read_and_no_convert
        >;

public:

    dynamic_image_reader( const Device&                         io_dev
                        , const image_read_settings< cells_tag >& settings
                        )
    : parent_t( io_dev
              , settings
              )
    {}

    template< typename Images >
    void apply( any_image< Images >& images )
    {
        detail::cells_type_format_checker format_checker( this->_info._bit_depth
                                                        , this->_info._color_type
                                                        );

        if( !construct_matched( images
                              , format_checker
                              ))
        {
            io_error( "No matching image type between those of the given any_image and that of the file" );
        }
        else
        {
            this->init_image( images
                            , this->_settings
                            );

            detail::dynamic_io_fnobj< detail::cells_read_is_supported
                                    , parent_t
                                    > op( this );

            apply_operation( view( images )
                           , op
                           );
        }
    }
};

#if BOOST_WORKAROUND(BOOST_MSVC, >= 1400)
#pragma warning(pop)
#endif

} // gil
} // boost

#endif
