//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_SUPPORTED_TYPES_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_SUPPORTED_TYPES_HPP

#include <boost/gil/extension/io/cells/tags.hpp>

#include <boost/gil/channel.hpp>
#include <boost/gil/color_base.hpp>
#include <boost/gil/io/base.hpp>

#include <type_traits>

namespace boost { namespace gil { namespace detail {

// Read Support
template< cells_bitdepth::type   BitDepth
        , cells_color_type::type ColorType
        >
struct cells_rw_support_base
{
    static const cells_bitdepth::type   _bit_depth  = BitDepth;
    static const cells_color_type::type _color_type = ColorType;
};

template< typename Channel
        , typename ColorSpace
        >
struct cells_read_support : read_support_false
                          , cells_rw_support_base< 0
                                                 , 0
                                                 > {};


template<>
struct cells_read_support<uint8_t
                         , gray_t
                         > : read_support_true
                           , cells_rw_support_base< 1
                                                  , 8
                                                  > {};

// Write support

template< typename Channel
        , typename ColorSpace
        >
struct cells_write_support : write_support_false
{};


template<>
struct cells_write_support<uint8_t
                         , gray_t
                         > : write_support_true
                           , cells_rw_support_base< 1
                                                  , 8
                                                  > {};

} // namespace detail

template<typename Pixel>
struct is_read_supported<Pixel, cells_tag>
    : std::integral_constant
    <
        bool,
        detail::cells_read_support
        <
            typename channel_type<Pixel>::type,
            typename color_space_type<Pixel>::type
        >::is_supported
    >
{
    using parent_t = detail::cells_read_support
        <
            typename channel_type<Pixel>::type,
            typename color_space_type<Pixel>::type
        >;

//    static const cells_image_type::type _asc_type = parent_t::_asc_type;
//    static const cells_image_type::type _bin_type = parent_t::_bin_type;
    static const cells_bitdepth::type   _bit_depth  = parent_t::_bit_depth;
    static const cells_color_type::type _color_type = parent_t::_color_type;
};

template<typename Pixel>
struct is_write_supported<Pixel, cells_tag>
    : std::integral_constant
    <
        bool,
        detail::cells_write_support
        <
            typename channel_type<Pixel>::type,
            typename color_space_type<Pixel>::type
        >::is_supported
    >
{
    using parent_t = detail::cells_write_support
        <
            typename channel_type<Pixel>::type,
            typename color_space_type<Pixel>::type
        >;

    static const cells_bitdepth::type   _bit_depth  = parent_t::_bit_depth;
    static const cells_color_type::type _color_type = parent_t::_color_type;
};

} // namespace gil
} // namespace boost

#endif
