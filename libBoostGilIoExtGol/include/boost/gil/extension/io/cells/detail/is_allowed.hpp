//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_IS_ALLOWED_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_DETAIL_IS_ALLOWED_HPP

#include <boost/gil/extension/io/cells/tags.hpp>

#include <type_traits>

namespace boost { namespace gil { namespace detail {

template< typename View >
bool is_allowed( const image_read_info< cells_tag >& /* info */
               , std::true_type   // is read_and_no_convert
               )
{
    using pixel_t = typename get_pixel_type<View>::type;

    using channel_t = typename channel_traits<typename element_type<pixel_t>::type>::value_type;

    const auto dst_num_channels = num_channels< pixel_t >::value;
    const auto dst_bit_depth    = detail::unsigned_integral_num_bits< channel_t >::value;

    return dst_num_channels == 1 && dst_bit_depth == 8;
}

template< typename View >
bool is_allowed( const image_read_info< cells_tag >& /* info */
               , std::false_type  // is read_and_convert
               )
{
    return true;
}

} // namespace detail
} // namespace gil
} // namespace boost

#endif
