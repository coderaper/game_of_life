//
// Copyright (c) 2021 Daniel Balkanski
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_EXTENSION_IO_CELLS_HPP
#define BOOST_GIL_EXTENSION_IO_CELLS_HPP

#include <boost/gil/extension/io/cells/read.hpp>
#include <boost/gil/extension/io/cells/write.hpp>

#endif
