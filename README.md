# game_of_life

Just another simulator of the Conway's famous game of life. It is still 
work in progress, but it is already usable. Be kind with it. :)

This is an implementation in C++ language, which uses Qt GUI Toolkit.
On plafroms with Curses library an additional minimalistic Command Line
Interface (CLI) simulator is build, which utilizes the same game of life engine.

## Project Layout

**`game_of_life`** project is split down into a few sub-projects:

- [**`libStringUtils`**](./libStringUtils) - small library which contains few string manipulation utility functions.

- [**`libBoostGilIoExtGol`**](./libBoostGilIoExtGol) - Boost.Gil IO extension library for reading/writing game of life file formats.

- [**`libGameOfLifeEngine`**](./libGameOfLifeEngine) - the game of life engine library.

- [**`gameoflife-cli`**](./gameoflife-cli) - minimalistic game of life simulator with Command Line Interface. Built only on platforms with Curses.

- [**`qgameoflife`**](./qgameoflife) - GUI version of the game of life simulator.



## Supported platforms

Currently qgameoflife is tested on Windows and Linux platforms.


## Building and Installing

### Requirements
At minimum, compilation requires:

- [CMake](https://cmake.org/) — version 3.16 or later, and eventually [`ninja-build`](https://ninja-build.org/) or ``make``
- A C++ compiler with C++17 support (e.g. gcc/g++ >= 7, Clang >= 5, Visual Studio >= 2017 ). Visual Studio 2017 (15.7+) has already built-in support of CMake projects. 
- [Boost.Gil](https://www.boost.org/doc/libs/1_73_0/libs/gil/doc/html/index.html) library version 1.73 or later.
- libpng library.
- Qt5 GUI toolkit.


## Screenshots

### qgameoflife screenshot on Linux
![qgameoflife](./screenshots/qgameoflife-pulsar_p3-1.png) 

### qgameoflife screenshot on Windows
![qgameoflife](./screenshots/qgameoflife-random-1.png)


## General Help

### Cells editing

Mouse double click or [space] key toggle the cells state. Use arrow keys to navigate,
arrow keys plus Ctrl / Shift to select cells.

### Supported file  formats

For now the `qgameoflife` and `gameoflife-cli` support the popular [`plaintext`](https://conwaylife.com/wiki/Plaintext) file format (which uses the .cells file extension) and 8-bit grayscale .png format.

