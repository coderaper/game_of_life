/**
 Various string manipulation utility functions

 Copyright (c) 2020-2021, Daniel Balkanski

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stringutils/stringutils.h>

namespace stringutils {

std::vector<std::string> tokenize_string( const std::string& str,
                                          const std::string& delimiters,
                                          bool trim_empty )
{
    std::vector<std::string> tokens;

    auto length = str.length();
    std::string::size_type prev_pos = 0;
    while ( prev_pos < length + 1 )
    {
        auto pos = str.find_first_of( delimiters, prev_pos );
        if ( pos == std::string::npos ) {
            pos = length;
        }

        if ( pos != prev_pos || ! trim_empty )
            tokens.emplace_back( std::string( str.data() + prev_pos,
                                 pos - prev_pos ) );

        prev_pos = pos + 1;
    }

    return tokens;
}


std::vector<std::string> tokenize_string( const std::string& str,
                                          std::string::value_type delimiter,
                                          bool trim_empty )
{
    std::vector<std::string> tokens;

    auto length = str.length();
    std::string::size_type prev_pos = 0;
    while ( prev_pos < length + 1 )
    {
        auto pos = str.find_first_of( delimiter, prev_pos );
        if ( pos == std::string::npos ) {
            pos = length;
        }

        if ( pos != prev_pos || ! trim_empty )
            tokens.emplace_back( std::string( str.data() + prev_pos,
                                 pos - prev_pos ) );

        prev_pos = pos + 1;
    }

    return tokens;
}

}  // namespace stringutils
