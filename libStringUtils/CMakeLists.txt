cmake_minimum_required( VERSION 3.13 FATAL_ERROR  )

project( StringUtils VERSION 1.0.0 LANGUAGES CXX )

string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPERCASE)
string(TOLOWER ${PROJECT_NAME} PROJECT_NAME_LOWERCASE)

set( ${PROJECT_NAME}_PUBLIC_HEADERS
  include/stringutils/stringutils.h
)

# The library name is a lowercase project name
set( LIBRARY_NAME ${PROJECT_NAME_LOWERCASE} )
set( LIBRARY_EXPORT_NAME ${PROJECT_NAME} )
set( EXPORT_NAMESPACE ${PROJECT_NAME} )

add_library( ${LIBRARY_NAME}
  ${${PROJECT_NAME}_PUBLIC_HEADERS}
  src/stringutils.cpp
)

add_library( ${EXPORT_NAMESPACE}::${LIBRARY_EXPORT_NAME} ALIAS ${LIBRARY_NAME} )

# Set library target properties
set_target_properties( ${LIBRARY_NAME}
  PROPERTIES
	EXPORT_NAME ${LIBRARY_EXPORT_NAME}
    INTERFACE_COMPILE_FEATURES cxx_std_17
    LINKER_LANGUAGE CXX
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED TRUE
    CXX_EXTENSIONS OFF
#    POSITION_INDEPENDENT_CODE ON
#    CXX_VISIBILITY_PRESET hidden
#    VISIBILITY_INLINES_HIDDEN 1
    WINDOWS_EXPORT_ALL_SYMBOLS ON
    VERSION ${PROJECT_VERSION}
    SOVERSION "${PROJECT_VERSION_MAJOR}"
#    DEBUG_POSTFIX "_debug"
    PUBLIC_HEADER
      "${${PROJECT_NAME}_PUBLIC_HEADERS}"
)

target_include_directories( ${LIBRARY_NAME}
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
#    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src>
#    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
    $<INSTALL_INTERFACE:include}>
)


# Use GNUInstallDirs to install the components into correct locations on all platforms.
include( GNUInstallDirs )

install(
  TARGETS
    ${LIBRARY_NAME}
  EXPORT
    ${PROJECT_NAME}Targets
  RUNTIME DESTINATION
    ${CMAKE_INSTALL_BINDIR}
  LIBRARY DESTINATION
    ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION
    ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}
#  INCLUDES DESTINATION
#    ${CMAKE_INSTALL_INCLUDEDIR}
)


# Helper functions for creating config files
include( CMakePackageConfigHelpers )

source_group( "CMake Templates"
  FILES
    "${PROJECT_NAME}Config.cmake.in"
)

configure_package_config_file(
    ${PROJECT_NAME}Config.cmake.in
    ${PROJECT_BINARY_DIR}/cmake/${PROJECT_NAME}Config.cmake
  INSTALL_DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/cmake
  PATH_VARS
    CMAKE_INSTALL_INCLUDEDIR
    CMAKE_INSTALL_LIBDIR
#    CMAKE_INSTALL_BINDIR
#    CMAKE_INSTALL_SYSCONFDIR
#    CMAKE_INSTALL_DATADIR
)

write_basic_package_version_file(
  ${PROJECT_BINARY_DIR}/cmake/${PROJECT_NAME}ConfigVersion.cmake
  VERSION ${${PROJECT_NAME}_VERSION}
  COMPATIBILITY SameMajorVersion
)

# create export file which can be imported by other cmake projects
install(
  EXPORT
    ${PROJECT_NAME}Targets
  NAMESPACE
    ${EXPORT_NAMESPACE}::
  DESTINATION
    ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}Config.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/cmake/${PROJECT_NAME}ConfigVersion.cmake
  DESTINATION
    ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}/
  COMPONENT
    cmake
)
