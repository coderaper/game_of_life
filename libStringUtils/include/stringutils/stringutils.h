/**
 Various string manipulation utility functions

 Copyright (c) 2020-2021, Daniel Balkanski

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef STRINGUTILS_STRINGUTILS_H_INCLUDED
#define STRINGUTILS_STRINGUTILS_H_INCLUDED

#include <algorithm>
#include <string>
#include <cctype>
#include <locale>
#include <vector>

namespace stringutils {

/// case insensitive string compare of std::string references
inline bool iequals( const std::string& s1, const std::string& s2 )
{
    return std::equal( std::cbegin( s1 ), std::cend( s1 ),
                       std::cbegin( s2 ), std::cend( s2 ),
                       []( unsigned char ch1, unsigned char ch2 ) -> bool {
                           return std::tolower( ch1 ) == std::tolower( ch2 );
                       } );
}

/// case insensitive string compare of std::string views
inline bool iequals( const std::string_view s1, const std::string_view s2 )
{
    return std::equal( std::cbegin( s1 ), std::cend( s1 ),
                       std::cbegin( s2 ), std::cend( s2 ),
                       []( unsigned char ch1, unsigned char ch2 ) -> bool {
                           return std::tolower( ch1 ) == std::tolower( ch2 );
                       } );
}


inline std::string ltrim( const std::string& str )
{
    auto lambda_is_space = []( char ch ) {
        return std::isspace<char>( ch, std::locale::classic() );
    };

    auto it = std::find_if_not( str.cbegin(), str.cend(), lambda_is_space );

    return std::string( it, str.cend() );
}


inline std::string rtrim( const std::string& str )
{
    auto lambda_is_space = []( char ch ) {
        return std::isspace<char>( ch, std::locale::classic() );
    };

    auto it = std::find_if_not( str.crbegin(), str.crend(), lambda_is_space );

    return std::string( str.cbegin(), it.base() );
}


inline std::string trim( const std::string& str )
{
    auto lambda_is_space = []( char ch ) {
        return std::isspace<char>( ch, std::locale::classic() );
    };

    auto itl = std::find_if_not( str.cbegin(), str.cend(), lambda_is_space );

    if ( itl == str.cend() ) {
        return std::string();
    }

    auto itr = std::find_if_not( str.crbegin(), str.crend(), lambda_is_space );

    return std::string( itl, itr.base() );
}


std::vector<std::string> tokenize_string( const std::string& str,
                                          const std::string& delimiters = std::string( "," ),
                                          bool trim_empty = false );

std::vector<std::string> tokenize_string( const std::string& str,
                                          std::string::value_type delimiter = ',',
                                          bool trim_empty = false );

}  // namespace stringutils

#endif  // STRINGUTILS_STRINGUTILS_H_INCLUDED
