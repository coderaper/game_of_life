/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <cstdio>            // for printf()
#include <cstring>           // for strlen()
#include <thread>


#include <GameOfLifeEngine/GameOfLifeEngine.h>
#include <gameoflife/Terminal.h>
#include <gameoflife/GameOfLife.h>


namespace {

// Game default parameters:
constexpr size_t def_lifeworld_rows = 26;
constexpr size_t def_lifeworld_cols = 78;

constexpr double def_alive_ratio = 1.0 / 3.0;

constexpr int def_frame_period_ms{ 400 };

constexpr size_t def_num_steps_to_run = 120;

} // nameless namespace


namespace game_of_life {

GameOfLife::GameOfLife()
: engine_(  std::make_unique<GameOfLifeEngine>( def_lifeworld_rows,
                                                def_lifeworld_cols ) ),
  state_( State::IDLE ),
  cursor_row_( engine_->rows() / 2 ),
  cursor_col_( engine_->cols() / 2 ),
  num_steps_to_run_( def_num_steps_to_run ),
  terminal_( Terminal::instance() )
{
    engine_->set_populate_alive_ratio( def_alive_ratio );
    engine_->set_frame_period( def_frame_period_ms );
    engine_->populate_randomly();
}


GameOfLife::~GameOfLife() {
    Terminal::instance_delete();
}


void GameOfLife::redraw() const
{
    terminal_.clear();

    switch ( state_ )
    {
        case State::IDLE:
            terminal_.draw( *engine_ );
            terminal_.print( "[C]onfigure game  [K]ill 'am all    [S]ave world to file    [I]gnite life\n" );
            terminal_.print( "[E]dit world      [P]opulate world  [L]oad world from file  [Q]uit       " );
            terminal_.refresh();
            break;

        case State::CONFIGURATION:
            terminal_.print( "***** Game Configuration *****\n" );
            break;

        case State::SAVE_TO_FILE:
        case State::LOAD_FROM_FILE:
            terminal_.draw( *engine_ );
            break;

        case State::EDIT:
            {
                auto alive_ratio = engine_->population_density();
                terminal_.draw( *engine_, cursor_row_, cursor_col_ );
                terminal_.print( "Cells alive ratio %8.6f, "
                                 "Pos (row %3zd, col %3zd) - State: %s\n",
                                 alive_ratio, cursor_row_ + 1, cursor_col_ + 1,
                                 ( engine_->is_cell_alive( cursor_row_,
                                                           cursor_col_ ) ?
                                  "Alive" : "Dead" ) );
                terminal_.print( "Use [I],[J],[K],[L] to navigate, [space] "
                               "to toggle dead or alive, [Q] to Quit" );
                terminal_.refresh();
            }
            break;

        case State::RUN:
            {
                auto alive_ratio = engine_->population_density();
                terminal_.draw( *engine_ );
                terminal_.print( "Cells alive ratio %8.6f, %4zd steps left\n",
                                 alive_ratio, steps_to_run_left_ );
                if ( engine_->frame_period() == 0 ) {
                    terminal_.print( "Press [Q] to quit, any other key to update" );
                    terminal_.refresh();
                 } /*else {
                    terminal_.print( "\n" );
                    terminal_.refresh();
                }*/
                terminal_.refresh();
            }
            break;
    }
}


void GameOfLife::enter_configuration_state()
{
    state_ = State::CONFIGURATION;

    redraw();

    bool res;

    constexpr size_t min_rows = 2;
    constexpr size_t max_rows = 80;
    size_t rows;
    do {
        terminal_.print( "World rows (%zu - %zu), [Enter] to confirm current %zu: ",
                         min_rows, max_rows, engine_->rows() );
        terminal_.refresh();
        res = terminal_.get_number( rows, engine_->rows(), min_rows, max_rows );
    } while ( res != true );

    constexpr size_t min_cols = 2;
    constexpr size_t max_cols = 160;
    size_t cols;
    do {
        terminal_.print( "World columns (%zu - %zu), [Enter] to confirm current %zu: ",
                         min_cols, max_cols, engine_->cols() );
        terminal_.refresh();
        res = terminal_.get_number( cols, engine_->cols(), min_cols, max_cols );
    } while ( res != true );

    constexpr double min_alive_ratio = 0.0;
    constexpr double max_alive_ratio = 1.0;
    double alive_ratio;
    do {
        terminal_.print( "Cells alive ratio (%f - %f), [Enter] to confirm current %f: ",
                         min_alive_ratio, max_alive_ratio, engine_->populate_alive_ratio() );
        terminal_.refresh();
        res = terminal_.get_number( alive_ratio,
                                    engine_->populate_alive_ratio(),
                                    min_alive_ratio,
                                    max_alive_ratio );
    } while ( res != true );

    if ( engine_->populate_alive_ratio() != alive_ratio ) {
        engine_->set_populate_alive_ratio( alive_ratio );
    }

    if ( rows != engine_->rows() || cols != engine_->cols() )
    {
        engine_->resize_and_populate_randomly( rows, cols );
        engine_->populate_randomly();
    }

    constexpr ptrdiff_t min_delay_ms = 0;
    constexpr ptrdiff_t max_delay_ms = 2000;
    ptrdiff_t frame_period_ms;
    do {
        terminal_.print( "Update Delay of 0 means step by step life evolution!\n" );
        terminal_.print( "Update Delay in milliseconds (%zu - %zu), [Enter] to confirm current %zu: ",
                         min_delay_ms, max_delay_ms, engine_->frame_period() );
        terminal_.refresh();
        res = terminal_.get_number( frame_period_ms,
                                    static_cast<ptrdiff_t>( engine_->frame_period() ),
                                    min_delay_ms,
                                    max_delay_ms );
    } while ( res != true );

    if ( engine_->frame_period() != frame_period_ms ) {
        engine_->set_frame_period( frame_period_ms );
    }

    static const size_t min_num_steps_to_run = 1;
    static const size_t max_num_steps_to_run = 1000000;
    do {
        terminal_.print( "Number of steps to execute (%zu - %zu), [Enter] to confirm current %zu: ",
                         min_num_steps_to_run, max_num_steps_to_run,
                         num_steps_to_run_ );
        terminal_.refresh();
        res = terminal_.get_number( num_steps_to_run_,
                                  num_steps_to_run_,
                                  min_num_steps_to_run,
                                  max_num_steps_to_run );
    } while ( res != true );

    state_ = State::IDLE;
}


void GameOfLife::enter_save_to_file_state()
{
    state_ = State::SAVE_TO_FILE;

    redraw();

    std::string filename;
    terminal_.print( "Enter output filename, empty to abort: " );
    terminal_.refresh();
    filename = terminal_.get_line();

    if ( filename.empty() ) {
        state_ = State::IDLE;
        return;
    }

    std::string error_message;
    try {
        engine_->save_world( filename.c_str() );
        terminal_.print( "Game world was saved in file \"%s\".\n"
                         "Press key to continue... ", filename.c_str() );
        terminal_.refresh();
        terminal_.get_char();
    }
    catch ( std::exception& ex ) {
        error_message = "Error writing game world to file: ";
        error_message += ex.what();
    }
    catch ( ... )
    {
        error_message = "Error writing game world to file: '";
        error_message += filename;
        error_message += "': caught unknown exception!";
    }

    if ( ! error_message.empty() ) {
        terminal_.print( "%s", error_message.c_str() );
        terminal_.refresh();
        terminal_.get_char();
    }

    state_ = State::IDLE;
}


void GameOfLife::enter_load_from_file_state()
{
    state_ = State::LOAD_FROM_FILE;

    redraw();

    std::string filename;
    terminal_.print( "Enter input filename, empty to abort: " );
    terminal_.refresh();
    filename = terminal_.get_line();

    if ( filename.empty() ) {
        state_ = State::IDLE;
        return;
    }

    std::string error_message;
    try
    {
        engine_->load_world( filename );
    }
    catch ( std::exception& ex )
    {
        error_message = "Error loading game world from file: ";
        error_message += ex.what();
    }
    catch ( ... )
    {
        error_message = "Cannot load game world from file '";
        error_message += filename;
        error_message += "': caught unknown exception!";
    }

    if ( ! error_message.empty() ) {
        terminal_.print( "%s", error_message.c_str() );
        terminal_.refresh();
        terminal_.get_char();
    }

    state_ = State::IDLE;
}


void GameOfLife::enter_edit_state()
{
    state_ = State::EDIT;

    while ( state_ == State::EDIT ) {
        redraw();

        int ch = terminal_.get_char();
        switch ( ch ) {
            case 'q':
            case 'Q':
                state_ = State::IDLE;
                break;

            case 'j':
            case 'J':
            case KEY_LEFT:
                if ( cursor_col_ > 0 )
                    --( cursor_col_ );
                break;

            case 'l':
            case 'L':
            case KEY_RIGHT:
                if ( cursor_col_ < ( engine_->cols() - 1 ) )
                    ++( cursor_col_ );
                break;

            case 'i':
            case 'I':
            case KEY_UP:
                if ( cursor_row_ > 0 )
                    --( cursor_row_ );
                break;

            case 'k':
            case 'K':
            case KEY_DOWN:
                if ( cursor_row_ < ( engine_->rows() - 1 ) )
                    ++( cursor_row_ );
                break;

            case ' ':
                {
                    bool alive = engine_->is_cell_alive( cursor_row_,
                                                         cursor_col_ );
                    engine_->set_cell_alive( cursor_row_,
                                             cursor_col_,
                                             ! alive );
                }
                break;

//          default:
        }
    }
}


void GameOfLife::enter_run_state()
{
    state_ = State::RUN;
    steps_to_run_left_ = num_steps_to_run_;

    redraw();

    std::future<void> featureEvolvedWorld;

    while ( state_ == State::RUN ) {
        if ( steps_to_run_left_ == 0 ) {
            state_ = State::IDLE;
            continue;
        }

        auto start_time = std::chrono::steady_clock::now();
        engine_->start_single_generation( std::function<void()>() );
        do {
            int ch = terminal_.get_char_nodelay();
            if ( ch != ERR ) {
                if ( ch == 'q' || ch == 'Q' )
                    state_ = State::IDLE;
            }

            using namespace std::chrono_literals;
            std::this_thread::sleep_for( 10ms );

        } while ( ! engine_->has_generation_ready() );

        engine_->flip_generations();

        --steps_to_run_left_;

        if ( engine_->frame_period() == 0 ) {
            int ch = terminal_.get_char();
            if ( ch == 'q' || ch == 'Q'  ) {
                state_ = State::IDLE;
                continue;
            }
        } else {
            auto end_time = start_time +
                            std::chrono::milliseconds{ engine_->frame_period() };
            std::this_thread::sleep_until( end_time );
        }

        redraw();
    }
}


void GameOfLife::execute()
{
    while ( state_ != State::QUIT ) {
        redraw();

        int ch = terminal_.get_char();
        switch ( ch ) {
            case 'q':
            case 'Q':
                terminal_.print( "\n" );
                terminal_.refresh();
                state_ = State::QUIT;
                break;

            case 'c':
            case 'C':
                enter_configuration_state();
                break;

            case 'e':
            case 'E':
                enter_edit_state();
                break;

            case 'k':
            case 'K':
                engine_->kill();
                break;

            case 'p':
            case 'P':
                engine_->populate_randomly();
                break;

            case 's':
            case 'S':
                enter_save_to_file_state();
                break;

            case 'l':
            case 'L':
                enter_load_from_file_state();
                break;

            case 'i':
            case 'I':
                enter_run_state();
                break;

//          default:
        }
    }
}


}  // namespace game_of_life
