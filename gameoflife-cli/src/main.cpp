/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <cstdlib>              // for EXIT_XXX
#include <iostream>
#include <iomanip>


#include <gameoflife/GameOfLife.h>

int main( int, const char* const* )
{
    try
    {
        using GameOfLife = game_of_life::GameOfLife;
        GameOfLife game;

        game.execute();
    }
    catch ( std::exception& ex )
    {
        std::cout << "Failure!" << std::endl;
        std::cerr << "Caught std::exception: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch ( ... )
    {
        std::cout << "Failure!" << std::endl;
        std::cerr << "Caught unknown exception!" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
