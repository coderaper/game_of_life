/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <iostream>
#include <charconv>
#include <clocale>
#include <cstdlib>      // for ldiv()

#include <GameOfLifeEngine/GameOfLifeEngine.h>
#include <gameoflife/Terminal.h>

namespace game_of_life {

constexpr char Terminal::CELL_ALIVE_SYMBOL;
constexpr char Terminal::CELL_DEAD_SYMBOL;

std::mutex Terminal::mutex_;
std::atomic<Terminal*> Terminal::instance_ = nullptr;


Terminal::Terminal() {
    setlocale( LC_CTYPE, "" );
    initscr();                 // start curses mode

    NCURSES_SIZE_T rows;
    NCURSES_SIZE_T cols;
    getmaxyx( stdscr, rows, cols );

    raw();                     // Line buffering disabled
    cbreak();                  // Line buffering disabled, pass on everything
    keypad( stdscr, true );    // We get F1, F2 etc..
}


Terminal::~Terminal() {
    endwin();                  // end curses mode
}


Terminal& Terminal::instance()
{
    Terminal* instance = instance_.load( std::memory_order_relaxed );
    std::atomic_thread_fence( std::memory_order_acquire );
    if ( instance == nullptr ) {
        std::lock_guard<std::mutex> lock( mutex_ );
        instance = instance_.load( std::memory_order_relaxed );
        if ( instance == nullptr ) {
            instance = new Terminal;
            std::atomic_thread_fence( std::memory_order_release );
            instance_.store( instance, std::memory_order_relaxed );
        }
    }

    return *instance;
}


void Terminal::instance_delete() {
    Terminal* instance = instance_.load( std::memory_order_relaxed );
    std::atomic_thread_fence( std::memory_order_acquire );
    if ( instance != nullptr ) {
        std::lock_guard<std::mutex> lock( mutex_ );
        instance = instance_.load( std::memory_order_relaxed );
        if ( instance != nullptr ) {
            delete instance;
            instance = nullptr;
            std::atomic_thread_fence( std::memory_order_release );
            instance_.store( instance, std::memory_order_relaxed );
        }
    }
}


void Terminal::clear() {
    ::clear();				// clear the screen
    ::move( 0, 0 );			// start at the beginning of the screen
}


void Terminal::display_draw_top_horizontal_ruller( size_t cols ) {
    addch( ACS_ULCORNER );
    for ( size_t i = 0; i < cols; ++i ) {
        printw( "%d", (i + 1) % 10 );
    }
    addch( ACS_URCORNER );
    addch( '\n' );
}


void Terminal::display_draw_bottom_horizontal_ruller( size_t cols ) {
    addch( ACS_LLCORNER );
    for ( size_t i = 0; i < cols; ++i ) {
        printw( "%d", (i + 1) % 10 );
    }
    addch( ACS_LRCORNER );
    addch( '\n' );
}


void Terminal::display_draw_top_horizontal_ruller( size_t cols,
                                                   size_t cursor_col )
{
    addch( ACS_ULCORNER );
    for ( size_t i = 0; i < cols; ++i ) {
        if ( i == cursor_col )
            addch( ACS_VLINE );
        else
            printw( "%d", (i + 1) % 10 );
    }
    addch( ACS_URCORNER );
    addch( '\n' );
}


void Terminal::display_draw_bottom_horizontal_ruller( size_t cols,
                                                      size_t cursor_col )
{
    addch( ACS_LLCORNER );
    for ( size_t i = 0; i < cols; ++i ) {
        if ( i == cursor_col )
            addch( ACS_VLINE );
        else
            printw( "%d", (i + 1) % 10 );
    }
    addch( ACS_LRCORNER );
    addch( '\n' );
}


char Terminal::get_cell_symbol( const GameOfLifeEngine& world,
                                size_t row,
                                size_t col )
{
     return world.is_cell_alive( row, col ) ? CELL_ALIVE_SYMBOL :
                                              CELL_DEAD_SYMBOL;
}


void Terminal::draw( const GameOfLifeEngine& world )
{
    display_draw_top_horizontal_ruller( world.cols() );

    for ( size_t row = 0; row < world.rows(); ++row ) {
        unsigned int rowmark = (row + 1) % 10;
        printw( "%d", rowmark );
        for ( size_t col = 0; col < world.cols(); ++col ) {
            char cell_sym = get_cell_symbol( world, row, col );
            //std::cout << cell_sym;
            printw( "%c", cell_sym );
        }
        printw( "%d\n", rowmark );
    }

    display_draw_bottom_horizontal_ruller( world.cols() );
}


void Terminal::draw( const GameOfLifeEngine& world,
                     size_t cursor_row,
                     size_t cursor_col )
{
    display_draw_top_horizontal_ruller( world.cols(), cursor_col );

    for ( size_t row = 0; row < world.rows(); ++row ) {
        unsigned int rowmark = (row + 1) % 10;
        if ( row == cursor_row )
            addch( ACS_HLINE );
        else
            printw( "%d", rowmark );
        for ( size_t col = 0; col < world.cols(); ++col ) {
            char cell_sym = get_cell_symbol( world, row, col );
            printw( "%c", cell_sym );
        }
        if ( row == cursor_row )
        {
            addch( ACS_HLINE );
            addch( '\n' );
        }
        else
            printw( "%d\n", rowmark );
    }

    display_draw_bottom_horizontal_ruller( world.cols(), cursor_col );
}


//int Terminal::print( const char* format, ... )
//{
//    va_list arglist;
//    va_start( arglist, format );
//    int ret = vw_printw( stdscr, format, arglist );
//    va_end( arglist );

//    return ret;
//}


void Terminal::refresh()
{
    ::refresh();
}


int Terminal::get_char()
{
    return ::getch();
}


int Terminal::get_char_nodelay()
{
    nodelay( stdscr, true );
    int ch = getch();
    nodelay( stdscr, false );

    return ch;
}


std::string Terminal::get_line()
{
    std::array<char, 256> buffer;

    echo();
    // int n =
    getnstr( buffer.data(), buffer.size() );
    noecho();

    return std::string( buffer.data() );
}


template<typename T>
bool Terminal::get_number( T& number )
{
    auto line = get_line();
    bool result = false;

    if ( ! line.empty() )
    {
        auto [ptr, ec] = std::from_chars<T>( line.data(),
                                             line.data() + line.size(),
                                             number );

        if ( ec == std::errc() ) {
            result = true;
        }
    }

    return result;
}


template<typename T>
bool Terminal::get_number( T& number,
                           T default_value )
{
    auto line = get_line();
    bool result = false;

    if ( ! line.empty() )
    {
        auto [ptr, ec] = std::from_chars<T>( line.data(),
                                             line.data() + line.size(),
                                             number );

        if ( ec == std::errc() ) {
            result = true;
        }
    }
    else
    {
        number = default_value;
        result = true;
    }

    return result;
}


template<typename T>
bool Terminal::get_number( T& number,
                           T default_value,
                           T min,
                           T max )
{
    auto line = get_line();
    bool result = false;

    if ( ! line.empty() )
    {
        T n;
        auto [ptr, ec] = std::from_chars<T>( line.data(),
                                             line.data() + line.size(),
                                             n );

        if ( ec == std::errc() ) {
            if ( min <= n && n <= max ) {
                number = n;
                result = true;
            }
        }
    }
    else
    {
        number = default_value;
        result = true;
    }

    return result;
}


template<>
bool Terminal::get_number<double>( double& number )
{
    auto line = get_line();
    bool result = false;

    if ( ! line.empty() )
    {
        try {
            size_t convend;
            number = std::stod( line, &convend );
            result = true;
        }
        catch (...) {
        }
    }

    return result;
}


template<>
bool Terminal::get_number<double>( double& number,
                                   double default_value )
{
    auto line = get_line();
    bool result = false;

    if ( ! line.empty() )
    {
        try {
            size_t convend;
            number = std::stod( line, &convend );
            result = true;
        }
        catch (...) {
        }
    }
    else
    {
        number = default_value;
        result = true;
    }

    return result;
}


template<>
bool Terminal::get_number<double>( double& number,
                                   double default_value,
                                   double min,
                                   double max )
{
    auto line = get_line();
    bool result = false;

    if ( ! line.empty() )
    {
        try {
            size_t convend;
            double d = std::stod( line, &convend );
            if ( min <= d && d <= max ) {
                number = d;
                result = true;
            }
        }
        catch (...) {
        }
    }
    else
    {
        number = default_value;
        result = true;
    }

    return result;
}


template
bool Terminal::get_number<size_t>( size_t& number );

template
bool Terminal::get_number<size_t>( size_t& number,
                                   size_t default_value );

template
bool Terminal::get_number<size_t>( size_t& number,
                                   size_t default_value,
                                   size_t min,
                                   size_t max );

template
bool Terminal::get_number<ptrdiff_t>( ptrdiff_t& number );

template
bool Terminal::get_number<ptrdiff_t>( ptrdiff_t& number,
                                      ptrdiff_t default_value );

template
bool Terminal::get_number<ptrdiff_t>( ptrdiff_t& number,
                                      ptrdiff_t default_value,
                                      ptrdiff_t min,
                                      ptrdiff_t max );

}  // namespace game_of_life
