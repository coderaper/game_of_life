/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAME_OF_LIFE_TERMINAL_H_INCLUDED
#define GAME_OF_LIFE_TERMINAL_H_INCLUDED

#include <atomic>
#include <mutex>
#include <string>

#define NCURSES_ENABLE_STDBOOL_H
#include <ncurses/ncurses.h>

namespace game_of_life {

class GameOfLifeEngine;

class Terminal {

    Terminal();
    ~Terminal();

public:
    Terminal( const Terminal& ) = delete;
    Terminal( Terminal&& ) = delete;
    Terminal& operator=( const Terminal& ) = delete;
    Terminal& operator=( Terminal&& ) = delete;

    static Terminal& instance();

    static void instance_delete();

    /// Clears the screen
    void clear();

    /// Draws the life world on the screen
    void draw( const GameOfLifeEngine& world );

    /// Draws the life world on the screen with cursor (for edit mode)
    void draw( const GameOfLifeEngine& world,
               size_t cursor_row,
               size_t cursor_col );


    /// Produces output on the terminal screen according to the supplied format,
    /// which is identilcal to this of printf family of functions.
//    int print( const char* format, ... );
    template <typename ... Args>
    auto print( const char * const format,
                const Args& ... args ) noexcept -> decltype( ::printw( format, args... ) )
    {
      return ::printw( format, args... );
    }

    void refresh();

    int get_char();

    int get_char_nodelay();

    std::string get_line();

    template<typename T>
    bool get_number( T& number );

    template<typename T>
    bool get_number( T& number,
                     T default_value );

    template<typename T>
    bool get_number( T& number,
                     T default_value,
                     T min,
                     T max );

private:
    void display_draw_top_horizontal_ruller( size_t cols );
    void display_draw_bottom_horizontal_ruller( size_t cols );

    void display_draw_top_horizontal_ruller( size_t cols,
                                             size_t cursor_col );
    void display_draw_bottom_horizontal_ruller( size_t cols,
                                                size_t cursor_col );

    char get_cell_symbol( const GameOfLifeEngine& world, size_t row, size_t col );

private:
    // cell alive symbol for the display
    static constexpr char CELL_ALIVE_SYMBOL = '@';
    // cell dead symbol for the display
    static constexpr char CELL_DEAD_SYMBOL = ' ';

    static std::mutex mutex_;
    static std::atomic<Terminal*> instance_;

};


template<>
bool Terminal::get_number<double>( double& number );

template<>
bool Terminal::get_number<double>( double& number,
                                   double default_value );

template<>
bool Terminal::get_number<double>( double& number,
                                   double default_value,
                                   double min,
                                   double max );

extern template
bool Terminal::get_number<size_t>( size_t& number );

extern template
bool Terminal::get_number<size_t>( size_t& number,
                                   size_t default_value );

extern template
bool Terminal::get_number<size_t>( size_t& number,
                                   size_t default_value,
                                   size_t min,
                                   size_t max );

extern template
bool Terminal::get_number<ptrdiff_t>( ptrdiff_t& number );

extern template
bool Terminal::get_number<ptrdiff_t>( ptrdiff_t& number,
                                      ptrdiff_t default_value );

extern template
bool Terminal::get_number<ptrdiff_t>( ptrdiff_t& number,
                                      ptrdiff_t default_value,
                                      ptrdiff_t min,
                                      ptrdiff_t max );

extern template
bool Terminal::get_number<double>( double& number );

extern template
bool Terminal::get_number<double>( double& number,
                                   double default_value );

extern template
bool Terminal::get_number<double>( double& number,
                                   double default_value,
                                   double min,
                                   double max );

}  // namespace game_of_life

#endif  /* GAME_OF_LIFE_TERMINAL_H_INCLUDED */
