/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAME_OF_LIFE_GAMEOFLIFE_H_INCLUDED
#define GAME_OF_LIFE_GAMEOFLIFE_H_INCLUDED

#include <cstddef>
#include <memory>

namespace game_of_life {

class GameOfLifeEngine;

class Terminal;

class GameOfLife {
public:
    GameOfLife();

    ~GameOfLife();

    // Starts the game execution
    void execute();

private:
    void redraw() const;

    void enter_configuration_state();

    void enter_save_to_file_state();

    void enter_load_from_file_state();

    void enter_edit_state();

    void enter_run_state();

private:
    std::unique_ptr<GameOfLifeEngine> engine_;

    enum class State {
        IDLE,
        CONFIGURATION,
        SAVE_TO_FILE,
        LOAD_FROM_FILE,
        EDIT,
        RUN,
        QUIT,
    };

    State state_;

    size_t cursor_row_;              // cursor row position in edit mode
    size_t cursor_col_;              // cursor col position in edit mode

    size_t num_steps_to_run_;
    size_t steps_to_run_left_ = 0;

    Terminal& terminal_;
};


}  // namespace game_of_life

#endif  /* GAME_OF_LIFE_GAMEOFLIFE_H_INCLUDED */
