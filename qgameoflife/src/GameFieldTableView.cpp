/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QScrollBar>
#include <QtGui/QKeyEvent>

#include <qgameoflife/GameFieldModel.h>
#include <qgameoflife/CellItemDelegate.h>
#include <qgameoflife/GameFieldTableView.h>

GameFieldTableView::GameFieldTableView( QWidget* parent )
    : QTableView( parent )
{
    setStyleSheet(
        "QTableView {"
        " color: gold;"
        " selection-color: lightgoldenrodyellow; "
        " gridline-color: rgb(112, 0, 0);"
        " border-width: 2px; border-style: solid; border-color: rgb(112, 0, 0)"
        "} "
        "QTableView::item::selected:!focus { selection-background-color: lightsteelblue; }"
    );

    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setSizeAdjustPolicy( QAbstractScrollArea::AdjustToContents );

    {
        auto hHeader = horizontalHeader();
        hHeader->hide();
        hHeader->setSectionResizeMode( QHeaderView::ResizeToContents );
        hHeader->setMinimumSectionSize( 1 );
    }

    {
        auto vHeader = verticalHeader();
        vHeader->hide();
        vHeader->setSectionResizeMode( QHeaderView::ResizeToContents );
        vHeader->setMinimumSectionSize( 1 );
    }

    // for now allow only single item selection
    setSelectionBehavior( QAbstractItemView::SelectItems );
    setSelectionMode( QAbstractItemView::ExtendedSelection );

    CellItemDelegate* delegate = new CellItemDelegate( this );
    setItemDelegate( delegate );

    QTableView::setShowGrid( true );
    setCellItemDelegateSizeAndMargins( showGrid(), preserveGridSpacing() );

    // disable auto scrolling
    setAutoScroll( false );

    updateTableView();
}


void GameFieldTableView::setModel( GameFieldModel* model ) {
    QTableView::setModel( model );
    updateTableView();
}


GameFieldModel* GameFieldTableView::model() const {
    return qobject_cast<GameFieldModel*>( QTableView::model() );
}


void GameFieldTableView::setItemDelegate( CellItemDelegate* delegate ) {
    QTableView::setItemDelegate( delegate );
}


CellItemDelegate* GameFieldTableView::itemDelegate() const {
    return qobject_cast<CellItemDelegate*>( QTableView::itemDelegate() );
}


int GameFieldTableView::cellSize() const {
    return cellSize_;
}

bool GameFieldTableView::circularCells() const {
    return itemDelegate()->circularCells();
}


bool GameFieldTableView::showGrid() const {
    return QTableView::showGrid();
}


bool GameFieldTableView::preserveGridSpacing() const {
    return preserveGridSpacing_;
}

void GameFieldTableView::setCellSize( int size ) {
    if ( cellSize() != size ) {
        cellSize_ = size;
        setCellItemDelegateSizeAndMargins( showGrid(), preserveGridSpacing() );
        updateTableView();
    }
}


void GameFieldTableView::setCircularCells( bool circular ) {
    auto isOldCircular = itemDelegate()->circularCells();
    if ( isOldCircular != circular ) {
        itemDelegate()->setCircularCells( circular );
        updateTableView();
    }
}


void GameFieldTableView::setShowGrid( bool show ) {
    if ( showGrid() != show ) {
        setCellItemDelegateSizeAndMargins( show, preserveGridSpacing() );
        QTableView::setShowGrid( show );
        updateTableView();
    }
}


void GameFieldTableView::setPreserveGridSpacing( bool preserve ) {
    if ( preserveGridSpacing() != preserve ) {
        preserveGridSpacing_ = preserve;
        if ( ! showGrid() ) {
            setCellItemDelegateSizeAndMargins( showGrid(), preserve );
            updateTableView();
        }
    }
}


void GameFieldTableView::updateTableView()
{
    QSize tableSize = calcTableSize();
    setMaximumSize( tableSize );
    setMinimumSize( tableSize );

    resizeColumnsToContents();
    resizeRowsToContents();
}


void GameFieldTableView::setCellItemDelegateSizeAndMargins(
    bool showGrid,
    bool preserveGridSpacing
)
{
    auto delegate = itemDelegate();
    if ( ! showGrid && preserveGridSpacing ) {
        delegate->setCellSize( cellSize() + 1 );
        auto lw = lineWidth();
        delegate->setContentsMargins( 0, 0, lw, lw );
    }
    else {
        delegate->setCellSize( cellSize() );
        delegate->setContentsMargins( 0, 0, 0, 0 );
    }
}

QSize GameFieldTableView::calcTableSize()
{
    CellItemDelegate* delegate = itemDelegate();
    if ( delegate == nullptr ) {
        return {0, 0};
    }

    GameFieldModel* gameModel = model();
    if ( gameModel == nullptr ) {
        return {0, 0};
    }

    auto w = 2 * frameWidth();
    auto h = 2 * frameWidth();

    auto columnCount = gameModel->columnCount( gameModel->index( 0, 0 ) );
    auto rowCount = gameModel->rowCount( gameModel->index( 0, 0 ) );
    w += delegate->cellSize() * columnCount;
    h += delegate->cellSize() * rowCount;

    if ( showGrid() ) {
        auto lw = lineWidth();
        w += columnCount > 0 ? (columnCount - 1) *  lw : 0;
        h += rowCount > 0 ? (rowCount - 1) *  lw : 0;
    }

    if ( columnCount > 0 ) {
        w -= delegate->cellSize() - cellSize();
    }

    if ( rowCount > 0 ) {
        h -= delegate->cellSize() - cellSize();
    }

    return QSize( w, h );
}


void GameFieldTableView::keyPressEvent( QKeyEvent* event )
{
    if ( event->key() == Qt::Key_Tab || event->key() == Qt::Key_Backtab )
    {
        event->ignore();
    }
    else
    {
        QTableView::keyPressEvent( event );
    }
}



void GameFieldTableView::wheelEvent( QWheelEvent* event )
{
    // filter out wheel events, because they shift our table slightly, even
    // if it fits in the viewport
    event->ignore();
}
