/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <cstdio>

#include <QtWidgets/QApplication>
#include <QtCore/QCommandLineParser>

#include <qgameoflife/qgameoflife_config.h>
#include <qgameoflife/Application.h>

#include <qgameoflife/MainWindow.h>

#include <QtCore/QLoggingCategory>

int main( int argc, char *argv[] )
{
//    QLoggingCategory::defaultCategory()->setEnabled(QtDebugMsg, true);

    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
//    QGuiApplication::setAttribute( Qt::AA_UseHighDpiPixmaps );

    Application app( argc, argv );

    QCoreApplication::setApplicationName( "qgameoflife" );
    QGuiApplication::setApplicationDisplayName(
        QCoreApplication::translate( "main", "Game of Life" )
    );
    QCoreApplication::setOrganizationName( "coderaper" );
    QCoreApplication::setApplicationVersion( PROGRAM_VERSION_STR );

    QCommandLineParser parser;
    parser.setApplicationDescription( QCoreApplication::applicationName() );
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument(
        "file",
        QCoreApplication::translate( "main",
                                     "The input file to open on startup" ),
        "[file]"
    );
    parser.process( app );

    MainWindow win;
    win.show();

    const QStringList positionalArguments = parser.positionalArguments();
    if ( ! positionalArguments.isEmpty() ) {
        if ( positionalArguments.size() > 1 ) {
            QString msg{ QCoreApplication::translate( "main",
                "More than one 'file' arguments specified!" ) };
            fputs( qUtf8Printable( msg ), stderr );
            fputs( "\n\n", stderr );
            fputs( qUtf8Printable( parser.helpText() ), stderr );
            return EXIT_FAILURE;
        }

        win.openFile( positionalArguments.first() );
    }

    return app.exec();
}
