/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <qgameoflife/CellItemDelegate.h>
#include <qgameoflife/GameFieldModel.h>

#include <QtCore/QEvent>

// for debugging
//#include <QtCore/QMetaEnum>
//#include <QtCore/QDebug>

#include <QtGui/QPainter>
#include <QtGui/QKeyEvent>
#include <QtWidgets/QApplication>

CellItemDelegate::CellItemDelegate( QObject* parent )
: QAbstractItemDelegate( parent )
{}


void CellItemDelegate::paint( QPainter* painter,
                              const QStyleOptionViewItem& option,
                              const QModelIndex& index ) const
{
    assert( index.isValid() );

    QRect cellRect = option.rect.marginsRemoved( contentsMargins() );
    const auto& state = option.state;
    //QStyle* style = option.widget->style();
    const QPalette& palette = option.palette;

    QPalette::ColorGroup cg = state & QStyle::State_Enabled ?
        ( state & QStyle::State_Active ? QPalette::Normal : QPalette::Inactive ) :
        QPalette::Disabled;

    const auto isCellAlive = index.data( Qt::DisplayRole ).toBool();

    // draw non-curcular alive cells
    if ( ! circularCells() ) {
        if ( isCellAlive ) {
            const QBrush& brush = state & QStyle::State_Selected &&
                                  state & QStyle::State_Active ?
                palette.brush( cg, QPalette::HighlightedText ) :
                palette.brush( cg, QPalette::Text );
            painter->fillRect( cellRect, brush );
            return;
        }
    }

    if ( state & QStyle::State_Selected && state & QStyle::State_Active ) {
        const QBrush& brush = palette.brush( cg,
                                             QPalette::Highlight );

        painter->fillRect( cellRect, brush );
    }

    if ( isCellAlive ) {
        painter->save();
        painter->setRenderHint( QPainter::Antialiasing, true );
        painter->setPen( Qt::NoPen );

        if ( state & QStyle::State_Selected && state & QStyle::State_Active )
            painter->setBrush( palette.highlightedText() );
        else
            painter->setBrush( palette.text() );

        if ( circularCells() ) {
            auto radius = std::min( cellRect.width(), cellRect.height() ) / qreal{ 2.0 };

            auto circleRect = QRectF( cellRect.x() + cellRect.width() / 2 - radius,
                                      cellRect.y() + cellRect.height() / 2 - radius,
                                      2 * radius, 2 * radius );
            painter->drawEllipse( circleRect );
        }

        painter->restore();
    }
}


QSize CellItemDelegate::sizeHint( const QStyleOptionViewItem&  /* option */,
                                  const QModelIndex& /* index */) const
{
    return QSize( cellSize(), cellSize() );
}


void CellItemDelegate::setCellSize( int size )
{
    cellSize_ = size;
}


void CellItemDelegate::setCircularCells( bool circular )
{
    circularCells_ = circular;
}


void CellItemDelegate::setContentsMargins( int left, int top, int right, int bottom )
{
    contentsMargins_.setLeft( left );
    contentsMargins_.setTop( top );
    contentsMargins_.setRight( right );
    contentsMargins_.setBottom( bottom );
}


void CellItemDelegate::setContentsMargins( const QMargins& margins )
{
    contentsMargins_ = margins;
}


bool CellItemDelegate::editorEvent( QEvent* event,
                                    QAbstractItemModel* model,
                                    const QStyleOptionViewItem& option,
                                    const QModelIndex& index )
{
    assert( event != nullptr );
    assert( model != nullptr );

    QVariant value = index.data( Qt::DisplayRole );
    if ( ! value.isValid() )
        return false;

//    QMetaEnum metaEnum = QMetaEnum::fromType<QEvent::Type>();
//    qDebug() << __func__ << "(): cell [" << index.column() << ", "
//             << index.row() << "], event type: "
//             << metaEnum.valueToKey( event->type() ) << Qt::endl;

    if ( event->type() == QEvent::MouseButtonRelease ||
         event->type() == QEvent::MouseButtonDblClick ||
         event->type() == QEvent::MouseButtonPress )
    {
        QRect cellRect = option.rect.marginsRemoved( contentsMargins() );

        QMouseEvent* me = static_cast<QMouseEvent*>(event);
        if ( me->button() != Qt::LeftButton || ! cellRect.contains( me->pos() ) )
            return false;
        if ( event->type() == QEvent::MouseButtonRelease ||
             event->type() == QEvent::MouseButtonPress )
            return false;

    } else if ( event->type() == QEvent::KeyPress ) {
        if ( static_cast<QKeyEvent*>(event)->key() != Qt::Key_Space &&
             static_cast<QKeyEvent*>(event)->key() != Qt::Key_Select )
            return false;
    } else {
        return false;
    }

    bool isAlive = value.toBool();
    return model->setData( index, QVariant( ! isAlive ), Qt::EditRole );

    return false;
}
