/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <filesystem>
#include <fstream>
#include <string_view>

#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QRect>

#include <qgameoflife/GameFieldModel.h>

#include <GameOfLifeEngine/GameOfLifeEngine.h>


GameFieldModel::GameFieldModel( QObject* parent )
: QAbstractTableModel( parent ),
  engine_( std::make_unique<GameOfLifeEngine>() )
{
    populateAllRandomly();
    connect( this, &GameFieldModel::nextGenerationComputed,
             this, &GameFieldModel::flipGenerations );
}


GameFieldModel::~GameFieldModel() = default;


int GameFieldModel::rowCount( const QModelIndex& /* parent */ ) const
{
//    if ( parent.isValid() )
//        return 0;

    return engine_->rows();
}


int GameFieldModel::columnCount( const QModelIndex& /* parent */ ) const
{
//    if ( parent.isValid() )
//        return 0;

    return engine_->cols();
}


QVariant GameFieldModel::data( const QModelIndex& index, int role ) const
{
    if ( ! index.isValid() || role != Qt::DisplayRole )
        return QVariant();

    if ( index.row() >= static_cast<int>( engine_->rows() ) ||
         index.column() >= static_cast<int>( engine_->cols() ) )
        return QVariant();

    return engine_->is_cell_alive( index.row(), index.column() );
}


bool GameFieldModel::setData( const QModelIndex& index,
                              const QVariant& value,
                              int role )
{
    if ( isRunning() )
        return false;

    if ( role != Qt::EditRole || data( index, role ) == value )
        return false;

    bool oldIsResettable = engine_->is_resettable_to_seed_generation();
    auto alive = value.toBool();
    if ( engine_->is_cell_alive( index.row(), index.column() ) != alive )
    {
        engine_->set_cell_alive( index.row(), index.column(), alive );

        emit dataChanged( index, index, {Qt::DisplayRole, role} );
    }

    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();

    return true;
}


QVariant GameFieldModel::headerData( int /* section */,
                                      Qt::Orientation /* orientation */,
                                      int role ) const
{
    if ( role == Qt::SizeHintRole )
        return QSize( 0, 0 );

    return QVariant();
}


Qt::ItemFlags GameFieldModel::flags( const QModelIndex& index ) const
{
    if ( isRunning() || ! index.isValid() )
        return Qt::NoItemFlags;

    //return Qt::ItemIsEditable | QAbstractItemModel::flags( index );
    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled |
           Qt::ItemNeverHasChildren;
}


int GameFieldModel::framePeriod() const
{
    return engine_->frame_period();
}


void GameFieldModel::setFramePeriod( int ms ) {
    engine_->set_frame_period( ms );
}


int GameFieldModel::populationCount() const
{
    return engine_->population_count();
}


double GameFieldModel::populationDensity() const
{
    return engine_->population_density();
}


double GameFieldModel::populateAliveRatio() const
{
    return engine_->populate_alive_ratio();
}


void GameFieldModel::setPopulateAliveRatio( double aliveRatio )
{
    engine_->set_populate_alive_ratio( aliveRatio );
}


bool GameFieldModel::isResettableToSeedGeneration() const
{
    return engine_->is_resettable_to_seed_generation();
}


void GameFieldModel::resetToSeedGeneration()
{
//    throw std::runtime_error( "resetToSeedGeneration() is not implemented!" );
    engine_->reset_to_seed_generation();
    emit dataChanged( index( 0, 0 ),
                      index( engine_->rows() - 1, engine_->cols() - 1 ),
                      {Qt::DisplayRole} );
    emit seedGenerationChanged();
}


bool GameFieldModel::isRunning() const
{
    return engine_->is_running();
}


unsigned long long GameFieldModel::generation() const
{
    return engine_->generation();
}


GameFieldModel::Topology GameFieldModel::topology() const
{
    using game_of_life::WorldTopology;
    switch ( engine_->get_topology() ) {
        case WorldTopology::BOUNDED_PLANE:
            return Topology::BoundedPlane;

        case WorldTopology::TORUS:
            return Topology::Torus;

        case WorldTopology::KLEIN_BOTTLE_HORIZ_TWIST:
            return Topology::KleinBottleHorizontalTwist;

        case WorldTopology::KLEIN_BOTTLE_VERT_TWIST:
            return Topology::KleinBottleVerticalTwist;

        case WorldTopology::CROSS_SURFACE:
            return Topology::CrossSurface;

//        case WorldTopology::SPHERE:
//            return Topology::Sphere;

        default:
            ;
    }
    throw std::logic_error( "engine is set to unknow WorldTopology kind" );
}


void GameFieldModel::setTopology( Topology topology )
{
    if ( GameFieldModel::topology() == topology )
        return;

    using game_of_life::WorldTopology;
    switch ( topology ) {
        case Topology::BoundedPlane:
            engine_->set_topology( WorldTopology::BOUNDED_PLANE );
            emit topologyChanged( topology );
        break;

        case Topology::Torus:
            engine_->set_topology( WorldTopology::TORUS );
            emit topologyChanged( topology );
        break;

        case Topology::KleinBottleHorizontalTwist:
            engine_->set_topology( WorldTopology::KLEIN_BOTTLE_HORIZ_TWIST );
            emit topologyChanged( topology );
        break;

        case Topology::KleinBottleVerticalTwist:
            engine_->set_topology( WorldTopology::KLEIN_BOTTLE_VERT_TWIST );
            emit topologyChanged( topology );
        break;

        case Topology::CrossSurface:
            engine_->set_topology( WorldTopology::CROSS_SURFACE );
            emit topologyChanged( topology );
        break;

//        case Topology::Sphere:
//            engine_->set_topology( WorldTopology::SPHERE );
//            emit topologyChanged( topology );
//        break;

        default:
          throw std::logic_error( "unknow Topology kind" );
    }
}


QString GameFieldModel::toString( Topology topology ) {
    switch ( topology ) {
        case Topology::BoundedPlane:
            return tr( "Bounded Plane", "Topology" );

        case Topology::Torus:
            return tr( "Torus", "Topology" );

        case Topology::KleinBottleHorizontalTwist:
            return tr( "Klein Bottle with Horizontal Twist", "Topology" );

        case Topology::KleinBottleVerticalTwist:
            return tr( "Klein Bottle with Vertical Twist", "Topology" );

        case Topology::CrossSurface:
            return tr( "Cross-Surface", "Topology" );

//        case Topology::Sphere:
            return tr( "Sphere", "Topology" );

        default:
          throw std::logic_error( "unknow Topology kind" );
    }
}


void GameFieldModel::createNewEmpty( size_t width, size_t height )
{
    assert( ! isRunning() );

    beginResetModel();

    engine_->resize_and_exterminate( height, width );

    endResetModel();
}


void GameFieldModel::createNewRandomPopulated( size_t width, size_t height )
{
    assert( ! isRunning() );

    beginResetModel();

    engine_->resize_and_populate_randomly( height, width );

    endResetModel();
}


void GameFieldModel::expand( size_t newWidth, size_t newHeight,
                             size_t horizOffset, size_t vertOffset )
{
    beginResetModel();

    engine_->expand( newWidth, newHeight, horizOffset, vertOffset );

    endResetModel();
}


void GameFieldModel::crop( size_t top, size_t left,
                           size_t bottom, size_t right )
{
    beginResetModel();

    auto width = right - left + 1;
    auto height = bottom - top + 1;

    engine_->crop( top, left, width, height );

    endResetModel();
}


bool GameFieldModel::load( const QString& fileName )
{
    beginResetModel();

    const auto& utf8Bytes = fileName.toUtf8();
    engine_->load_world( utf8Bytes.constData() );

    endResetModel();

    return true;
}


bool GameFieldModel::save( const QString& fileName )
{
    const auto& utf8Bytes = fileName.toUtf8();
    engine_->save_world( utf8Bytes.constData(), true );

    return true;
}


bool GameFieldModel::saveSelection( const QString& fileName,
                                    size_t top, size_t left,
                                    size_t bottom, size_t right )
{
    auto width = right - left + 1;
    auto height = bottom - top + 1;

    const auto& utf8Bytes = fileName.toUtf8();
    engine_->save_world( utf8Bytes.constData(),
                         top, left, width, height,
                         true );

    return true;
}


void GameFieldModel::killAll()
{
    bool oldIsResettable = engine_->is_resettable_to_seed_generation();

    engine_->kill();

    emit dataChanged( index( 0, 0 ),
                      index( engine_->rows() - 1, engine_->cols() - 1 ),
                      {Qt::DisplayRole} );
    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();
}


void GameFieldModel::killSelection( size_t top, size_t left,
                                    size_t bottom, size_t right )
{
    auto width = right - left + 1;
    auto height = bottom - top + 1;

    bool oldIsResettable = engine_->is_resettable_to_seed_generation();

    engine_->kill( top, left, width, height );

    emit dataChanged( index( top, left ),
                      index( bottom, right ),
                      {Qt::DisplayRole} );
    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();
}


void GameFieldModel::resurrectAll()
{
    bool oldIsResettable = engine_->is_resettable_to_seed_generation();

    engine_->resurrect();

    emit dataChanged( index( 0, 0 ),
                      index( engine_->rows() - 1, engine_->cols() - 1 ),
                      {Qt::DisplayRole} );
    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();
}


void GameFieldModel::resurrectSelection( size_t top, size_t left,
                                         size_t bottom, size_t right )
{
    auto width = right - left + 1;
    auto height = bottom - top + 1;

    bool oldIsResettable = engine_->is_resettable_to_seed_generation();

    engine_->resurrect( top, left, width, height );

    emit dataChanged( index( top, left ),
                      index( bottom, right ),
                      {Qt::DisplayRole} );
    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();
}


void GameFieldModel::populateAllRandomly()
{
    bool oldIsResettable = engine_->is_resettable_to_seed_generation();

    engine_-> populate_randomly();

    emit dataChanged( index( 0, 0 ),
                      index( engine_->rows() - 1, engine_->cols() - 1 ),
                      {Qt::DisplayRole} );
    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();
}


void GameFieldModel::populateSelectionRandomly( size_t top, size_t left,
                                                size_t bottom, size_t right )
{
    auto width = right - left + 1;
    auto height = bottom - top + 1;

    bool oldIsResettable = engine_->is_resettable_to_seed_generation();

    engine_->populate_randomly( top, left, width, height );

    emit dataChanged( index( top, left ),
                      index( bottom, right ),
                      {Qt::DisplayRole} );
    if ( oldIsResettable != engine_->is_resettable_to_seed_generation() )
        emit seedGenerationChanged();
}

void GameFieldModel::startSingleGeneration()
{
    std::function<void()> callback = [this]() {
                                         this->onNextGenerationComputed();
                                     };
    engine_->start_single_generation( std::move( callback ) );
    emit runStateChanged( engine_->is_running() );
}


void GameFieldModel::startContinuousGeneration()
{
    std::function<void()> callback = [this]() {
                                         this->onNextGenerationComputed();
                                     };

    engine_->start_continuous_generation( std::move( callback ) );

    emit runStateChanged( engine_->is_running() );
}


void GameFieldModel::stopGeneration()
{
    engine_->stop_continuous_generation();
}


void GameFieldModel::flipGenerations()
{
    bool oldIsRunning = engine_->is_running();

    engine_->flip_generations();

    if ( engine_->is_running() != oldIsRunning ) {
        emit runStateChanged( engine_->is_running() );
    }

    emit dataChanged( index( 0, 0 ),
                      index( engine_->rows() - 1, engine_->cols() - 1 ),
                      {Qt::DisplayRole} );

    emit generationChanged();
}


void GameFieldModel::onNextGenerationComputed()
{
    emit nextGenerationComputed( QPrivateSignal{} );
}

