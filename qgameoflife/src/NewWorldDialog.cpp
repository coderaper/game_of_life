/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#include <qgameoflife/NewWorldDialog.h>
#include <qgameoflife/ui_NewWorldDialog.h>

NewWorldDialog::NewWorldDialog( QWidget* parent )
: QDialog( parent ),
  ui( std::make_unique<Ui::NewWorldDialog>() )
{
    ui->setupUi( this );

    setFixedSize( minimumSize() );
}


NewWorldDialog::~NewWorldDialog() = default;


void NewWorldDialog::setWorldColumns( int columns )
{
    ui->worldColumnsSpinBox->setValue( columns );
}


int NewWorldDialog::worldColumns() const
{
    return ui->worldColumnsSpinBox->value();
}


void NewWorldDialog::setWorldRows( int rows )
{
    ui->worldRowsSpinBox->setValue( rows );
}


int NewWorldDialog::worldRows() const
{
    return ui->worldRowsSpinBox->value();
}


void NewWorldDialog::setPopulateWorldRandomly( bool populate )
{
    ui->populateWorldRandomlyGroupBox->setChecked( populate );
}


bool NewWorldDialog::populateWorldRandomly() const
{
    return ui->populateWorldRandomlyGroupBox->isChecked();
}


void NewWorldDialog::setDesiredPopulationDensity( double populationDensity )
{
    ui->desiredPopulationDensityDblSpinBox->setValue( populationDensity );
}


double NewWorldDialog::desiredPopulationDensity() const
{
    return ui->desiredPopulationDensityDblSpinBox->value();
}
