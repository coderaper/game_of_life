/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <utility>

#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>
#include <QtCore/QItemSelection>
#include <QtCore/QItemSelectionModel>
#include <QtCore/QDir>
#include <QtGui/QCloseEvent>
#include <QtGui/QScreen>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtCore/QMetaEnum>

// for debugging
//#include <QtCore/QDebug>

#include <qgameoflife/MainWindow.h>
#include <qgameoflife/ui_MainWindow.h>
#include <qgameoflife/GameFieldModel.h>
#include <qgameoflife/NewWorldDialog.h>
#include <qgameoflife/ExpandWorldSizeDialog.h>
#include <qgameoflife/AboutDialog.h>


const QString MainWindow::cellSizeToolTipPrefix =
    QCoreApplication::translate( "MainWindow", "Cell Size: ", nullptr );

const QString MainWindow::cellSizeUnit =
    QCoreApplication::translate( "MainWindow", " px", nullptr );

const QString MainWindow::frameDurationToolTipPrefix =
    QCoreApplication::translate( "MainWindow", "Frame Duration: ", nullptr );

const QString MainWindow::frameDurationUnit =
    QCoreApplication::translate( "MainWindow", " ms", nullptr );

const QString MainWindow::worldSizeInfoFormat =
    QCoreApplication::translate( "MainWindow", "World Size: %1 x %2", nullptr );

const QString MainWindow::worldTopologyInfoFormat =
    QCoreApplication::translate( "MainWindow", "World Topology: %1 ", nullptr );

const char* MainWindow::cfgGroup_MainWin = "MainWindow";
const char* MainWindow::cfgKey_MainWinPos = "position";
const char* MainWindow::cfgKey_MainWinSize = "size";

const char* MainWindow::cfgGroup_General = "General";
const char* MainWindow::cfgPrefix_RecentFileList = "RecentFileList";
const char* MainWindow::cfgKey_RecentFile = "file";

MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent ),
      ui( std::make_unique<Ui::MainWindow>() ),
      newWorldDialog( new NewWorldDialog( this ) ),
      expandWorldSizeDialog( new ExpandWorldSizeDialog( this ) )
{
    model = new GameFieldModel( this );
    model->setObjectName( QString::fromUtf8( "model" ) );

    selectionModel = new QItemSelectionModel( model, this );
    selectionModel->setObjectName( QString::fromUtf8( "selectionModel" ) );

    topologyGroup = new QActionGroup( this );
    topologyGroup->setObjectName( QString::fromUtf8( "topologyGroup" ) );

    setThemeIcons();

    auto [ cellSizeSpinBoxTool, cellSizeSpinBox ] =
        createCellSizeSpinBoxTool( this );
    this->cellSizeSpinBox = cellSizeSpinBox;

    auto [ frameDurationSpinBoxTool, frameDurationSpinBox ] =
        createFrameDurationSpinBoxTool( this );
    this->frameDurationSpinBox = frameDurationSpinBox;

    worldSizeInfoLabel = new QLabel( this );
    worldSizeInfoLabel->setObjectName( QString::fromUtf8( "worldSizeInfoLabel" ) );

    worldTopologyInfoLabel = new QLabel( this );
    worldTopologyInfoLabel->setObjectName( QString::fromUtf8( "worldTopologyInfoLabel" ) );

    ui->setupUi( this );

//    qDebug() << __func__ << "() dump of object tree after ui->setupUi( this ) call:" << Qt::endl;
//    dumpObjectTree();

//    qDebug() << __func__ << "() dump of object info after ui->setupUi( this ) call:" << Qt::endl;
//    dumpObjectInfo();

    // Toolbar is created and automatic slots connection is done, now let's
    // move the tools to the toolbar
    ui->toolBar->insertWidget( ui->actionShowGrid, cellSizeSpinBoxTool );
    ui->toolBar->addSeparator();
    ui->toolBar->addWidget( frameDurationSpinBoxTool );

    ui->statusBar->addPermanentWidget( worldTopologyInfoLabel );
    ui->statusBar->addPermanentWidget( worldSizeInfoLabel );

//    qDebug() << __func__ << "() dump of object tree after moving to toolbar calls:" << Qt::endl;
//    dumpObjectTree();

//    qDebug() << __func__ << "() dump of object info after moving to toolbar calls:" << Qt::endl;
//    dumpObjectInfo();

    ui->splitter->setStretchFactor( 0, 1 );
    ui->splitter->setStretchFactor( 1, 0 );
    ui->splitter->setCollapsible( 0, false );

    ui->gameFieldTableView->setModel( model );
    ui->gameFieldTableView->setSelectionModel( selectionModel );

    ui->gameFieldTableView->setContextMenuPolicy( Qt::CustomContextMenu );

    on_cellSizeSpinBox_valueChanged( ui->gameFieldTableView->cellSize() );

    on_frameDurationSpinBox_valueChanged( model->framePeriod() );

    ui->actionShowGrid->setChecked( ui->gameFieldTableView->showGrid() );

    ui->actionCollapseCellSpacing->setChecked( ! ui->gameFieldTableView->preserveGridSpacing() );

    ui->actionCollapseCellSpacing->setEnabled( ! ui->gameFieldTableView->showGrid() );

    restoreSettings();

    {
        actionRecentFileSubMenu = ui->recentMenu->menuAction();
        for ( auto& recentFileAction : recentFileActions ) {
            recentFileAction =
                ui->recentMenu->addAction( QString{}, this,
                                           &MainWindow::openRecentFile );
            recentFileAction->setVisible( false );
        }
        setRecentFilesVisible( MainWindow::hasRecentFiles() );
    }

    {
        GameFieldModel::Topology currTopology = model->topology();
        QMetaEnum e = QMetaEnum::fromType<GameFieldModel::Topology>();
        for ( int k = 0; k < e.keyCount(); k++ )
        {
            GameFieldModel::Topology topology =
                static_cast<GameFieldModel::Topology>( e.value( k ) );

            QAction* action =
                ui->menuTopology->addAction( model->toString( topology ) );
            action->setCheckable( true );
            action->setData( topology );
            if ( topology == currTopology )
                action->setChecked( true );
            topologyGroup->addAction( action );
        }
    }

    updateActionsState();

    displayPopulationInfo();
    displayGenerationInfo();

    displayWorldInfo();
}


MainWindow::~MainWindow() = default;


void MainWindow::openFile( const QString& fileName )
{
//    qDebug() << __func__ << "(" << qUtf8Printable( fileName )
//             << ") called " << Qt::endl;

    QString errorMessage;

    QGuiApplication::setOverrideCursor( Qt::WaitCursor );
    try
    {
        if ( model->load( fileName ) ) {
            setCurrentFile( fileName );
            statusBar()->showMessage(tr( "File loaded" ), 2000 );
        }
        else {
            errorMessage = tr( "Cannot open file '%1'!" )
                           .arg( QDir::toNativeSeparators( fileName ) );
        }
    }
    catch ( std::exception& ex )
    {
        errorMessage = tr( "Cannot open file '%1':\n%2." )
                       .arg( QDir::toNativeSeparators( fileName ), ex.what() );
    }
    catch ( ... )
    {
        errorMessage = tr( "Cannot open file '%1':\n%2." )
                       .arg( QDir::toNativeSeparators( fileName ),
                             tr( "Caught unknown exception!" ) );
    }

    QGuiApplication::restoreOverrideCursor();

    if ( ! errorMessage.isEmpty() ) {
        QMessageBox::warning( this, tr( "Open File" ), errorMessage );
    }
}


bool MainWindow::saveFile( const QString& fileName,
                           bool selectionOnly,
                           const QString& operationName )
{
    // Check for prerequisite violations
    assert( ! selectionOnly || selectionModel->hasSelection() );
    assert( ! selectionOnly || selectionModel->selection().size() == 1 );

    QString errorMessage;

    QGuiApplication::setOverrideCursor( Qt::WaitCursor );

    try
    {
        bool saved = false;
        if ( selectionOnly ) {
            const QItemSelection selection = selectionModel->selection();
            const QItemSelectionRange& selectionRange = selection.first();
            saved = model->saveSelection( fileName,
                                          selectionRange.top(),
                                          selectionRange.left(),
                                          selectionRange.bottom(),
                                          selectionRange.right() );
            if ( saved ) {
                statusBar()->showMessage(tr( "World selection was saved" ), 2000 );
            }
        }
        else {
            saved = model->save( fileName );
            if ( saved ) {
                setCurrentFile( fileName );
                statusBar()->showMessage( tr( "Whole world was saved" ), 2000 );
            }
        }

        if ( ! saved ) {
            errorMessage = tr( "Cannot save file '%1'!" )
                           .arg( QDir::toNativeSeparators( fileName ) );
        }
    }
    catch ( std::exception& ex )
    {
        errorMessage = tr( "Cannot save file '%1':\n%2." )
                       .arg( QDir::toNativeSeparators( fileName ), ex.what() );
    }
    catch ( ... )
    {
        errorMessage = tr( "Cannot save file '%1':\n%2." )
                       .arg( QDir::toNativeSeparators( fileName ),
                             tr( "Caught unknown exception!" ) );
    }

    QGuiApplication::restoreOverrideCursor();

    if ( ! errorMessage.isEmpty() ) {
        QMessageBox::warning( this, operationName, errorMessage );
        return false;
    }

    return true;
}


void MainWindow::on_actionFileNew_triggered()
{
    if ( ! newWorldDialog )
        return;

    assert( model != nullptr );

    // Setup the NewWorldDialog
    auto worldColumns = model->columnCount( model->index( 0, 0 ) );
    newWorldDialog->setWorldColumns( worldColumns );
    auto worldRows = model->rowCount( model->index( 0, 0 ) );
    newWorldDialog->setWorldRows( worldRows );
    auto populateAliveRatio = model->populateAliveRatio();
    newWorldDialog->setDesiredPopulationDensity( populateAliveRatio );

    auto dialogResult = newWorldDialog->exec();

    QString errorMessage;
    if ( dialogResult == QDialog::Accepted ) {
        try {
            auto width = newWorldDialog->worldColumns();
            auto height = newWorldDialog->worldRows();
            if ( newWorldDialog->populateWorldRandomly() ) {
                model->setPopulateAliveRatio(
                    newWorldDialog->desiredPopulationDensity()
                );
                model->createNewRandomPopulated( width, height );
            }
            else {
                model->createNewEmpty( width, height );
            }
        }
        catch ( std::exception& ex )
        {
            errorMessage = tr( "Caught std::exception: " );
            errorMessage.append( ex.what() );
        }
        catch ( ... )
        {
            errorMessage = tr( "Caught unknown exception!" );
        }
    }

    if ( ! errorMessage.isEmpty() ) {
        QMessageBox::critical( this, tr( "Create New World" ),
                               tr( "Unable to create New World!\n%1" )
                               .arg( errorMessage ) );
    }
}

void MainWindow::on_actionFileOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName( this,
        tr( "Choose a file" ), currentFile,
        tr( "Conway World formats (*.cells *.png);;"
            "Plaintext Conway World format (*.cells);;"
            "Binary Conway World format (*.png)" ) );

    if ( ! fileName.isEmpty() )
        openFile( fileName );
}


void MainWindow::on_actionFileSaveWhole_triggered()
{
    static const QString operationName = tr( "Save Whole World" );

    QString fileName = QFileDialog::getSaveFileName( this,
                       operationName,
                       currentFile );
    if ( fileName.isEmpty() )
        return;

    saveFile( fileName, false, operationName );
}


void MainWindow::on_actionFileSaveSelection_triggered()
{
    static const QString operationName = tr( "Save World Selection" );

    if ( ! selectionModel->hasSelection() ) {
        QMessageBox::warning( this,
                              operationName,
                              tr( "There is no selection to save" ) );
        return;
    }

    const QItemSelection selection = selectionModel->selection();
    if ( selection.size() != 1 ) {
        QMessageBox::warning( this,
                              operationName,
                              tr( "Only single rectangular selections can "
                                  "be saved!" ) );
        return;
    }

    QString fileName = QFileDialog::getSaveFileName( this,
                                                     operationName,
                                                     currentFile );
    if ( fileName.isEmpty() )
        return;

    saveFile( fileName, true, operationName );
}


void MainWindow::on_recentMenu_aboutToShow()
{
    QSettings settings( QCoreApplication::organizationName(),
                        QCoreApplication::applicationName() );

    const  std::list<QString> recentFiles = readRecentFiles( settings );
    const std::size_t count = std::min( maxRecentFiles, recentFiles.size() );
    std::size_t i = 0;
    for ( const auto& recentFile : recentFiles ) {
        const QString filename = QFileInfo( recentFile ).fileName();
        recentFileActions[i]->setText( tr("&%1 %2").arg( i + 1 ).arg( filename ) );
        recentFileActions[i]->setData( recentFile );
        recentFileActions[i]->setVisible( true );
        if ( ++i >= count )
            break;
    }
    for ( ; i < maxRecentFiles; ++i ) {
        recentFileActions[i]->setData( QVariant{} );
        recentFileActions[i]->setVisible( false );
    }
}


void MainWindow::on_actionExit_triggered()
{
    close();
}


void MainWindow::on_actionEditKill_triggered()
{
    if ( selectionModel->hasSelection() ) {
        const QItemSelection selection = selectionModel->selection();
        for ( const auto& selectionRange : selection ) {
//            qDebug() << __func__ << "() selectionRange: ["
//                     << selectionRange.top() << ", "
//                     << selectionRange.left() << "] to ["
//                     << selectionRange.bottom() << ", "
//                     << selectionRange.right() << "]" << Qt::endl;
            model->killSelection( selectionRange.top(),
                                  selectionRange.left(),
                                  selectionRange.bottom(),
                                  selectionRange.right() );
        }
    } else {
        model->killAll();
    }
}


void MainWindow::on_actionEditResurrect_triggered()
{
    if ( selectionModel->hasSelection() ) {
        const QItemSelection selection = selectionModel->selection();
        for ( const auto& selectionRange : selection ) {
            model->resurrectSelection( selectionRange.top(),
                                       selectionRange.left(),
                                       selectionRange.bottom(),
                                       selectionRange.right() );
        }
    } else {
        model->resurrectAll();
    }
}


void MainWindow::on_actionEditPopulateRandomly_triggered()
{
    if ( selectionModel->hasSelection() ) {
        const QItemSelection selection = selectionModel->selection();
        for ( const auto& selectionRange : selection ) {
            model->populateSelectionRandomly( selectionRange.top(),
                                              selectionRange.left(),
                                              selectionRange.bottom(),
                                              selectionRange.right() );
        }
    } else {
        model->populateAllRandomly();
    }
}


void MainWindow::on_actionSelectAll_triggered()
{
    ui->gameFieldTableView->selectAll();
}


void MainWindow::on_actionSelectNone_triggered()
{
    ui->gameFieldTableView->clearSelection();
}


void MainWindow::on_actionSelectInvert_triggered()
{
    if ( selectionModel->hasSelection() ) {
        QModelIndex topLeft = model->index( 0, 0, QModelIndex() );
        QModelIndex bottomRight = model->index( model->rowCount() - 1,
                                                model->columnCount() - 1,
                                                QModelIndex() );
        QItemSelection allSelection( topLeft, bottomRight );
        selectionModel->select( allSelection, QItemSelectionModel::Toggle );
    } else {
        ui->gameFieldTableView->selectAll();
    }
}


void MainWindow::on_actionWorldExpand_triggered()
{
//    qDebug() << __func__ << "() called " << Qt::endl;
    if ( ! expandWorldSizeDialog )
        return;

    assert( model != nullptr );

    // Setup the ExpandWorldSizeDialog
    auto worldColumns = model->columnCount( model->index( 0, 0 ) );
    expandWorldSizeDialog->setWorldColumns( worldColumns );
    expandWorldSizeDialog->setExpandedWorldColumns( worldColumns );
    auto worldRows = model->rowCount( model->index( 0, 0 ) );
    expandWorldSizeDialog->setWorldRows( worldRows );
    expandWorldSizeDialog->setExpandedWorldRows( worldRows );
    expandWorldSizeDialog->setHorizontalOffset( 0 );
    expandWorldSizeDialog->setVerticalOffset( 0 );

    auto dialogResult = expandWorldSizeDialog->exec();

    QString errorMessage;
    if ( dialogResult == QDialog::Accepted ) {
        try {
            auto newHeight = expandWorldSizeDialog->expandedWorldRows();
            auto newWidth = expandWorldSizeDialog->expandedWorldColumns();
            auto horizOffset = expandWorldSizeDialog->horizontalOffset();
            auto vertOffset = expandWorldSizeDialog->verticalOffset();
            model->expand( newWidth, newHeight, horizOffset, vertOffset );
        }
        catch ( std::exception& ex )
        {
            errorMessage = tr( "Caught std::exception: " );
            errorMessage.append( ex.what() );
        }
        catch ( ... )
        {
            errorMessage = tr( "Caught unknown exception!" );
        }
    }

    if ( ! errorMessage.isEmpty() ) {
        QMessageBox::critical( this, tr( "Expand World Size" ),
                               tr( "Unable to expand the World!\n%1" )
                               .arg( errorMessage ) );
    }
}


void MainWindow::on_actionWorldCropToSelection_triggered()
{
    static const QString operationName = tr( "Crop World to Selection" );

    if ( ! selectionModel->hasSelection() ) {
        QMessageBox::warning( this,
                              operationName,
                              tr( "There is no selection to use!" ) );
        return;
    }

    const QItemSelection selection = selectionModel->selection();
    if ( selection.size() != 1 ) {
        QMessageBox::warning( this,
                              operationName,
                              tr( "Only single rectangular selections can "
                                  "be used!" ) );
        return;
    }

    QString errorMessage;

    QGuiApplication::setOverrideCursor( Qt::WaitCursor );

    try
    {
        const QItemSelection selection = selectionModel->selection();
        const QItemSelectionRange& selectionRange = selection.first();
        model->crop( selectionRange.top(),
                     selectionRange.left(),
                     selectionRange.bottom(),
                     selectionRange.right() );
    }
    catch ( std::exception& ex )
    {
        errorMessage = tr( "Cannot crop world to selection:\n%1." )
                       .arg( ex.what() );
    }
    catch ( ... )
    {
        errorMessage = tr( "Cannot crop world to selection:"
                           "Caught unknown exception!" );
    }

    QGuiApplication::restoreOverrideCursor();

    if ( ! errorMessage.isEmpty() ) {
        QMessageBox::warning( this, operationName, errorMessage );
    }
}


Q_INVOKABLE void MainWindow::on_topologyGroup_triggered( QAction* action )
{
    QVariant v = action->data();
    GameFieldModel::Topology topology = v.value<GameFieldModel::Topology>();
    model->setTopology( topology );
}


void MainWindow::on_actionReset_triggered()
{
    model->resetToSeedGeneration();
}


void MainWindow::on_actionNextGeneration_triggered()
{
    model->startSingleGeneration();
}


void MainWindow::on_actionStartGenerating_triggered()
{
    model->startContinuousGeneration();
}


void MainWindow::on_actionStopGenerating_triggered()
{
    model->stopGeneration();
}


void MainWindow::on_actionShowGrid_toggled( bool checked )
{
    ui->gameFieldTableView->setShowGrid( checked );

    ui->actionCollapseCellSpacing->setEnabled( ! ui->gameFieldTableView->showGrid() );
}


void MainWindow::on_actionCollapseCellSpacing_toggled( bool checked )
{
    ui->gameFieldTableView->setPreserveGridSpacing( ! checked );
}


void MainWindow::on_actionAboutGameofLife_triggered()
{
    auto dialog = new AboutDialog( this );
    dialog->setAttribute( Qt::WA_DeleteOnClose );
    dialog->show();
    dialog->raise();
}


void MainWindow::on_actionAboutQt_triggered()
{
    qApp->aboutQt();
}


void MainWindow::on_cellSizeSpinBox_valueChanged( int value )
{
//    qDebug() << __func__ << "( " << value << " ) called " << Qt::endl;
    QString toolTip = cellSizeToolTipPrefix + QString::number( value ) +
        cellSizeUnit;
    cellSizeSpinBox->setToolTip( toolTip );

    if ( ui->gameFieldTableView->cellSize() != value )
        ui->gameFieldTableView->setCellSize( value );

    if ( ui->cellSizeSlider->value() != value )
        ui->cellSizeSlider->setValue( value );

    if ( ui->cellSizeSlider->toolTip() != toolTip )
        ui->cellSizeSlider->setToolTip( toolTip );
}


void MainWindow::on_cellSizeSlider_valueChanged( int value )
{
//    qDebug() << __func__ << "( " << value << " ) called " << Qt::endl;
    QString toolTip = cellSizeToolTipPrefix + QString::number( value ) +
        cellSizeUnit;
    ui->cellSizeSlider->setToolTip( toolTip );

    if ( ui->gameFieldTableView->cellSize() != value )
        ui->gameFieldTableView->setCellSize( value );

    if ( cellSizeSpinBox->value() != value )
        cellSizeSpinBox->setValue( value );

    if ( cellSizeSpinBox->toolTip() != toolTip )
        cellSizeSpinBox->setToolTip( toolTip );
}


void MainWindow::on_frameDurationSpinBox_valueChanged( int value )
{
//    qDebug() << __func__ << "( " << value << " ) called " << Qt::endl;
    QString toolTip = frameDurationToolTipPrefix + QString::number( value ) +
        frameDurationUnit;;
    frameDurationSpinBox->setToolTip( toolTip );

    if ( model->framePeriod() != value )
        model->setFramePeriod( value );

    if ( ui->frameDurationSlider->value() != value )
        ui->frameDurationSlider->setValue( value );

    if ( ui->frameDurationSlider->toolTip() != toolTip )
        ui->frameDurationSlider->setToolTip( toolTip );
}


void MainWindow::on_frameDurationSlider_valueChanged( int value )
{
//    qDebug() << __func__ << "( " << value << " ) called " << Qt::endl;
    QString toolTip = frameDurationToolTipPrefix + QString::number( value ) +
        frameDurationUnit;
    ui->frameDurationSlider->setToolTip( toolTip );

    if ( model->framePeriod() != value )
        model->setFramePeriod( value );

    if ( frameDurationSpinBox->value() != value )
        frameDurationSpinBox->setValue( value );

    if ( frameDurationSpinBox->toolTip() != toolTip )
        frameDurationSpinBox->setToolTip( toolTip );
}


void MainWindow::on_model_dataChanged( const QModelIndex& /* topLeft */,
                                       const QModelIndex& /* bottomRight */,
                                       const QVector<int>& /* roles */ )
{
    displayPopulationInfo();
}


void MainWindow::on_model_topologyChanged( /*GameFieldModel::Topology topology*/ )
{
    displayWorldInfo();
}


void MainWindow::on_model_modelReset()
{
    updateActionsState();

    ui->gameFieldTableView->updateTableView();

    displayPopulationInfo();

    displayGenerationInfo();

    displayWorldInfo();
}


void MainWindow::on_model_runStateChanged( bool /* isRunning */ )
{
    updateActionsState();
}


void MainWindow::on_model_seedGenerationChanged()
{
    updateActionsState();
    displayGenerationInfo();
}


void MainWindow::on_model_generationChanged()
{
    displayGenerationInfo();
}


//void MainWindow::on_selectionModel_currentChanged( const QModelIndex& current,
//                                                   const QModelIndex& previous )
//{
//    Q_UNUSED( current )
//    Q_UNUSED( previous )
//}


void MainWindow::on_selectionModel_selectionChanged(
    const QItemSelection& selected,
    const QItemSelection& deselected
)
{
    Q_UNUSED( selected )
    Q_UNUSED( deselected )
    updateActionsState();
}


void MainWindow::on_gameFieldTableView_customContextMenuRequested( const QPoint& pos )
{
    QModelIndex item = ui->gameFieldTableView->indexAt( pos );

    if ( ! item.isValid() )
        return;

    QMenu menu;
    QMenu* contextMenu = new QMenu( this );
    contextMenu->setObjectName( QString::fromUtf8( "contextMenu" ) );

    contextMenu->addAction( ui->actionEditKill );
    contextMenu->addAction( ui->actionEditResurrect );
    contextMenu->addAction( ui->actionEditPopulateRandomly );
    contextMenu->addSeparator();
    contextMenu->addAction( ui->actionSelectAll );
    contextMenu->addAction( ui->actionSelectNone );
    contextMenu->addAction( ui->actionSelectInvert );

    contextMenu->setAttribute( Qt::WA_DeleteOnClose );

    contextMenu->popup( ui->gameFieldTableView->viewport()->mapToGlobal( pos ) );
}


void MainWindow::closeEvent( QCloseEvent* event )
{
    if ( event->spontaneous() ) {
        auto choice = QMessageBox::warning( this,
                                            tr( "Quit" ),
                                            tr( "Do you really want to quit?" ),
                                            QMessageBox::Yes | QMessageBox::No |
                                            QMessageBox::Cancel );
        if ( choice == QMessageBox::Yes ) {
            storeSettings();
            QMainWindow::closeEvent( event );
        }
        else {
            event->ignore();
        }
    } else {
        storeSettings();
        QMainWindow::closeEvent( event );
    }
}


void MainWindow::setThemeIcons()
{
    QIcon::setThemeName( "BuiltIn" );
    //QIcon::setFallbackThemeName( "breeze" );
    //QIcon::setFallbackThemeName( "Tango" );
    //QIcon::setFallbackThemeName( "Adwaita" )
    QIcon::setFallbackThemeName( "oxygen" );

//    qDebug() << "\nQIcon::themeName: " << QIcon::themeName()
//              << Qt::endl;

//    qDebug() << "\nQIcon::fallbackThemeName: " << QIcon::fallbackThemeName()
//              << Qt::endl;

//    qDebug() << "\nQIcon::themeSearchPaths:\n";
//    for ( const auto& sp : QIcon::themeSearchPaths() ) {
//        qDebug() << sp << "\n";
//    }
//    qDebug() << Qt::endl;

//    qDebug() << "\nQIcon::setFallbackSearchPaths:\n";
//    for ( const auto& sp : QIcon::fallbackSearchPaths() ) {
//        qDebug() << sp << "\n";
//    }
//    qDebug() << Qt::endl;
}


void MainWindow::setCurrentFile( const QString& fileName )
{
    static int sequenceNumber = 1;

    isUntitled = fileName.isEmpty();
    if ( fileName.isEmpty() ) {
        currentFile = tr( "ConwayWorld%1.txt" ).arg( sequenceNumber++ );
    } else {
        currentFile = QFileInfo( fileName ).canonicalFilePath();
    }

    if ( ! isUntitled && windowFilePath() != currentFile )
        MainWindow::prependToRecentFiles( currentFile );

    setWindowFilePath( currentFile );
}

void MainWindow::storeSettings()
{
    QSettings settings( QCoreApplication::organizationName(),
                        QCoreApplication::applicationName() );

    settings.beginGroup( cfgGroup_MainWin );
    settings.setValue( cfgKey_MainWinSize, size() );
    settings.setValue( cfgKey_MainWinPos, pos() );
    settings.endGroup();
}


void MainWindow::restoreSettings()
{
    QSettings settings( QCoreApplication::organizationName(),
                        QCoreApplication::applicationName() );

    settings.beginGroup( cfgGroup_MainWin );
    if ( settings.contains( cfgKey_MainWinSize ) ) {
        resize( settings.value( cfgKey_MainWinSize, sizeHint() ).toSize() );
    }

    if ( settings.contains( cfgKey_MainWinPos ) ) {
        move( settings.value( cfgKey_MainWinPos, QPoint(100, 100) ).toPoint() );
    }
    settings.endGroup();
}


void MainWindow::prependToRecentFiles( const QString& fileName )
{
    QSettings settings( QCoreApplication::organizationName(),
                        QCoreApplication::applicationName() );

    const std::list<QString> oldRecentFiles = readRecentFiles( settings );
    std::list<QString> recentFiles = oldRecentFiles;
    recentFiles.remove( fileName );
    recentFiles.push_front( fileName );
    if ( oldRecentFiles != recentFiles )
        writeRecentFiles( recentFiles, settings );

    setRecentFilesVisible( ! recentFiles.empty() );
}


bool MainWindow::hasRecentFiles()
{
    QSettings settings( QCoreApplication::organizationName(),
                        QCoreApplication::applicationName() );

    settings.beginGroup( cfgGroup_General );

    const int count = settings.beginReadArray( cfgPrefix_RecentFileList );
    settings.endArray();

    settings.endGroup();

    return count > 0;
}


std::list<QString> MainWindow::readRecentFiles( QSettings& settings )
{
    std::list<QString> result;

    settings.beginGroup( cfgGroup_General );

    const int count = settings.beginReadArray( cfgPrefix_RecentFileList );
    for ( int i = 0; i < count; ++i ) {
        settings.setArrayIndex( i );
        result.emplace_back( settings.value( cfgKey_RecentFile ).toString() );
    }
    settings.endArray();

    settings.endGroup();

    return result;
}


void MainWindow::writeRecentFiles( const std::list<QString>& files,
                                   QSettings& settings )
{
    settings.beginGroup( cfgGroup_General );

    settings.beginWriteArray( cfgPrefix_RecentFileList );
    int i = 0;
    for ( const auto& file :  files ) {
        settings.setArrayIndex( i++ );
        settings.setValue( cfgKey_RecentFile, file );
    }
    settings.endArray();

    settings.endGroup();
}


void MainWindow::setRecentFilesVisible( bool visible )
{
    actionRecentFileSubMenu->setVisible( visible );
}


void MainWindow::openRecentFile()
{
    if ( const QAction* action = qobject_cast<const QAction*>( sender() ) )
        openFile( action->data().toString() );
}


std::pair<QWidget*, QSpinBox*>
MainWindow::createCellSizeSpinBoxTool( QWidget* parent )
{
    auto cellSizeSpinBoxContainer = new QWidget( parent );
    cellSizeSpinBoxContainer->setObjectName( QString::fromUtf8( "cellSizeSpinBoxContainer" ) );

    auto cellSizeSpinBoxLayout = new QVBoxLayout( cellSizeSpinBoxContainer );
    cellSizeSpinBoxLayout->setObjectName( QString::fromUtf8( "cellSizeSpinBoxLayout" ) );
    cellSizeSpinBoxLayout->setSizeConstraint( QLayout::SetMinAndMaxSize );

    auto cellSizeSpinBoxTool = new QSpinBox( cellSizeSpinBoxContainer );
    cellSizeSpinBoxTool->setObjectName( QString::fromUtf8( "cellSizeSpinBox" ) );
    cellSizeSpinBoxTool->setSuffix( cellSizeUnit );
    cellSizeSpinBoxTool->setMinimum( 4 );
    cellSizeSpinBoxTool->setMaximum( 64 );
    cellSizeSpinBoxTool->setSingleStep( 1 );

    cellSizeSpinBoxLayout->addWidget( cellSizeSpinBoxTool );

    return std::make_pair( cellSizeSpinBoxContainer, cellSizeSpinBoxTool );
}


std::pair<QWidget*, QSpinBox*>
MainWindow::createFrameDurationSpinBoxTool( QWidget* parent )
{
    auto frameDurationSpinBoxContainer = new QWidget( parent );
    frameDurationSpinBoxContainer->setObjectName( QString::fromUtf8( "frameDurationSpinBoxContainer" ) );

    auto frameDurationSpinBoxLayout = new QVBoxLayout( frameDurationSpinBoxContainer );
    frameDurationSpinBoxLayout->setObjectName( QString::fromUtf8( "frameDurationSpinBoxLayout" ) );
    frameDurationSpinBoxLayout->setSizeConstraint( QLayout::SetMinAndMaxSize );

    auto frameDurationSpinBoxTool = new QSpinBox( frameDurationSpinBoxContainer );
    frameDurationSpinBoxTool->setObjectName( QString::fromUtf8( "frameDurationSpinBox" ) );
    frameDurationSpinBoxTool->setSuffix( frameDurationUnit );
    frameDurationSpinBoxTool->setMinimum( 0 );
    frameDurationSpinBoxTool->setMaximum( 2000 );
    frameDurationSpinBoxTool->setSingleStep( 25 );

    frameDurationSpinBoxLayout->addWidget( frameDurationSpinBoxTool );

    return std::make_pair( frameDurationSpinBoxContainer, frameDurationSpinBoxTool );
}


void MainWindow::updateActionsState()
{
    bool isModelRunning = model->isRunning();

    ui->actionFileNew->setEnabled( ! isModelRunning );
    ui->actionFileOpen->setEnabled( ! isModelRunning );
    actionRecentFileSubMenu->setEnabled( ! isModelRunning );
    ui->actionFileSaveWhole->setEnabled( ! isModelRunning );
    ui->actionFileSaveSelection->setEnabled( ! isModelRunning );

    ui->actionEditKill->setEnabled( ! isModelRunning );
    ui->actionEditResurrect->setEnabled( ! isModelRunning );
    ui->actionEditPopulateRandomly->setEnabled( ! isModelRunning );

    ui->actionSelectAll->setEnabled( ! isModelRunning );
    ui->actionSelectNone->setEnabled( selectionModel->hasSelection() );
    ui->actionSelectInvert->setEnabled( ! isModelRunning );

    ui->actionReset->setEnabled( model->isResettableToSeedGeneration() );
    ui->actionNextGeneration->setEnabled( ! isModelRunning );
    ui->actionStartGenerating->setEnabled( ! isModelRunning );
    ui->actionStopGenerating->setEnabled( isModelRunning );

    ui->actionEditPreferences->setEnabled( ! isModelRunning );
}


void MainWindow::displayPopulationInfo()
{
    ui->populationCount->display( static_cast<int>( model->populationCount() ) );
    ui->populationDensity->display( QString::number( model->populationDensity(), 'f', 9 ) );
}


void MainWindow::displayGenerationInfo()
{
    ui->generation->display( static_cast<int>( model->generation() ) );
}


void MainWindow::displayWorldInfo()
{
    auto columnCount = model->columnCount( model->index( 0, 0 ) );
    auto rowCount = model->rowCount( model->index( 0, 0 ) );

    QString worldSizeInfo = QString( worldSizeInfoFormat ).arg(
        QString::number( columnCount ),
        QString::number( rowCount )
    );
    worldSizeInfoLabel->setText( worldSizeInfo );

    QString worldTopologyInfo = QString( worldTopologyInfoFormat ).arg(
        GameFieldModel::toString( model->topology() )
    );
    worldTopologyInfoLabel->setText( worldTopologyInfo );
}
