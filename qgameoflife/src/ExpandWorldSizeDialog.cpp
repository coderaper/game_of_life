/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <QtWidgets/QAbstractButton>

#include <qgameoflife/ExpandWorldSizeDialog.h>
#include <qgameoflife/ui_ExpandWorldSizeDialog.h>

ExpandWorldSizeDialog::ExpandWorldSizeDialog( QWidget* parent )
: QDialog( parent ),
  ui( std::make_unique<Ui::ExpandWorldSizeDialog>() )
{
    ui->setupUi( this );

    //setFixedSize( sizeHint() );
    setFixedSize( minimumSize() );
}


ExpandWorldSizeDialog::~ExpandWorldSizeDialog() = default;


void ExpandWorldSizeDialog::setWorldColumns( int columns )
{
    worldColumns_ = columns;
    ui->expandedWorldColumnsSpinBox->setMinimum( worldColumns_ );
}


int ExpandWorldSizeDialog::worldColumns() const
{
    return worldColumns_;
}


void ExpandWorldSizeDialog::setWorldRows( int rows )
{
    worldRows_ = rows;
    ui->expandedWorldRowsSpinBox->setMinimum( worldRows_ );
}


int ExpandWorldSizeDialog::worldRows() const
{
    return worldRows_;
}


void ExpandWorldSizeDialog::setExpandedWorldColumns( int columns )
{
    ui->expandedWorldColumnsSpinBox->setValue( columns );
}


int ExpandWorldSizeDialog::expandedWorldColumns() const
{
    return ui->expandedWorldColumnsSpinBox->value();
}


void ExpandWorldSizeDialog::setExpandedWorldRows( int rows )
{
    ui->expandedWorldRowsSpinBox->setValue( rows );
}



int ExpandWorldSizeDialog::expandedWorldRows() const
{
    return ui->expandedWorldRowsSpinBox->value();
}


void ExpandWorldSizeDialog::setHorizontalOffset( int horizontalOffset )
{
    ui->horizontalOffsetSpinBox->setValue( horizontalOffset );
}


int ExpandWorldSizeDialog::horizontalOffset() const
{
    return ui->horizontalOffsetSpinBox->value();
}


void ExpandWorldSizeDialog::setVerticalOffset( int verticalOffset )
{
    ui->verticalOffsetSpinBox->setValue( verticalOffset );
}


int ExpandWorldSizeDialog::verticalOffset() const
{
    return ui->verticalOffsetSpinBox->value();
}


void ExpandWorldSizeDialog::on_expandedWorldColumnsSpinBox_valueChanged( int value )
{
    int maxHorizontalOffset = value - worldColumns();
    ui->horizontalOffsetSpinBox->setMaximum( maxHorizontalOffset );
}


void ExpandWorldSizeDialog::on_expandedWorldRowsSpinBox_valueChanged( int value )
{
    int maxVerticalOffset = value - worldRows();
    ui->verticalOffsetSpinBox->setMaximum( maxVerticalOffset );
}


void ExpandWorldSizeDialog::on_centerButton_clicked( bool /* checked */ )
{
    int maxHorizontalOffset =
        ui->expandedWorldColumnsSpinBox->value() - worldColumns();

    ui->horizontalOffsetSpinBox->setValue( maxHorizontalOffset / 2 );

    int maxVerticalOffset =
        ui->expandedWorldRowsSpinBox->value() - worldRows();

    ui->verticalOffsetSpinBox->setValue( maxVerticalOffset / 2 );
}


void ExpandWorldSizeDialog::on_buttonBox_clicked( QAbstractButton* button )
{
    if ( button == ui->buttonBox->button( QDialogButtonBox::Reset ) ) {
        setExpandedWorldColumns( worldColumns() );
        setExpandedWorldRows( worldRows() );
    }
}
