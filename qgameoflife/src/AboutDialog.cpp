/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#include <QtGui/QPainter>

#include <qgameoflife/AboutDialog.h>
#include <qgameoflife/ui_AboutDialog.h>
#include <qgameoflife/qgameoflife_config.h>


AboutDialog::AboutDialog( QWidget* parent )
: QDialog( parent ),
  ui( std::make_unique<Ui::AboutDialog>() )
{
    ui->setupUi( this );
    setWindowTitle( tr( "About %1" ).arg( PROGRAM_NAME ) );

    //setAutoFillBackground( false );

    QString css = "<style TYPE='text/css'> "
                  "body { font-family: sans-serif; } "
                  ".appname { font-size: 16pt; color: darkred; } "
                  ".appver { font-size: 12pt; color: darkred; } "
                  "h2 { font-size: 12pt; } "
                  "li { line-height: 120%; } "
                  "</style>";

    ui->titleLabel->setStyleSheet( "background-color : black;" );
    ui->titleLabel->setText( css + titleText() );

    ui->aboutLabel->setText(
        "<p>" + descriptionText() + "</p>"
        "<p>" + copyrightText() + "</p>"
        "<hr>"
        "<p>" + tr( "Project page: %1").arg( homepageText() ) + "</p>"
        "<p>" + tr( "License: %1" ).arg( licenseText() ) + "</p>"
    );
}


AboutDialog::~AboutDialog() = default;


void AboutDialog::paintEvent( QPaintEvent* )
{
    QRect rect( 0, 0,
                this->width(),
                ui->titleLabel->pos().y() + ui->titleLabel->height() );
    QPainter painter( this );
    painter.fillRect( rect, QColor::fromRgb( 0x000000 ) );
}


QString AboutDialog::titleText() const
{
    return "<table style='width:100%' border=0><tr>"
           "<td style='padding:8px 8px;'><img src=':/icons/BuiltIn/48x48/apps/game-of-life.png' style='margin:8 px;'></td>"
           "<td style='padding:8px 8px;'>" +
           QString( "<div class=appname>%1</div><div class=appver>Version %2</div>")
           .arg( PROGRAM_NAME, PROGRAM_VERSION_STR ) + "</td></tr></table>";
}


QString AboutDialog::descriptionText() const
{
    return tr( "%1 is a simulator of the "
               "<a href='https://en.wikipedia.org/wiki/John_Horton_Conway'>"
               "John Conway</a>'s famous "
               "<a href='https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life'>"
               "Game of Life</a>." ).arg( PROGRAM_NAME );
}


QString AboutDialog::copyrightText() const
{
    return tr( "Copyright (c) 2021 Daniel Balkanski" );
}


QString AboutDialog::homepageText() const
{
    return "<a href='https://gitlab.com/coderaper/game_of_life'>"
           "https://gitlab.com/coderaper/game_of_life</a>";
}


QString AboutDialog::licenseText() const
{
    return "<a href='https://www.gnu.org/licenses/old-licenses/gpl-2.0.html'>"
           "GNU General Public License version 2 or later</a>";
}
