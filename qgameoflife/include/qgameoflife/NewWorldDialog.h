/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef NEWWORLDDIALOG_H_INCLUDED
#define NEWWORLDDIALOG_H_INCLUDED

#include <memory>

#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class NewWorldDialog; }

QT_END_NAMESPACE

class NewWorldDialog : public QDialog
{
    Q_OBJECT

    Q_PROPERTY( int worldColumns READ worldColumns WRITE setWorldColumns )
    Q_PROPERTY( int worldRows READ worldRows WRITE setWorldRows )
    Q_PROPERTY( bool populateWorldRandomly READ populateWorldRandomly WRITE setPopulateWorldRandomly )
    Q_PROPERTY( double desiredPopulationDensity READ desiredPopulationDensity WRITE setDesiredPopulationDensity )

public:
    explicit NewWorldDialog( QWidget* parent = nullptr );

    ~NewWorldDialog();

    void setWorldColumns( int columns );
    int worldColumns() const;

    void setWorldRows( int rows );
    int worldRows() const;

    void setPopulateWorldRandomly( bool populate );
    bool populateWorldRandomly() const;

    void setDesiredPopulationDensity( double populationDensity );
    double desiredPopulationDensity() const;

private:
    std::unique_ptr<Ui::NewWorldDialog> ui;
};

#endif // NEWWORLDDIALOG_H_INCLUDED
