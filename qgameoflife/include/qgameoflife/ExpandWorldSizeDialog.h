/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef EXPANDWORLDSIZEDIALOG_H_INCLUDED
#define EXPANDWORLDSIZEDIALOG_H_INCLUDED

#include <memory>

#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE
class QAbstractButton;
namespace Ui { class ExpandWorldSizeDialog; }
QT_END_NAMESPACE

class ExpandWorldSizeDialog : public QDialog
{
    Q_OBJECT

    Q_PROPERTY( int worldColumns READ worldColumns WRITE setWorldColumns )
    Q_PROPERTY( int worldRows READ worldRows WRITE setWorldRows )
    Q_PROPERTY( int expandedWorldColumns READ expandedWorldColumns WRITE setExpandedWorldColumns )
    Q_PROPERTY( int expandedWorldRows READ expandedWorldRows WRITE setExpandedWorldRows )
    Q_PROPERTY( int horizontalOffset READ horizontalOffset WRITE setHorizontalOffset )
    Q_PROPERTY( int verticalOffset READ verticalOffset WRITE setVerticalOffset )

public:
    explicit ExpandWorldSizeDialog( QWidget* parent = nullptr );

    ~ExpandWorldSizeDialog();

    void setWorldColumns( int columns );
    int worldColumns() const;

    void setWorldRows( int rows );
    int worldRows() const;

    void setExpandedWorldColumns( int columns );
    int expandedWorldColumns() const;

    void setExpandedWorldRows( int rows );
    int expandedWorldRows() const;

    void setHorizontalOffset( int horizontalOffset );
    int horizontalOffset() const;

    void setVerticalOffset( int verticalOffset );
    int verticalOffset() const;

private:
    Q_INVOKABLE void on_expandedWorldColumnsSpinBox_valueChanged( int value );

    Q_INVOKABLE void on_expandedWorldRowsSpinBox_valueChanged( int value );

    Q_INVOKABLE void on_centerButton_clicked( bool checked );

    Q_INVOKABLE void on_buttonBox_clicked( QAbstractButton* button );

private:
    std::unique_ptr<Ui::ExpandWorldSizeDialog> ui;
    int worldColumns_ = 0;
    int worldRows_ = 0;
};

#endif // EXPANDWORLDSIZEDIALOG_H_INCLUDED
