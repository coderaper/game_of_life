#ifndef APPLICATION_H_INCLUDED
#define APPLICATION_H_INCLUDED

#include <QtWidgets/QApplication>
//#include <QtWidgets/QtWidgets>
#include <QtWidgets/QMessageBox>
#include <QtCore/QString>

#include <exception>

class Application : public QApplication
{
public:
    Application( int& argc, char** argv )
    : QApplication( argc, argv )
    {}

    virtual ~Application() {}

	virtual bool notify( QObject* receiver, QEvent* event )
	{
		try
		{
			return QApplication::notify( receiver, event );
		}
        catch ( std::exception& ex )
		{
            QString msg = tr( "Caught std::exception in object '%1':\n%2" )
                .arg( receiver->objectName(), ex.what() );
            QMessageBox::critical( nullptr, tr( "Critical Error" ), msg );
		}
        catch ( ... )
        {
            QString msg = tr( "Caught unknown exception in object '%1'!" )
                .arg( receiver->objectName() );
            QMessageBox::critical( nullptr, tr( "Critical Error" ), msg );
        }
		return false;
	}

};

#endif // APPLICATION_H_INCLUDED
