/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef CELLITEMDELEGATE_H_INCLUDED
#define CELLITEMDELEGATE_H_INCLUDED

#include <QtWidgets/QAbstractItemDelegate>
#include <QtWidgets/QStyledItemDelegate>
#include <QtCore/QModelIndex>
#include <QtCore/QSize>
#include <QtCore/QMargins>
#include <QtCore/QMarginsF>

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QObject;
class QPainter;
QT_END_NAMESPACE


class CellItemDelegate : public QAbstractItemDelegate
{
    Q_OBJECT

public:
    CellItemDelegate( QObject* parent = nullptr );

    void paint( QPainter* painter,
                const QStyleOptionViewItem& option,
                const QModelIndex& index ) const override;

    QSize sizeHint( const QStyleOptionViewItem& option,
                    const QModelIndex& index ) const override;

    int cellSize() const { return cellSize_; }

    bool circularCells() const { return circularCells_; }

    QMargins contentsMargins() const { return contentsMargins_; }

public:
    Q_INVOKABLE void setCellSize( int size );

    Q_INVOKABLE void setCircularCells( bool circular );

    Q_INVOKABLE void setContentsMargins( int left, int top, int right, int bottom );

    Q_INVOKABLE void setContentsMargins( const QMargins& margins );

protected:
    virtual bool editorEvent( QEvent* event,
                              QAbstractItemModel* model,
                              const QStyleOptionViewItem& option,
                              const QModelIndex& index ) override;

private:
    int cellSize_ = 16;

    bool circularCells_ = true;

    QMargins contentsMargins_;
};


#endif  // CELLITEMDELEGATE_H_INCLUDED
