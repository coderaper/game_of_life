/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef ABOUTDIALOG_H_INCLUDED
#define ABOUTDIALOG_H_INCLUDED

#include <memory>

#include <QtWidgets/QDialog>
#include <QtCore/QString>

QT_BEGIN_NAMESPACE
namespace Ui { class AboutDialog; }
QT_END_NAMESPACE

class AboutDialog : public QDialog
{
    Q_OBJECT
public:
    explicit AboutDialog( QWidget* parent = nullptr );

    ~AboutDialog();

protected:
    void paintEvent( QPaintEvent* event ) override;

private:
    QString titleText() const;
    QString descriptionText() const;
    QString copyrightText() const;
    QString homepageText() const;
    QString licenseText() const;

    std::unique_ptr<Ui::AboutDialog> ui;
};

#endif // ABOUTDIALOG_H_INCLUDED
