/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAMEFIELDMODEL_H_INCLUDED
#define GAMEFIELDMODEL_H_INCLUDED

#include <atomic>
#include <array>
#include <chrono>
#include <memory>
#include <future>

#include <QtCore/QAbstractTableModel>
#include <QtCore/QPoint>

namespace game_of_life {

class GameOfLifeEngine;

}  // namespace game_of_life

class GameFieldModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit GameFieldModel( QObject* parent = nullptr );

    ~GameFieldModel();

    int rowCount( const QModelIndex &parent = QModelIndex() ) const override;
    int columnCount( const QModelIndex &parent = QModelIndex() ) const override;

    QVariant data( const QModelIndex& index,
                   int role = Qt::DisplayRole ) const override;
    bool setData( const QModelIndex& index,
                  const QVariant& value,
                  int role = Qt::EditRole ) override;

    QVariant headerData( int section,
                         Qt::Orientation orientation,
                         int role = Qt::DisplayRole ) const override;

    Qt::ItemFlags flags( const QModelIndex& index ) const override;

    int framePeriod() const;
    void setFramePeriod( int ms );

    int populationCount() const;

    /// Returns the current population density
    double populationDensity() const;

    double populateAliveRatio() const;

    void setPopulateAliveRatio( double aliveRatio );

    bool isResettableToSeedGeneration() const;

    void resetToSeedGeneration();

    bool isRunning() const;

    unsigned long long generation() const;

    enum Topology {
        BoundedPlane,
        Torus,
        KleinBottleHorizontalTwist,
        KleinBottleVerticalTwist,
        CrossSurface,
//        Sphere,
    };
    Q_ENUM( Topology )

    /// Returns the current world topology
    Topology topology() const;

    /// Sets new world topology
    void setTopology( Topology topology );

    static QString toString( Topology topology );

    /// Create new empty game field
    Q_INVOKABLE void createNewEmpty( size_t width, size_t height );

    /// Create new random populated game field
    Q_INVOKABLE void createNewRandomPopulated( size_t width, size_t height );

    /// Expand the existing game field
    Q_INVOKABLE void expand( size_t newWidth, size_t newHeight,
                             size_t horizOffset, size_t vertOffset );

    /// Crop the existing game field
    Q_INVOKABLE void crop( size_t top, size_t left,
                           size_t bottom, size_t right );

    /// Load a game field from file
    Q_INVOKABLE bool load( const QString& fileName );

    /// Save the game field to file
    Q_INVOKABLE bool save( const QString& fileName );
    /// Save the game field region to file
    Q_INVOKABLE bool saveSelection( const QString& fileName,
                                    size_t top, size_t left,
                                    size_t bottom, size_t right );

    /// Kill all cells
    Q_INVOKABLE void killAll();
    /// Kill all cells in specified region
    Q_INVOKABLE void killSelection( size_t top, size_t left,
                                    size_t bottom, size_t right );

    /// Resurrect all cells
    Q_INVOKABLE void resurrectAll();
    /// Resurrect all cells in specified region
    Q_INVOKABLE void resurrectSelection( size_t top, size_t left,
                                         size_t bottom, size_t right );

    /// Randomly populate game filed with life cells
    Q_INVOKABLE void populateAllRandomly();

    /// Randomly populate game filed region with life cells
    Q_INVOKABLE void populateSelectionRandomly( size_t top, size_t left,
                                                size_t bottom, size_t right );

    Q_INVOKABLE void startSingleGeneration();

    Q_INVOKABLE void startContinuousGeneration();

    Q_INVOKABLE void stopGeneration();

    Q_INVOKABLE void flipGenerations();

    Q_SIGNAL void topologyChanged( Topology topology );

    Q_SIGNAL void runStateChanged( bool isRunning );

    Q_SIGNAL void seedGenerationChanged();

    Q_SIGNAL void generationChanged();

    Q_SIGNAL void nextGenerationComputed( QPrivateSignal dummy );

private:
    void onNextGenerationComputed();

    void updateStatistics() const;

private:
    using GameOfLifeEngine = game_of_life::GameOfLifeEngine;
    std::unique_ptr<GameOfLifeEngine> engine_;
};


#endif // GAMEFIELDMODEL_H_INCLUDED
