/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef GAMEFIELDTABLEVIEW_H_INCLUDED
#define GAMEFIELDTABLEVIEW_H_INCLUDED

#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE
class QKeyEvent;
QT_END_NAMESPACE

class GameFieldModel;
class CellItemDelegate;

class GameFieldTableView : public QTableView
{
public:
    GameFieldTableView( QWidget* parent = nullptr );

    // memeber function redefinitions: Attentions - name hiding!
public:
    void setModel( GameFieldModel* model );
    GameFieldModel* model() const;

    void setItemDelegate( CellItemDelegate* delegate );
    CellItemDelegate* itemDelegate() const;

    int cellSize() const;

    bool circularCells() const;

    bool showGrid() const;

    bool preserveGridSpacing() const;

    void updateTableView();

public:
    Q_INVOKABLE void setCellSize( int size );

    Q_INVOKABLE void setCircularCells( bool circular );

    Q_INVOKABLE void setShowGrid( bool show );

    Q_INVOKABLE void setPreserveGridSpacing( bool preserve );

private:
    void setCellItemDelegateSizeAndMargins( bool showGrid,
                                            bool preserveGridSpacing );

    QSize calcTableSize();

protected:
    void keyPressEvent( QKeyEvent* event ) override;

    void wheelEvent( QWheelEvent* pEvent ) override;

private:
    int cellSize_ = 16;

    bool preserveGridSpacing_ = true;
};

#endif // GAMEFIELDTABLEVIEW_H_INCLUDED
