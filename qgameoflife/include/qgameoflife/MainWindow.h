/*******************************************************************************
* This file is part of game_of_life project.
* https://gitlab.com:coderaper/game_of_life.git
*
* Copyright (c) 2021 Daniel Balkanski
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include <memory>
#include <array>
#include <list>

#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtWidgets/QMainWindow>

class GameFieldModel;
class NewWorldDialog;
class ExpandWorldSizeDialog;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
class QItemSelection;
class QItemSelectionModel;
class QWidget;
class QLabel;
class QSpinBox;
class QSettings;
class QActionGroup;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow( QWidget* parent = nullptr );
    ~MainWindow() override;

    void openFile( const QString& fileName );
    bool saveFile( const QString& fileName,
                   bool selectionOnly = false,
                   const QString& operationName = "Save File" );

private:
    // File menu action slots
    Q_INVOKABLE void on_actionFileNew_triggered();
    Q_INVOKABLE void on_actionFileOpen_triggered();
    Q_INVOKABLE void on_actionFileSaveWhole_triggered();
    Q_INVOKABLE void on_actionFileSaveSelection_triggered();
    Q_INVOKABLE void on_actionExit_triggered();

    // Recent files sub-menu slot
    Q_INVOKABLE void on_recentMenu_aboutToShow();

    // Edit menu action slots
    Q_INVOKABLE void on_actionEditKill_triggered();
    Q_INVOKABLE void on_actionEditResurrect_triggered();
    Q_INVOKABLE void on_actionEditPopulateRandomly_triggered();

    // Select menu action slots
    Q_INVOKABLE void on_actionSelectAll_triggered();
    Q_INVOKABLE void on_actionSelectNone_triggered();
    Q_INVOKABLE void on_actionSelectInvert_triggered();

    // World menu action slots
    Q_INVOKABLE void on_actionWorldExpand_triggered();
    Q_INVOKABLE void on_actionWorldCropToSelection_triggered();
    Q_INVOKABLE void on_topologyGroup_triggered( QAction* action );

    // Control menu action slots
    Q_INVOKABLE void on_actionReset_triggered();
    Q_INVOKABLE void on_actionNextGeneration_triggered();
    Q_INVOKABLE void on_actionStartGenerating_triggered();
    Q_INVOKABLE void on_actionStopGenerating_triggered();

    // View menu action slots
    Q_INVOKABLE void on_actionShowGrid_toggled( bool checked );
    Q_INVOKABLE void on_actionCollapseCellSpacing_toggled( bool checked );

    // Help menu action slots
    Q_INVOKABLE void on_actionAboutGameofLife_triggered();
    Q_INVOKABLE void on_actionAboutQt_triggered();

    // Cell size spin box slot
    Q_INVOKABLE void on_cellSizeSpinBox_valueChanged( int value );

    // Cell size slider
    Q_INVOKABLE void on_cellSizeSlider_valueChanged( int value );

    // Frame duration spin box slot
    Q_INVOKABLE void on_frameDurationSpinBox_valueChanged( int value );

    // Frame duration slider slot
    Q_INVOKABLE void on_frameDurationSlider_valueChanged( int value );

    Q_INVOKABLE void on_model_dataChanged( const QModelIndex& topLeft,
                                           const QModelIndex &bottomRight,
                                           const QVector<int>& roles );

    Q_INVOKABLE void on_model_topologyChanged( /*GameFieldModel::Topology topology*/ );

    Q_INVOKABLE void on_model_modelReset();

    Q_INVOKABLE void on_model_runStateChanged( bool isRunning );

    Q_INVOKABLE void on_model_seedGenerationChanged();

    Q_INVOKABLE void on_model_generationChanged();

//    Q_INVOKABLE void on_selectionModel_currentChanged( const QModelIndex& current,
//                                                       const QModelIndex& previous );
    Q_INVOKABLE void on_selectionModel_selectionChanged( const QItemSelection& selected,
                                                         const QItemSelection& deselected );

    Q_INVOKABLE void on_gameFieldTableView_customContextMenuRequested( const QPoint& pos );

    void closeEvent( QCloseEvent* event ) override;

private:
    void setThemeIcons();

    void setCurrentFile( const QString& fileName );

    void storeSettings();
    void restoreSettings();

    void prependToRecentFiles( const QString& fileName );
    void setRecentFilesVisible( bool visible );

    void openRecentFile();

    static bool hasRecentFiles();
    static std::list<QString> readRecentFiles( QSettings& settings );
    static void writeRecentFiles( const std::list<QString>& files,
                                  QSettings& settings );

    std::pair<QWidget*, QSpinBox*>
    createCellSizeSpinBoxTool( QWidget* parent = nullptr );

    std::pair<QWidget*, QSpinBox*>
    createFrameDurationSpinBoxTool( QWidget* parent = nullptr );

    void updateActionsState();

    void displayPopulationInfo();

    void displayGenerationInfo();

    void displayWorldInfo();

private:
    std::unique_ptr<Ui::MainWindow> ui;
    NewWorldDialog* newWorldDialog;
    ExpandWorldSizeDialog* expandWorldSizeDialog;
    QSpinBox* cellSizeSpinBox = nullptr;
    QSpinBox* frameDurationSpinBox = nullptr;
    QLabel* worldSizeInfoLabel = nullptr;
    QLabel* worldTopologyInfoLabel = nullptr;

    GameFieldModel* model = nullptr;
    QItemSelectionModel* selectionModel = nullptr;

    QAction* actionRecentFileSubMenu = nullptr;
    static constexpr std::size_t maxRecentFiles = 12;
    std::array<QAction*, maxRecentFiles> recentFileActions;

    QActionGroup* topologyGroup = nullptr;

    bool isUntitled = true;
    QString currentFile;

    static const QString cellSizeToolTipPrefix;
    static const QString cellSizeUnit;

    static const QString frameDurationToolTipPrefix;
    static const QString frameDurationUnit;

    static const QString worldSizeInfoFormat;
    static const QString worldTopologyInfoFormat;

    static const char* cfgGroup_MainWin;
    static const char* cfgKey_MainWinPos;
    static const char* cfgKey_MainWinSize;

    static const char* cfgGroup_General;
    static const char* cfgPrefix_RecentFileList;
    static const char* cfgKey_RecentFile;
};

#endif // MAINWINDOW_H_INCLUDED
